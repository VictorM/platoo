CREATE TABLE bac_on_jeu (
    diese_bac_on_jeu integer NOT NULL,
    num_bac_on_jeu integer NOT NULL,
    id_jeu integer NOT NULL,
    label_bac character varying(45)
);


CREATE TABLE bac_on_page (
    diese_bac_on_page integer NOT NULL,
    num_bac_on_page integer NOT NULL,
    id_page integer NOT NULL,
    label_bac character varying(45),
    noeud_start integer
);


CREATE TABLE bdd_fonctions (
    id_fonction integer NOT NULL,
    label_fonction character varying(45)
);


CREATE TABLE board (
    svg_board character varying(45) NOT NULL,
    id_board integer NOT NULL,
    nom_board character varying(45) NOT NULL
);



CREATE TABLE board_on_jeu (
    diese_board_on_jeu integer NOT NULL,
    num_board_on_jeu integer NOT NULL,
    id_board integer NOT NULL,
    id_jeu integer NOT NULL
);

CREATE TABLE board_on_page (
    diese_board_on_page integer NOT NULL,
    num_board_on_page integer NOT NULL,
    id_page integer NOT NULL,
    id_board integer,
    noeud_start integer
);

CREATE TABLE box_on_jeu (
    diese_box_on_jeu integer NOT NULL,
    num_box_on_jeu integer NOT NULL,
    id_jeu integer NOT NULL,
    id_pion integer NOT NULL
);


CREATE TABLE box_on_page (
    num_box_on_page integer NOT NULL,
    diese_box_on_page integer NOT NULL,
    id_page integer NOT NULL,
    id_pion integer,
    noeud_start integer
);


CREATE TABLE carac_pions_jeu (
    id_jeu integer NOT NULL,
    id_pion integer NOT NULL,
    change_pion integer,
    diese_bac_on_jeu integer,
    taille integer
);


CREATE TABLE carac_pions_joueur_on_jeu (
    id_joueur integer NOT NULL,
    id_jeu integer NOT NULL,
    id_pion integer NOT NULL,
    change_pion integer,
    diese_bac_on_jeu integer,
    taille integer
);


CREATE TABLE carac_pions_joueur_on_page (
    id_page integer NOT NULL,
    id_joueur integer NOT NULL,
    id_pion integer NOT NULL,
    change_pion integer,
    diese_bac_on_page integer,
    taille integer
);


CREATE TABLE carac_pions_page (
    id_page integer NOT NULL,
    id_pion integer NOT NULL,
    change_pion integer,
    diese_bac_on_page integer,
    taille integer
);


CREATE TABLE chat_on_page (
    id_page integer NOT NULL,
    id_message integer NOT NULL,
    message character(200),
    "time" timestamp with time zone,
    auteur character varying(20),
    couleur character varying(20)
);


CREATE TABLE jeu (
    id_jeu integer NOT NULL,
    nom_jeu character varying(45) NOT NULL,
    nombre_trashs integer,
    wiki character varying(45)
);


CREATE TABLE joueur_on_jeu (
    id_jeu integer NOT NULL,
    id_joueur integer NOT NULL,
    label_joueur character varying(45),
    action_clic_onboard integer,
    action_scroll_onboard integer,
    clickable_board integer,
    click_board_create_type integer,
    click_board_move_bac integer,
    click_board_move_type integer
);


CREATE TABLE joueur_on_page (
    id_page integer NOT NULL,
    id_joueur integer NOT NULL,
    label_joueur character varying(45),
    action_clic_onboard integer,
    action_scroll_onboard integer,
    clickable_board integer,
    click_board_create_type integer,
    click_board_move_type integer,
    click_board_move_bac integer
);


CREATE TABLE page (
    id_page integer NOT NULL,
    nom_page character varying(45),
    nombre_trashs integer,
    creating_date timestamp with time zone,
    id_jeu_depart integer,
    noeud_courant integer
);


CREATE TABLE pion (
    id_pion integer NOT NULL,
    svg_pion character varying(45) NOT NULL,
    nom_pion character varying(45) NOT NULL,
    taille integer,
    directed boolean
);


CREATE TABLE pion_on_bac_on_jeu (
    diese_bac_on_jeu integer NOT NULL,
    id_jeu integer NOT NULL,
    num_pion_on_bac_on_jeu integer NOT NULL,
    diese_pion_on_bac_on_jeu integer NOT NULL,
    id_pion integer,
    nombre_pions integer
);


CREATE TABLE pion_on_bac_on_page (
    id_page integer NOT NULL,
    diese_bac_on_page integer NOT NULL,
    num_pion_on_bac_on_page integer NOT NULL,
    diese_pion_on_bac_on_page integer NOT NULL,
    id_pion integer,
    nombre_pions integer,
    noeud_start integer
);


CREATE TABLE pion_on_board_on_jeu (
    diese_board_on_jeu integer NOT NULL,
    id_jeu integer NOT NULL,
    diese_pion_on_board_on_jeu integer NOT NULL,
    num_pion_on_board_on_jeu integer NOT NULL,
    position_x real,
    position_y real,
    taille integer,
    id_pion integer
);


CREATE TABLE pion_on_board_on_page (
    id_page integer NOT NULL,
    diese_board_on_page integer NOT NULL,
    num_pion_on_board_on_page integer NOT NULL,
    diese_pion_on_board_on_page integer NOT NULL,
    id_pion integer,
    noeud_start integer,
    position_x real,
    position_y real,
    taille integer
);


CREATE TABLE save_on_page (
    id_page integer NOT NULL,
    noeud_precedent integer NOT NULL,
    noeud_suivant integer NOT NULL,
    date timestamp with time zone,
    type_action character varying(45),
    auteur character varying(45),
    couleur character varying(20),
    id_joueur integer
);


CREATE TABLE stop_bac_on_page (
    id_page integer NOT NULL,
    num_bac_on_page integer NOT NULL,
    noeud_stop integer NOT NULL
);


CREATE TABLE stop_board_on_page (
    id_page integer NOT NULL,
    num_board_on_page integer NOT NULL,
    noeud_stop integer NOT NULL
);


CREATE TABLE stop_box_on_page (
    id_page integer NOT NULL,
    num_box_on_page integer NOT NULL,
    noeud_stop integer NOT NULL
);

CREATE TABLE stop_pion_on_bac_on_page (
    id_page integer NOT NULL,
    diese_bac_on_page integer NOT NULL,
    num_pion_on_bac_on_page integer NOT NULL,
    noeud_stop integer NOT NULL
);

CREATE TABLE stop_pion_on_board_on_page (
    id_page integer NOT NULL,
    diese_board_on_page integer NOT NULL,
    num_pion_on_board_on_page integer NOT NULL,
    noeud_stop integer NOT NULL
);



INSERT INTO bac_on_jeu (diese_bac_on_jeu, num_bac_on_jeu, id_jeu, label_bac) VALUES (1, 0, 1, 'blanc');
INSERT INTO bac_on_jeu (diese_bac_on_jeu, num_bac_on_jeu, id_jeu, label_bac) VALUES (2, 1, 1, 'noir');
INSERT INTO bac_on_jeu (diese_bac_on_jeu, num_bac_on_jeu, id_jeu, label_bac) VALUES (1, 0, 2, 'blanc');
INSERT INTO bac_on_jeu (diese_bac_on_jeu, num_bac_on_jeu, id_jeu, label_bac) VALUES (2, 1, 2, 'noir');
INSERT INTO bac_on_jeu (diese_bac_on_jeu, num_bac_on_jeu, id_jeu, label_bac) VALUES (1, 0, 3, 'prise de noir');
INSERT INTO bac_on_jeu (diese_bac_on_jeu, num_bac_on_jeu, id_jeu, label_bac) VALUES (2, 1, 3, 'prise de blanc');
INSERT INTO bac_on_jeu (diese_bac_on_jeu, num_bac_on_jeu, id_jeu, label_bac) VALUES (1, 0, 4, 'prise de noir');
INSERT INTO bac_on_jeu (diese_bac_on_jeu, num_bac_on_jeu, id_jeu, label_bac) VALUES (2, 1, 4, 'prise de blanc');
INSERT INTO bac_on_jeu (diese_bac_on_jeu, num_bac_on_jeu, id_jeu, label_bac) VALUES (1, 0, 5, 'prise de noir');
INSERT INTO bac_on_jeu (diese_bac_on_jeu, num_bac_on_jeu, id_jeu, label_bac) VALUES (2, 1, 5, 'prise de blanc');
INSERT INTO bac_on_jeu (diese_bac_on_jeu, num_bac_on_jeu, id_jeu, label_bac) VALUES (1, 0, 6, 'roi');
INSERT INTO bac_on_jeu (diese_bac_on_jeu, num_bac_on_jeu, id_jeu, label_bac) VALUES (2, 1, 6, 'general de jade');
INSERT INTO bac_on_jeu (diese_bac_on_jeu, num_bac_on_jeu, id_jeu, label_bac) VALUES (1, 0, 7, 'tigre');
INSERT INTO bac_on_jeu (diese_bac_on_jeu, num_bac_on_jeu, id_jeu, label_bac) VALUES (2, 1, 7, 'chevre');

INSERT INTO bdd_fonctions (id_fonction, label_fonction) VALUES (1, 'deletePionOnboard');
INSERT INTO bdd_fonctions (id_fonction, label_fonction) VALUES (2, 'changePionOnboard');
INSERT INTO bdd_fonctions (id_fonction, label_fonction) VALUES (3, 'movePionOnboard');
INSERT INTO bdd_fonctions (id_fonction, label_fonction) VALUES (4, 'clickableBoardNew');
INSERT INTO bdd_fonctions (id_fonction, label_fonction) VALUES (5, 'clickableBoardMove');
INSERT INTO bdd_fonctions (id_fonction, label_fonction) VALUES (6, 'movePionOnboardShogi');
INSERT INTO bdd_fonctions (id_fonction, label_fonction) VALUES (7, 'rien');

INSERT INTO board (svg_board, id_board, nom_board) VALUES ('dame.svg', 1, 'dame');
INSERT INTO board (svg_board, id_board, nom_board) VALUES ('echec.svg', 2, 'echec');
INSERT INTO board (svg_board, id_board, nom_board) VALUES ('go9.svg', 3, 'go9');
INSERT INTO board (svg_board, id_board, nom_board) VALUES ('go13.svg', 4, 'go13');
INSERT INTO board (svg_board, id_board, nom_board) VALUES ('go19.svg', 5, 'go19');
INSERT INTO board (svg_board, id_board, nom_board) VALUES ('shogi.svg', 6, 'shogi');
INSERT INTO board (svg_board, id_board, nom_board) VALUES ('baghChal.svg', 7, 'baghChal');

INSERT INTO board_on_jeu (diese_board_on_jeu, num_board_on_jeu, id_board, id_jeu) VALUES (1, 0, 1, 1);
INSERT INTO board_on_jeu (diese_board_on_jeu, num_board_on_jeu, id_board, id_jeu) VALUES (1, 0, 2, 2);
INSERT INTO board_on_jeu (diese_board_on_jeu, num_board_on_jeu, id_board, id_jeu) VALUES (1, 0, 3, 3);
INSERT INTO board_on_jeu (diese_board_on_jeu, num_board_on_jeu, id_board, id_jeu) VALUES (1, 0, 4, 4);
INSERT INTO board_on_jeu (diese_board_on_jeu, num_board_on_jeu, id_board, id_jeu) VALUES (1, 0, 5, 5);
INSERT INTO board_on_jeu (diese_board_on_jeu, num_board_on_jeu, id_board, id_jeu) VALUES (1, 0, 6, 6);
INSERT INTO board_on_jeu (diese_board_on_jeu, num_board_on_jeu, id_board, id_jeu) VALUES (1, 0, 7, 7);

INSERT INTO box_on_jeu (diese_box_on_jeu, num_box_on_jeu, id_jeu, id_pion) VALUES (1, 0, 1, 1);
INSERT INTO box_on_jeu (diese_box_on_jeu, num_box_on_jeu, id_jeu, id_pion) VALUES (2, 1, 1, 2);
INSERT INTO box_on_jeu (diese_box_on_jeu, num_box_on_jeu, id_jeu, id_pion) VALUES (3, 2, 1, 3);
INSERT INTO box_on_jeu (diese_box_on_jeu, num_box_on_jeu, id_jeu, id_pion) VALUES (4, 3, 1, 4);
INSERT INTO box_on_jeu (diese_box_on_jeu, num_box_on_jeu, id_jeu, id_pion) VALUES (1, 0, 2, 5);
INSERT INTO box_on_jeu (diese_box_on_jeu, num_box_on_jeu, id_jeu, id_pion) VALUES (2, 1, 2, 6);
INSERT INTO box_on_jeu (diese_box_on_jeu, num_box_on_jeu, id_jeu, id_pion) VALUES (3, 2, 2, 7);
INSERT INTO box_on_jeu (diese_box_on_jeu, num_box_on_jeu, id_jeu, id_pion) VALUES (4, 3, 2, 8);
INSERT INTO box_on_jeu (diese_box_on_jeu, num_box_on_jeu, id_jeu, id_pion) VALUES (5, 4, 2, 9);
INSERT INTO box_on_jeu (diese_box_on_jeu, num_box_on_jeu, id_jeu, id_pion) VALUES (6, 5, 2, 10);
INSERT INTO box_on_jeu (diese_box_on_jeu, num_box_on_jeu, id_jeu, id_pion) VALUES (7, 6, 2, 11);
INSERT INTO box_on_jeu (diese_box_on_jeu, num_box_on_jeu, id_jeu, id_pion) VALUES (8, 7, 2, 12);
INSERT INTO box_on_jeu (diese_box_on_jeu, num_box_on_jeu, id_jeu, id_pion) VALUES (9, 8, 2, 13);
INSERT INTO box_on_jeu (diese_box_on_jeu, num_box_on_jeu, id_jeu, id_pion) VALUES (10, 9, 2, 14);
INSERT INTO box_on_jeu (diese_box_on_jeu, num_box_on_jeu, id_jeu, id_pion) VALUES (11, 10, 2, 15);
INSERT INTO box_on_jeu (diese_box_on_jeu, num_box_on_jeu, id_jeu, id_pion) VALUES (12, 11, 2, 16);
INSERT INTO box_on_jeu (diese_box_on_jeu, num_box_on_jeu, id_jeu, id_pion) VALUES (1, 0, 3, 17);
INSERT INTO box_on_jeu (diese_box_on_jeu, num_box_on_jeu, id_jeu, id_pion) VALUES (2, 1, 3, 18);
INSERT INTO box_on_jeu (diese_box_on_jeu, num_box_on_jeu, id_jeu, id_pion) VALUES (1, 0, 4, 17);
INSERT INTO box_on_jeu (diese_box_on_jeu, num_box_on_jeu, id_jeu, id_pion) VALUES (2, 1, 4, 18);
INSERT INTO box_on_jeu (diese_box_on_jeu, num_box_on_jeu, id_jeu, id_pion) VALUES (1, 0, 5, 17);
INSERT INTO box_on_jeu (diese_box_on_jeu, num_box_on_jeu, id_jeu, id_pion) VALUES (2, 1, 5, 18);
INSERT INTO box_on_jeu (diese_box_on_jeu, num_box_on_jeu, id_jeu, id_pion) VALUES (1, 0, 6, 19);
INSERT INTO box_on_jeu (diese_box_on_jeu, num_box_on_jeu, id_jeu, id_pion) VALUES (2, 1, 6, 20);
INSERT INTO box_on_jeu (diese_box_on_jeu, num_box_on_jeu, id_jeu, id_pion) VALUES (3, 2, 6, 21);
INSERT INTO box_on_jeu (diese_box_on_jeu, num_box_on_jeu, id_jeu, id_pion) VALUES (4, 3, 6, 22);
INSERT INTO box_on_jeu (diese_box_on_jeu, num_box_on_jeu, id_jeu, id_pion) VALUES (5, 4, 6, 23);
INSERT INTO box_on_jeu (diese_box_on_jeu, num_box_on_jeu, id_jeu, id_pion) VALUES (6, 5, 6, 24);
INSERT INTO box_on_jeu (diese_box_on_jeu, num_box_on_jeu, id_jeu, id_pion) VALUES (7, 6, 6, 25);
INSERT INTO box_on_jeu (diese_box_on_jeu, num_box_on_jeu, id_jeu, id_pion) VALUES (8, 7, 6, 26);
INSERT INTO box_on_jeu (diese_box_on_jeu, num_box_on_jeu, id_jeu, id_pion) VALUES (9, 8, 6, 27);
INSERT INTO box_on_jeu (diese_box_on_jeu, num_box_on_jeu, id_jeu, id_pion) VALUES (10, 9, 6, 28);
INSERT INTO box_on_jeu (diese_box_on_jeu, num_box_on_jeu, id_jeu, id_pion) VALUES (11, 10, 6, 29);
INSERT INTO box_on_jeu (diese_box_on_jeu, num_box_on_jeu, id_jeu, id_pion) VALUES (12, 11, 6, 30);
INSERT INTO box_on_jeu (diese_box_on_jeu, num_box_on_jeu, id_jeu, id_pion) VALUES (13, 12, 6, 31);
INSERT INTO box_on_jeu (diese_box_on_jeu, num_box_on_jeu, id_jeu, id_pion) VALUES (14, 13, 6, 32);
INSERT INTO box_on_jeu (diese_box_on_jeu, num_box_on_jeu, id_jeu, id_pion) VALUES (15, 14, 6, 33);
INSERT INTO box_on_jeu (diese_box_on_jeu, num_box_on_jeu, id_jeu, id_pion) VALUES (16, 15, 6, 34);
INSERT INTO box_on_jeu (diese_box_on_jeu, num_box_on_jeu, id_jeu, id_pion) VALUES (17, 16, 6, 35);
INSERT INTO box_on_jeu (diese_box_on_jeu, num_box_on_jeu, id_jeu, id_pion) VALUES (18, 17, 6, 36);
INSERT INTO box_on_jeu (diese_box_on_jeu, num_box_on_jeu, id_jeu, id_pion) VALUES (19, 18, 6, 37);
INSERT INTO box_on_jeu (diese_box_on_jeu, num_box_on_jeu, id_jeu, id_pion) VALUES (20, 19, 6, 38);
INSERT INTO box_on_jeu (diese_box_on_jeu, num_box_on_jeu, id_jeu, id_pion) VALUES (21, 20, 6, 39);
INSERT INTO box_on_jeu (diese_box_on_jeu, num_box_on_jeu, id_jeu, id_pion) VALUES (22, 21, 6, 40);
INSERT INTO box_on_jeu (diese_box_on_jeu, num_box_on_jeu, id_jeu, id_pion) VALUES (23, 22, 6, 41);
INSERT INTO box_on_jeu (diese_box_on_jeu, num_box_on_jeu, id_jeu, id_pion) VALUES (24, 23, 6, 42);
INSERT INTO box_on_jeu (diese_box_on_jeu, num_box_on_jeu, id_jeu, id_pion) VALUES (25, 24, 6, 43);
INSERT INTO box_on_jeu (diese_box_on_jeu, num_box_on_jeu, id_jeu, id_pion) VALUES (26, 25, 6, 44);
INSERT INTO box_on_jeu (diese_box_on_jeu, num_box_on_jeu, id_jeu, id_pion) VALUES (27, 26, 6, 45);
INSERT INTO box_on_jeu (diese_box_on_jeu, num_box_on_jeu, id_jeu, id_pion) VALUES (28, 27, 6, 46);
INSERT INTO box_on_jeu (diese_box_on_jeu, num_box_on_jeu, id_jeu, id_pion) VALUES (1, 0, 7, 47);
INSERT INTO box_on_jeu (diese_box_on_jeu, num_box_on_jeu, id_jeu, id_pion) VALUES (2, 1, 7, 48);

INSERT INTO carac_pions_jeu (id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (1, 1, 3, 1, 7);
INSERT INTO carac_pions_jeu (id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (1, 2, 4, 2, 7);
INSERT INTO carac_pions_jeu (id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (1, 3, 1, 1, 7);
INSERT INTO carac_pions_jeu (id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (1, 4, 2, 2, 10);
INSERT INTO carac_pions_jeu (id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (2, 5, 13, 2, 10);
INSERT INTO carac_pions_jeu (id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (2, 6, 14, 1, 10);
INSERT INTO carac_pions_jeu (id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (2, 7, 13, 2, 10);
INSERT INTO carac_pions_jeu (id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (2, 8, 14, 1, 10);
INSERT INTO carac_pions_jeu (id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (2, 9, 13, 2, 10);
INSERT INTO carac_pions_jeu (id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (2, 10, 14, 1, 10);
INSERT INTO carac_pions_jeu (id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (2, 11, 13, 2, 10);
INSERT INTO carac_pions_jeu (id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (2, 12, 14, 1, 10);
INSERT INTO carac_pions_jeu (id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (2, 13, 13, 2, 10);
INSERT INTO carac_pions_jeu (id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (2, 14, 14, 1, 10);
INSERT INTO carac_pions_jeu (id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (2, 15, 13, 2, 10);
INSERT INTO carac_pions_jeu (id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (2, 16, 14, 1, 10);
INSERT INTO carac_pions_jeu (id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (3, 17, 17, 2, 8);
INSERT INTO carac_pions_jeu (id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (3, 18, 18, 1, 8);
INSERT INTO carac_pions_jeu (id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (4, 17, 17, 2, 6);
INSERT INTO carac_pions_jeu (id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (4, 18, 18, 1, 6);
INSERT INTO carac_pions_jeu (id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (5, 17, 17, 2, 4);
INSERT INTO carac_pions_jeu (id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (5, 18, 18, 1, 4);
INSERT INTO carac_pions_jeu (id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (6, 19, 19, 2, 8);
INSERT INTO carac_pions_jeu (id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (6, 20, 20, 1, 8);
INSERT INTO carac_pions_jeu (id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (6, 21, 23, 2, 8);
INSERT INTO carac_pions_jeu (id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (6, 22, 24, 1, 8);
INSERT INTO carac_pions_jeu (id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (6, 23, 22, 2, 8);
INSERT INTO carac_pions_jeu (id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (6, 24, 21, 1, 8);
INSERT INTO carac_pions_jeu (id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (6, 25, 27, 2, 8);
INSERT INTO carac_pions_jeu (id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (6, 26, 28, 1, 8);
INSERT INTO carac_pions_jeu (id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (6, 27, 26, 2, 8);
INSERT INTO carac_pions_jeu (id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (6, 28, 25, 1, 8);
INSERT INTO carac_pions_jeu (id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (6, 29, 30, 2, 8);
INSERT INTO carac_pions_jeu (id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (6, 30, 29, 1, 8);
INSERT INTO carac_pions_jeu (id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (6, 31, 33, 2, 8);
INSERT INTO carac_pions_jeu (id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (6, 32, 34, 1, 8);
INSERT INTO carac_pions_jeu (id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (6, 33, 32, 2, 8);
INSERT INTO carac_pions_jeu (id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (6, 34, 31, 1, 8);
INSERT INTO carac_pions_jeu (id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (6, 35, 37, 2, 8);
INSERT INTO carac_pions_jeu (id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (6, 36, 38, 1, 8);
INSERT INTO carac_pions_jeu (id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (6, 37, 36, 2, 8);
INSERT INTO carac_pions_jeu (id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (6, 38, 35, 1, 8);
INSERT INTO carac_pions_jeu (id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (6, 39, 41, 2, 8);
INSERT INTO carac_pions_jeu (id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (6, 40, 42, 1, 8);
INSERT INTO carac_pions_jeu (id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (6, 41, 40, 2, 8);
INSERT INTO carac_pions_jeu (id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (6, 42, 39, 1, 8);
INSERT INTO carac_pions_jeu (id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (6, 43, 45, 2, 8);
INSERT INTO carac_pions_jeu (id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (6, 44, 46, 1, 8);
INSERT INTO carac_pions_jeu (id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (6, 45, 44, 2, 8);
INSERT INTO carac_pions_jeu (id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (6, 46, 43, 1, 8);
INSERT INTO carac_pions_jeu (id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (7, 47, 47, 1, 12);
INSERT INTO carac_pions_jeu (id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (7, 48, 48, 1, 12);
INSERT INTO carac_pions_jeu (id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (6, 49, 49, 1, 8);
INSERT INTO carac_pions_jeu (id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (6, 50, 50, 2, 8);

INSERT INTO carac_pions_joueur_on_jeu (id_joueur, id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (1, 1, 1, 3, 1, 7);
INSERT INTO carac_pions_joueur_on_jeu (id_joueur, id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (1, 1, 2, 4, 2, 7);
INSERT INTO carac_pions_joueur_on_jeu (id_joueur, id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (1, 1, 3, 1, 1, 7);
INSERT INTO carac_pions_joueur_on_jeu (id_joueur, id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (1, 1, 4, 2, 2, 7);
INSERT INTO carac_pions_joueur_on_jeu (id_joueur, id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (2, 1, 1, 3, 1, 7);
INSERT INTO carac_pions_joueur_on_jeu (id_joueur, id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (2, 1, 2, 4, 2, 7);
INSERT INTO carac_pions_joueur_on_jeu (id_joueur, id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (2, 1, 3, 1, 1, 7);
INSERT INTO carac_pions_joueur_on_jeu (id_joueur, id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (2, 1, 4, 2, 2, 7);
INSERT INTO carac_pions_joueur_on_jeu (id_joueur, id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (1, 2, 5, 13, 2, 10);
INSERT INTO carac_pions_joueur_on_jeu (id_joueur, id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (1, 2, 6, 14, 1, 10);
INSERT INTO carac_pions_joueur_on_jeu (id_joueur, id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (1, 2, 7, 7, 2, 10);
INSERT INTO carac_pions_joueur_on_jeu (id_joueur, id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (1, 2, 8, 8, 1, 10);
INSERT INTO carac_pions_joueur_on_jeu (id_joueur, id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (1, 2, 9, 9, 2, 10);
INSERT INTO carac_pions_joueur_on_jeu (id_joueur, id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (1, 2, 10, 10, 1, 10);
INSERT INTO carac_pions_joueur_on_jeu (id_joueur, id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (1, 2, 11, 11, 2, 10);
INSERT INTO carac_pions_joueur_on_jeu (id_joueur, id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (1, 2, 12, 12, 1, 10);
INSERT INTO carac_pions_joueur_on_jeu (id_joueur, id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (1, 2, 13, 13, 2, 10);
INSERT INTO carac_pions_joueur_on_jeu (id_joueur, id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (1, 2, 14, 14, 1, 10);
INSERT INTO carac_pions_joueur_on_jeu (id_joueur, id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (1, 2, 15, 15, 2, 10);
INSERT INTO carac_pions_joueur_on_jeu (id_joueur, id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (1, 2, 16, 16, 1, 10);
INSERT INTO carac_pions_joueur_on_jeu (id_joueur, id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (2, 2, 5, 13, 2, 10);
INSERT INTO carac_pions_joueur_on_jeu (id_joueur, id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (2, 2, 6, 14, 1, 10);
INSERT INTO carac_pions_joueur_on_jeu (id_joueur, id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (2, 2, 7, 7, 2, 10);
INSERT INTO carac_pions_joueur_on_jeu (id_joueur, id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (2, 2, 8, 8, 1, 10);
INSERT INTO carac_pions_joueur_on_jeu (id_joueur, id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (2, 2, 9, 9, 2, 10);
INSERT INTO carac_pions_joueur_on_jeu (id_joueur, id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (2, 2, 10, 10, 1, 10);
INSERT INTO carac_pions_joueur_on_jeu (id_joueur, id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (2, 2, 11, 11, 2, 10);
INSERT INTO carac_pions_joueur_on_jeu (id_joueur, id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (2, 2, 12, 12, 1, 10);
INSERT INTO carac_pions_joueur_on_jeu (id_joueur, id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (2, 2, 13, 13, 2, 10);
INSERT INTO carac_pions_joueur_on_jeu (id_joueur, id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (2, 2, 14, 14, 1, 10);
INSERT INTO carac_pions_joueur_on_jeu (id_joueur, id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (2, 2, 15, 15, 2, 10);
INSERT INTO carac_pions_joueur_on_jeu (id_joueur, id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (2, 2, 16, 16, 1, 10);
INSERT INTO carac_pions_joueur_on_jeu (id_joueur, id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (1, 3, 17, 17, 2, 8);
INSERT INTO carac_pions_joueur_on_jeu (id_joueur, id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (1, 3, 18, 18, 1, 8);
INSERT INTO carac_pions_joueur_on_jeu (id_joueur, id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (2, 3, 17, 17, 2, 8);
INSERT INTO carac_pions_joueur_on_jeu (id_joueur, id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (2, 3, 18, 18, 1, 8);
INSERT INTO carac_pions_joueur_on_jeu (id_joueur, id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (1, 4, 17, 17, 2, 6);
INSERT INTO carac_pions_joueur_on_jeu (id_joueur, id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (1, 4, 18, 18, 1, 6);
INSERT INTO carac_pions_joueur_on_jeu (id_joueur, id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (2, 4, 17, 17, 2, 6);
INSERT INTO carac_pions_joueur_on_jeu (id_joueur, id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (2, 4, 18, 18, 1, 6);
INSERT INTO carac_pions_joueur_on_jeu (id_joueur, id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (1, 5, 17, 17, 2, 4);
INSERT INTO carac_pions_joueur_on_jeu (id_joueur, id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (1, 5, 18, 18, 1, 4);
INSERT INTO carac_pions_joueur_on_jeu (id_joueur, id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (2, 5, 17, 17, 2, 4);
INSERT INTO carac_pions_joueur_on_jeu (id_joueur, id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (2, 5, 18, 18, 1, 4);
INSERT INTO carac_pions_joueur_on_jeu (id_joueur, id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (1, 6, 19, 19, 2, 8);
INSERT INTO carac_pions_joueur_on_jeu (id_joueur, id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (1, 6, 20, 20, 1, 8);
INSERT INTO carac_pions_joueur_on_jeu (id_joueur, id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (1, 6, 21, 23, 2, 8);
INSERT INTO carac_pions_joueur_on_jeu (id_joueur, id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (1, 6, 22, 24, 1, 8);
INSERT INTO carac_pions_joueur_on_jeu (id_joueur, id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (1, 6, 23, 22, 2, 8);
INSERT INTO carac_pions_joueur_on_jeu (id_joueur, id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (1, 6, 24, 21, 1, 8);
INSERT INTO carac_pions_joueur_on_jeu (id_joueur, id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (1, 6, 25, 27, 2, 8);
INSERT INTO carac_pions_joueur_on_jeu (id_joueur, id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (1, 6, 26, 28, 1, 8);
INSERT INTO carac_pions_joueur_on_jeu (id_joueur, id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (1, 6, 27, 26, 2, 8);
INSERT INTO carac_pions_joueur_on_jeu (id_joueur, id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (1, 6, 28, 25, 1, 8);
INSERT INTO carac_pions_joueur_on_jeu (id_joueur, id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (1, 6, 29, 30, 2, 8);
INSERT INTO carac_pions_joueur_on_jeu (id_joueur, id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (1, 6, 30, 29, 1, 8);
INSERT INTO carac_pions_joueur_on_jeu (id_joueur, id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (1, 6, 31, 33, 2, 8);
INSERT INTO carac_pions_joueur_on_jeu (id_joueur, id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (1, 6, 32, 34, 1, 8);
INSERT INTO carac_pions_joueur_on_jeu (id_joueur, id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (1, 6, 33, 32, 2, 8);
INSERT INTO carac_pions_joueur_on_jeu (id_joueur, id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (1, 6, 34, 31, 1, 8);
INSERT INTO carac_pions_joueur_on_jeu (id_joueur, id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (1, 6, 35, 37, 2, 8);
INSERT INTO carac_pions_joueur_on_jeu (id_joueur, id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (1, 6, 36, 38, 1, 8);
INSERT INTO carac_pions_joueur_on_jeu (id_joueur, id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (1, 6, 37, 36, 2, 8);
INSERT INTO carac_pions_joueur_on_jeu (id_joueur, id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (1, 6, 38, 35, 1, 8);
INSERT INTO carac_pions_joueur_on_jeu (id_joueur, id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (1, 6, 39, 41, 2, 8);
INSERT INTO carac_pions_joueur_on_jeu (id_joueur, id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (1, 6, 40, 42, 1, 8);
INSERT INTO carac_pions_joueur_on_jeu (id_joueur, id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (1, 6, 41, 40, 2, 8);
INSERT INTO carac_pions_joueur_on_jeu (id_joueur, id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (1, 6, 42, 39, 1, 8);
INSERT INTO carac_pions_joueur_on_jeu (id_joueur, id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (1, 6, 43, 45, 2, 8);
INSERT INTO carac_pions_joueur_on_jeu (id_joueur, id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (1, 6, 44, 46, 1, 8);
INSERT INTO carac_pions_joueur_on_jeu (id_joueur, id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (1, 6, 45, 44, 2, 8);
INSERT INTO carac_pions_joueur_on_jeu (id_joueur, id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (1, 6, 46, 43, 1, 8);
INSERT INTO carac_pions_joueur_on_jeu (id_joueur, id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (2, 6, 19, 19, 2, 8);
INSERT INTO carac_pions_joueur_on_jeu (id_joueur, id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (2, 6, 20, 20, 1, 8);
INSERT INTO carac_pions_joueur_on_jeu (id_joueur, id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (2, 6, 21, 23, 2, 8);
INSERT INTO carac_pions_joueur_on_jeu (id_joueur, id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (2, 6, 22, 24, 1, 8);
INSERT INTO carac_pions_joueur_on_jeu (id_joueur, id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (2, 6, 23, 22, 2, 8);
INSERT INTO carac_pions_joueur_on_jeu (id_joueur, id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (2, 6, 24, 21, 1, 8);
INSERT INTO carac_pions_joueur_on_jeu (id_joueur, id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (2, 6, 25, 27, 2, 8);
INSERT INTO carac_pions_joueur_on_jeu (id_joueur, id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (2, 6, 26, 28, 1, 8);
INSERT INTO carac_pions_joueur_on_jeu (id_joueur, id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (2, 6, 27, 26, 2, 8);
INSERT INTO carac_pions_joueur_on_jeu (id_joueur, id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (2, 6, 28, 25, 1, 8);
INSERT INTO carac_pions_joueur_on_jeu (id_joueur, id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (2, 6, 29, 30, 2, 8);
INSERT INTO carac_pions_joueur_on_jeu (id_joueur, id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (2, 6, 30, 29, 1, 8);
INSERT INTO carac_pions_joueur_on_jeu (id_joueur, id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (2, 6, 31, 33, 2, 8);
INSERT INTO carac_pions_joueur_on_jeu (id_joueur, id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (2, 6, 32, 34, 1, 8);
INSERT INTO carac_pions_joueur_on_jeu (id_joueur, id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (2, 6, 33, 32, 2, 8);
INSERT INTO carac_pions_joueur_on_jeu (id_joueur, id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (2, 6, 34, 31, 1, 8);
INSERT INTO carac_pions_joueur_on_jeu (id_joueur, id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (2, 6, 35, 37, 2, 8);
INSERT INTO carac_pions_joueur_on_jeu (id_joueur, id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (2, 6, 36, 38, 1, 8);
INSERT INTO carac_pions_joueur_on_jeu (id_joueur, id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (2, 6, 37, 36, 2, 8);
INSERT INTO carac_pions_joueur_on_jeu (id_joueur, id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (2, 6, 38, 35, 1, 8);
INSERT INTO carac_pions_joueur_on_jeu (id_joueur, id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (2, 6, 39, 41, 2, 8);
INSERT INTO carac_pions_joueur_on_jeu (id_joueur, id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (2, 6, 40, 42, 1, 8);
INSERT INTO carac_pions_joueur_on_jeu (id_joueur, id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (2, 6, 41, 40, 2, 8);
INSERT INTO carac_pions_joueur_on_jeu (id_joueur, id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (2, 6, 42, 39, 1, 8);
INSERT INTO carac_pions_joueur_on_jeu (id_joueur, id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (2, 6, 43, 45, 2, 8);
INSERT INTO carac_pions_joueur_on_jeu (id_joueur, id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (2, 6, 44, 46, 1, 8);
INSERT INTO carac_pions_joueur_on_jeu (id_joueur, id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (2, 6, 45, 44, 2, 8);
INSERT INTO carac_pions_joueur_on_jeu (id_joueur, id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (2, 6, 46, 43, 1, 8);
INSERT INTO carac_pions_joueur_on_jeu (id_joueur, id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (1, 7, 47, 47, 1, 12);
INSERT INTO carac_pions_joueur_on_jeu (id_joueur, id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (1, 7, 48, 48, 1, 12);
INSERT INTO carac_pions_joueur_on_jeu (id_joueur, id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (2, 7, 47, 47, 1, 12);
INSERT INTO carac_pions_joueur_on_jeu (id_joueur, id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (2, 7, 48, 48, 1, 12);
INSERT INTO carac_pions_joueur_on_jeu (id_joueur, id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (2, 6, 49, 49, 1, 8);
INSERT INTO carac_pions_joueur_on_jeu (id_joueur, id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (1, 6, 50, 50, 2, 8);
INSERT INTO carac_pions_joueur_on_jeu (id_joueur, id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (1, 6, 49, 49, 1, 8);
INSERT INTO carac_pions_joueur_on_jeu (id_joueur, id_jeu, id_pion, change_pion, diese_bac_on_jeu, taille) VALUES (2, 6, 50, 50, 2, 8);

INSERT INTO jeu (id_jeu, nom_jeu, nombre_trashs, wiki) VALUES (1, 'dame', 1, 'https://fr.wikipedia.org/wiki/Dames');
INSERT INTO jeu (id_jeu, nom_jeu, nombre_trashs, wiki) VALUES (2, 'echec', 1, 'https://fr.wikipedia.org/wiki/%C3%89checs');
INSERT INTO jeu (id_jeu, nom_jeu, nombre_trashs, wiki) VALUES (3, 'go (9x9)', 1, 'https://fr.wikipedia.org/wiki/Go_(jeu)');
INSERT INTO jeu (id_jeu, nom_jeu, nombre_trashs, wiki) VALUES (4, 'go (13x13)', 1, 'https://fr.wikipedia.org/wiki/Go_(jeu)');
INSERT INTO jeu (id_jeu, nom_jeu, nombre_trashs, wiki) VALUES (5, 'go (19x19)', 1, 'https://fr.wikipedia.org/wiki/Go_(jeu)');
INSERT INTO jeu (id_jeu, nom_jeu, nombre_trashs, wiki) VALUES (6, 'shogi', 1, 'https://fr.wikipedia.org/wiki/Sh%C5%8Dgi');
INSERT INTO jeu (id_jeu, nom_jeu, nombre_trashs, wiki) VALUES (7, 'bagh chal', 1, 'https://fr.wikipedia.org/wiki/Bagh_Chal');

INSERT INTO joueur_on_jeu (id_jeu, id_joueur, label_joueur, action_clic_onboard, action_scroll_onboard, clickable_board, click_board_create_type, click_board_move_bac, click_board_move_type) VALUES (1, 1, 'blanc', 1, 2, 4, 1, 1, 1);
INSERT INTO joueur_on_jeu (id_jeu, id_joueur, label_joueur, action_clic_onboard, action_scroll_onboard, clickable_board, click_board_create_type, click_board_move_bac, click_board_move_type) VALUES (1, 2, 'noir', 1, 2, 4, 2, 2, 2);
INSERT INTO joueur_on_jeu (id_jeu, id_joueur, label_joueur, action_clic_onboard, action_scroll_onboard, clickable_board, click_board_create_type, click_board_move_bac, click_board_move_type) VALUES (2, 1, 'blanc', 1, 2, 7, 13, 13, 1);
INSERT INTO joueur_on_jeu (id_jeu, id_joueur, label_joueur, action_clic_onboard, action_scroll_onboard, clickable_board, click_board_create_type, click_board_move_bac, click_board_move_type) VALUES (2, 2, 'noir', 1, 2, 7, 14, 14, 2);
INSERT INTO joueur_on_jeu (id_jeu, id_joueur, label_joueur, action_clic_onboard, action_scroll_onboard, clickable_board, click_board_create_type, click_board_move_bac, click_board_move_type) VALUES (3, 1, 'blanc', 3, 2, 4, 18, 18, 1);
INSERT INTO joueur_on_jeu (id_jeu, id_joueur, label_joueur, action_clic_onboard, action_scroll_onboard, clickable_board, click_board_create_type, click_board_move_bac, click_board_move_type) VALUES (3, 2, 'noir', 3, 2, 4, 17, 17, 2);
INSERT INTO joueur_on_jeu (id_jeu, id_joueur, label_joueur, action_clic_onboard, action_scroll_onboard, clickable_board, click_board_create_type, click_board_move_bac, click_board_move_type) VALUES (4, 1, 'blanc', 3, 2, 4, 18, 18, 1);
INSERT INTO joueur_on_jeu (id_jeu, id_joueur, label_joueur, action_clic_onboard, action_scroll_onboard, clickable_board, click_board_create_type, click_board_move_bac, click_board_move_type) VALUES (4, 2, 'noir', 3, 2, 4, 17, 17, 2);
INSERT INTO joueur_on_jeu (id_jeu, id_joueur, label_joueur, action_clic_onboard, action_scroll_onboard, clickable_board, click_board_create_type, click_board_move_bac, click_board_move_type) VALUES (5, 1, 'blanc', 3, 2, 4, 18, 18, 1);
INSERT INTO joueur_on_jeu (id_jeu, id_joueur, label_joueur, action_clic_onboard, action_scroll_onboard, clickable_board, click_board_create_type, click_board_move_bac, click_board_move_type) VALUES (5, 2, 'noir', 3, 2, 4, 17, 17, 2);
INSERT INTO joueur_on_jeu (id_jeu, id_joueur, label_joueur, action_clic_onboard, action_scroll_onboard, clickable_board, click_board_create_type, click_board_move_bac, click_board_move_type) VALUES (6, 1, 'roi', 6, 2, 7, 19, 43, 1);
INSERT INTO joueur_on_jeu (id_jeu, id_joueur, label_joueur, action_clic_onboard, action_scroll_onboard, clickable_board, click_board_create_type, click_board_move_bac, click_board_move_type) VALUES (6, 2, 'général de jade', 6, 2, 7, 20, 44, 2);
INSERT INTO joueur_on_jeu (id_jeu, id_joueur, label_joueur, action_clic_onboard, action_scroll_onboard, clickable_board, click_board_create_type, click_board_move_bac, click_board_move_type) VALUES (7, 1, 'tigres', 3, 2, 5, 47, 48, 2);
INSERT INTO joueur_on_jeu (id_jeu, id_joueur, label_joueur, action_clic_onboard, action_scroll_onboard, clickable_board, click_board_create_type, click_board_move_bac, click_board_move_type) VALUES (7, 2, 'chèvres', 3, 2, 5, 47, 48, 2);

INSERT INTO pion (id_pion, svg_pion, nom_pion, taille, directed) VALUES (5, 'echec_pion_blanc.svg', 'echec_pion_blanc', 10, false);
INSERT INTO pion (id_pion, svg_pion, nom_pion, taille, directed) VALUES (6, 'echec_pion_noir.svg', 'echec_pion_noir', 10, false);
INSERT INTO pion (id_pion, svg_pion, nom_pion, taille, directed) VALUES (7, 'echec_fou_blanc.svg', 'echec_fou_blanc', 10, false);
INSERT INTO pion (id_pion, svg_pion, nom_pion, taille, directed) VALUES (8, 'echec_fou_noir.svg', 'echec_fou_noir', 10, false);
INSERT INTO pion (id_pion, svg_pion, nom_pion, taille, directed) VALUES (9, 'echec_cavalier_blanc.svg', 'echec_cavalier_blanc', 10, false);
INSERT INTO pion (id_pion, svg_pion, nom_pion, taille, directed) VALUES (10, 'echec_cavalier_noir.svg', 'echec_cavalier_noir', 10, false);
INSERT INTO pion (id_pion, svg_pion, nom_pion, taille, directed) VALUES (11, 'echec_tour_blanc.svg', 'echec_tour_blanc', 10, false);
INSERT INTO pion (id_pion, svg_pion, nom_pion, taille, directed) VALUES (12, 'echec_tour_noir.svg', 'echec_tour_noir', 10, false);
INSERT INTO pion (id_pion, svg_pion, nom_pion, taille, directed) VALUES (13, 'echec_dame_blanc.svg', 'echec_dame_blanc', 10, false);
INSERT INTO pion (id_pion, svg_pion, nom_pion, taille, directed) VALUES (14, 'echec_dame_noir.svg', 'echec_dame_noir', 10, false);
INSERT INTO pion (id_pion, svg_pion, nom_pion, taille, directed) VALUES (15, 'echec_roi_blanc.svg', 'echec_roi_blanc', 10, false);
INSERT INTO pion (id_pion, svg_pion, nom_pion, taille, directed) VALUES (16, 'echec_roi_noir.svg', 'echec_roi_noir', 10, false);
INSERT INTO pion (id_pion, svg_pion, nom_pion, taille, directed) VALUES (17, 'go_noir.svg', 'go_noir', 6, false);
INSERT INTO pion (id_pion, svg_pion, nom_pion, taille, directed) VALUES (18, 'go_blanc.svg', 'go_blanc', 6, false);
INSERT INTO pion (id_pion, svg_pion, nom_pion, taille, directed) VALUES (19, 'shogi_roi_nord.svg', 'shogi_roi_nord', 8, true);
INSERT INTO pion (id_pion, svg_pion, nom_pion, taille, directed) VALUES (20, 'shogi_jade_sud.svg', 'shogi_jade_sud', 8, true);
INSERT INTO pion (id_pion, svg_pion, nom_pion, taille, directed) VALUES (21, 'shogi_tour_nord.svg', 'shogi_tour_nord', 8, true);
INSERT INTO pion (id_pion, svg_pion, nom_pion, taille, directed) VALUES (22, 'shogi_tour_sud.svg', 'shogi_tour_sud', 8, true);
INSERT INTO pion (id_pion, svg_pion, nom_pion, taille, directed) VALUES (23, 'shogi_dragon_nord.svg', 'shogi_tour_nord_p', 8, true);
INSERT INTO pion (id_pion, svg_pion, nom_pion, taille, directed) VALUES (24, 'shogi_dragon_sud.svg', 'shogi_tour_sud_p', 8, true);
INSERT INTO pion (id_pion, svg_pion, nom_pion, taille, directed) VALUES (25, 'shogi_fou_nord.svg', 'shogi_fou_nord', 8, true);
INSERT INTO pion (id_pion, svg_pion, nom_pion, taille, directed) VALUES (26, 'shogi_fou_sud.svg', 'shogi_fou_sud', 8, true);
INSERT INTO pion (id_pion, svg_pion, nom_pion, taille, directed) VALUES (27, 'shogi_cheval_dragon_nord.svg', 'shogi_fou_nord_p', 8, true);
INSERT INTO pion (id_pion, svg_pion, nom_pion, taille, directed) VALUES (28, 'shogi_cheval_dragon_sud.svg', 'shogi_fou_sud_p', 8, true);
INSERT INTO pion (id_pion, svg_pion, nom_pion, taille, directed) VALUES (29, 'shogi_general_or_nord.svg', 'shogi_or_nord', 8, true);
INSERT INTO pion (id_pion, svg_pion, nom_pion, taille, directed) VALUES (30, 'shogi_general_or_sud.svg', 'shogi_or_sud', 8, true);
INSERT INTO pion (id_pion, svg_pion, nom_pion, taille, directed) VALUES (31, 'shogi_general_argent_nord.svg', 'shogi_argent_nord', 8, true);
INSERT INTO pion (id_pion, svg_pion, nom_pion, taille, directed) VALUES (32, 'shogi_general_argent_sud.svg', 'shogi_argent_sud', 8, true);
INSERT INTO pion (id_pion, svg_pion, nom_pion, taille, directed) VALUES (33, 'shogi_argent_or_nord.svg', 'shogi_argent_nord_p', 8, true);
INSERT INTO pion (id_pion, svg_pion, nom_pion, taille, directed) VALUES (34, 'shogi_argent_or_sud.svg', 'shogi_argent_sud_p', 8, true);
INSERT INTO pion (id_pion, svg_pion, nom_pion, taille, directed) VALUES (35, 'shogi_cavalier_nord.svg', 'shogi_cavalier_nord', 8, true);
INSERT INTO pion (id_pion, svg_pion, nom_pion, taille, directed) VALUES (36, 'shogi_cavalier_sud.svg', 'shogi_cavalier_sud', 8, true);
INSERT INTO pion (id_pion, svg_pion, nom_pion, taille, directed) VALUES (37, 'shogi_cavalier_or_nord.svg', 'shogi_cavalier_nord_p', 8, true);
INSERT INTO pion (id_pion, svg_pion, nom_pion, taille, directed) VALUES (38, 'shogi_cavalier_or_sud.svg', 'shogi_cavalier_sud_p', 8, true);
INSERT INTO pion (id_pion, svg_pion, nom_pion, taille, directed) VALUES (39, 'shogi_lance_nord.svg', 'shogi_lance_nord', 8, true);
INSERT INTO pion (id_pion, svg_pion, nom_pion, taille, directed) VALUES (40, 'shogi_lance_sud.svg', 'shogi_lance_sud', 8, true);
INSERT INTO pion (id_pion, svg_pion, nom_pion, taille, directed) VALUES (41, 'shogi_lance_or_nord.svg', 'shogi_lance_nord_p', 8, true);
INSERT INTO pion (id_pion, svg_pion, nom_pion, taille, directed) VALUES (42, 'shogi_lance_or_sud.svg', 'shogi_lance_sud_p', 8, true);
INSERT INTO pion (id_pion, svg_pion, nom_pion, taille, directed) VALUES (43, 'shogi_pion_nord.svg', 'shogi_pion_nord', 8, true);
INSERT INTO pion (id_pion, svg_pion, nom_pion, taille, directed) VALUES (44, 'shogi_pion_sud.svg', 'shogi_pion_sud', 8, true);
INSERT INTO pion (id_pion, svg_pion, nom_pion, taille, directed) VALUES (45, 'shogi_pion_or_nord.svg', 'shogi_pion_nord_p', 8, true);
INSERT INTO pion (id_pion, svg_pion, nom_pion, taille, directed) VALUES (46, 'shogi_pion_or_sud.svg', 'shogi_pion_sud_p', 8, true);
INSERT INTO pion (id_pion, svg_pion, nom_pion, taille, directed) VALUES (1, 'dame_blanc.svg', 'dame_blanc', 7, false);
INSERT INTO pion (id_pion, svg_pion, nom_pion, taille, directed) VALUES (2, 'dame_noir.svg', 'dame_noir', 7, false);
INSERT INTO pion (id_pion, svg_pion, nom_pion, taille, directed) VALUES (3, 'dame_dame_blanc.svg', 'dame_dame_blanc', 7, false);
INSERT INTO pion (id_pion, svg_pion, nom_pion, taille, directed) VALUES (48, 'baghChal_chevre.svg', 'baghChal_chevre', 7, false);
INSERT INTO pion (id_pion, svg_pion, nom_pion, taille, directed) VALUES (4, 'dame_dame_noir.svg', 'dame_dame_noir', 7, false);
INSERT INTO pion (id_pion, svg_pion, nom_pion, taille, directed) VALUES (47, 'baghChal_tigre.svg', 'baghChal_tigre', 7, false);
INSERT INTO pion (id_pion, svg_pion, nom_pion, taille, directed) VALUES (49, 'shogi_roi_sud.svg', 'shogi_roi_sud', 8, true);
INSERT INTO pion (id_pion, svg_pion, nom_pion, taille, directed) VALUES (50, 'shogi_jade_nord.svg', 'shogi_jade_nord', 8, true);

INSERT INTO pion_on_bac_on_jeu (diese_bac_on_jeu, id_jeu, num_pion_on_bac_on_jeu, diese_pion_on_bac_on_jeu, id_pion, nombre_pions) VALUES (2, 7, 0, 1, 48, 20);

INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 1, 1, 0, 1.5, 91.5, 7, 1);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 1, 2, 1, 21.5, 91.5, 7, 1);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 1, 3, 2, 41.5, 91.5, 7, 1);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 1, 4, 3, 61.5, 91.5, 7, 1);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 1, 5, 4, 81.5, 91.5, 7, 1);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 1, 6, 5, 11.5, 81.5, 7, 1);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 1, 7, 6, 31.5, 81.5, 7, 1);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 1, 8, 7, 51.5, 81.5, 7, 1);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 1, 9, 8, 71.5, 81.5, 7, 1);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 1, 10, 9, 91.5, 81.5, 7, 1);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 1, 11, 10, 1.5, 71.5, 7, 1);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 1, 12, 11, 21.5, 71.5, 7, 1);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 1, 13, 12, 41.5, 71.5, 7, 1);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 1, 14, 13, 61.5, 71.5, 7, 1);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 1, 15, 14, 81.5, 71.5, 7, 1);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 1, 16, 15, 11.5, 61.5, 7, 1);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 1, 17, 16, 31.5, 61.5, 7, 1);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 1, 18, 17, 51.5, 61.5, 7, 1);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 1, 19, 18, 71.5, 61.5, 7, 1);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 1, 20, 19, 91.5, 61.5, 7, 1);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 1, 21, 20, 1.5, 31.5, 7, 2);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 1, 22, 21, 21.5, 31.5, 7, 2);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 1, 23, 22, 41.5, 31.5, 7, 2);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 1, 24, 23, 61.5, 31.5, 7, 2);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 1, 25, 24, 81.5, 31.5, 7, 2);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 1, 26, 25, 11.5, 21.5, 7, 2);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 1, 27, 26, 31.5, 21.5, 7, 2);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 1, 28, 27, 51.5, 21.5, 7, 2);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 1, 29, 28, 71.5, 21.5, 7, 2);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 1, 30, 29, 91.5, 21.5, 7, 2);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 1, 31, 30, 1.5, 11.5, 7, 2);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 1, 32, 31, 21.5, 11.5, 7, 2);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 1, 33, 32, 41.5, 11.5, 7, 2);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 1, 34, 33, 61.5, 11.5, 7, 2);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 1, 35, 34, 81.5, 11.5, 7, 2);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 1, 36, 35, 11.5, 1.5, 7, 2);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 1, 37, 36, 31.5, 1.5, 7, 2);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 1, 38, 37, 51.5, 1.5, 7, 2);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 1, 39, 38, 71.5, 1.5, 7, 2);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 1, 40, 39, 91.5, 1.5, 7, 2);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 2, 1, 0, 2, 14.5, 10, 6);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 2, 2, 1, 14.5, 14.5, 10, 6);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 2, 3, 2, 27, 14.5, 10, 6);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 2, 4, 3, 39.5, 14.5, 10, 6);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 2, 5, 4, 52, 14.5, 10, 6);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 2, 6, 5, 64.5, 14.5, 10, 6);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 2, 7, 6, 77, 14.5, 10, 6);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 2, 8, 7, 89.5, 14.5, 10, 6);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 2, 9, 8, 27, 2, 10, 8);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 2, 10, 9, 64.5, 2, 10, 8);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 2, 11, 10, 14.5, 2, 10, 10);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 2, 12, 11, 77, 2, 10, 10);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 2, 13, 12, 2, 2, 10, 12);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 2, 14, 13, 89.5, 2, 10, 12);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 2, 15, 14, 39.5, 2, 10, 14);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 2, 16, 15, 52, 2, 10, 16);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 2, 17, 16, 2, 77, 10, 5);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 2, 18, 17, 14.5, 77, 10, 5);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 2, 19, 18, 27, 77, 10, 5);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 2, 20, 19, 39.5, 77, 10, 5);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 2, 21, 20, 52, 77, 10, 5);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 2, 22, 21, 64.5, 77, 10, 5);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 2, 23, 22, 77, 77, 10, 5);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 2, 24, 23, 89.5, 77, 10, 5);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 2, 25, 24, 27, 89.5, 10, 7);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 2, 26, 25, 64.5, 89.5, 10, 7);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 2, 27, 26, 14.5, 89.5, 10, 9);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 2, 28, 27, 77, 89.5, 10, 9);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 2, 29, 28, 2, 89.5, 10, 11);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 2, 30, 29, 89.5, 89.5, 10, 11);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 2, 31, 30, 39.5, 89.5, 10, 13);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 2, 32, 31, 52, 89.5, 10, 15);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 6, 1, 0, 5.19999981, 85.1999969, 9, 40);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 6, 2, 1, 15.1999998, 85.1999969, 9, 36);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 6, 3, 2, 25.2000008, 85.1999969, 9, 32);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 6, 4, 3, 35.2000008, 85.1999969, 9, 30);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 6, 5, 4, 45.2000008, 85.1999969, 9, 20);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 6, 6, 5, 55.2000008, 85.1999969, 9, 30);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 6, 7, 6, 65.1999969, 85.1999969, 9, 32);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 6, 8, 7, 75.1999969, 85.1999969, 9, 36);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 6, 9, 8, 85.1999969, 85.1999969, 9, 40);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 6, 10, 9, 15.1999998, 75.1999969, 9, 26);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 6, 11, 10, 75.1999969, 75.1999969, 9, 22);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 6, 12, 11, 5.19999981, 65.1999969, 9, 44);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 6, 13, 12, 15.1999998, 65.1999969, 9, 44);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 6, 14, 13, 25.2000008, 65.1999969, 9, 44);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 6, 15, 14, 35.2000008, 65.1999969, 9, 44);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 6, 16, 15, 45.2000008, 65.1999969, 9, 44);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 6, 17, 16, 55.2000008, 65.1999969, 9, 44);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 6, 18, 17, 65.1999969, 65.1999969, 9, 44);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 6, 19, 18, 75.1999969, 65.1999969, 9, 44);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 6, 20, 19, 85.1999969, 65.1999969, 9, 44);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 6, 21, 20, 5.19999981, 5.19999981, 9, 39);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 6, 22, 21, 15.1999998, 5.19999981, 9, 35);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 6, 23, 22, 25.2000008, 5.19999981, 9, 31);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 6, 24, 23, 35.2000008, 5.19999981, 9, 29);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 6, 25, 24, 45.2000008, 5.19999981, 9, 19);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 6, 26, 25, 55.2000008, 5.19999981, 9, 29);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 6, 27, 26, 65.1999969, 5.19999981, 9, 31);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 6, 28, 27, 75.1999969, 5.19999981, 9, 35);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 6, 29, 28, 85.1999969, 5.19999981, 9, 39);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 6, 30, 29, 15.1999998, 15.1999998, 9, 21);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 6, 31, 30, 75.1999969, 15.1999998, 9, 25);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 6, 32, 31, 5.19999981, 25.2000008, 9, 43);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 6, 33, 32, 15.1999998, 25.2000008, 9, 43);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 6, 34, 33, 25.2000008, 25.2000008, 9, 43);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 6, 35, 34, 35.2000008, 25.2000008, 9, 43);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 6, 36, 35, 45.2000008, 25.2000008, 9, 43);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 6, 37, 36, 55.2000008, 25.2000008, 9, 43);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 6, 38, 37, 65.1999969, 25.2000008, 9, 43);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 6, 39, 38, 75.1999969, 25.2000008, 9, 43);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 6, 40, 39, 85.1999969, 25.2000008, 9, 43);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 7, 1, 0, 4, 4, 12, 47);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 7, 2, 1, 4, 84, 12, 47);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 7, 3, 2, 84, 4, 12, 47);
INSERT INTO pion_on_board_on_jeu (diese_board_on_jeu, id_jeu, diese_pion_on_board_on_jeu, num_pion_on_board_on_jeu, position_x, position_y, taille, id_pion) VALUES (1, 7, 4, 3, 84, 84, 12, 47);

ALTER TABLE ONLY bac_on_jeu
    ADD CONSTRAINT pk_bac_on_jeu PRIMARY KEY (diese_bac_on_jeu, id_jeu);

ALTER TABLE ONLY bac_on_page
    ADD CONSTRAINT pk_bac_on_page PRIMARY KEY (num_bac_on_page, id_page);


ALTER TABLE ONLY bdd_fonctions
    ADD CONSTRAINT pk_bdd_fonction PRIMARY KEY (id_fonction);

ALTER TABLE ONLY board
    ADD CONSTRAINT pk_board PRIMARY KEY (id_board);

ALTER TABLE ONLY board_on_jeu
    ADD CONSTRAINT pk_board_on_jeu PRIMARY KEY (id_jeu, diese_board_on_jeu);

ALTER TABLE ONLY board_on_page
    ADD CONSTRAINT pk_board_on_page PRIMARY KEY (num_board_on_page, id_page);

ALTER TABLE ONLY box_on_jeu
    ADD CONSTRAINT pk_box_on_jeu PRIMARY KEY (diese_box_on_jeu, id_jeu);

ALTER TABLE ONLY box_on_page
    ADD CONSTRAINT pk_box_on_page PRIMARY KEY (num_box_on_page, id_page);

ALTER TABLE ONLY carac_pions_jeu
    ADD CONSTRAINT pk_carac_pion_jeu PRIMARY KEY (id_jeu, id_pion);

ALTER TABLE ONLY carac_pions_joueur_on_jeu
    ADD CONSTRAINT pk_carac_pions_joueur_on_jeu PRIMARY KEY (id_joueur, id_jeu, id_pion);

ALTER TABLE ONLY carac_pions_joueur_on_page
    ADD CONSTRAINT pk_carac_pions_joueur_on_page PRIMARY KEY (id_page, id_joueur, id_pion);

ALTER TABLE ONLY carac_pions_page
    ADD CONSTRAINT pk_carac_pions_on_page PRIMARY KEY (id_page, id_pion);

ALTER TABLE ONLY chat_on_page
    ADD CONSTRAINT pk_chat_on_page PRIMARY KEY (id_page, id_message);

ALTER TABLE ONLY jeu
    ADD CONSTRAINT pk_jeu PRIMARY KEY (id_jeu);


ALTER TABLE ONLY joueur_on_jeu
    ADD CONSTRAINT pk_joueur_on_jeu PRIMARY KEY (id_jeu, id_joueur);

ALTER TABLE ONLY joueur_on_page
    ADD CONSTRAINT pk_joueur_on_page PRIMARY KEY (id_page, id_joueur);

ALTER TABLE ONLY page
    ADD CONSTRAINT pk_page PRIMARY KEY (id_page);


ALTER TABLE ONLY pion
    ADD CONSTRAINT pk_pion PRIMARY KEY (id_pion);


ALTER TABLE ONLY pion_on_bac_on_jeu
    ADD CONSTRAINT pk_pion_on_bac_on_jeu PRIMARY KEY (id_jeu, diese_pion_on_bac_on_jeu, diese_bac_on_jeu);


ALTER TABLE ONLY pion_on_bac_on_page
    ADD CONSTRAINT pk_pion_on_bac_on_page PRIMARY KEY (id_page, diese_bac_on_page, num_pion_on_bac_on_page);


ALTER TABLE ONLY pion_on_board_on_jeu
    ADD CONSTRAINT pk_pion_on_board_on_jeu PRIMARY KEY (id_jeu, diese_board_on_jeu, diese_pion_on_board_on_jeu);


ALTER TABLE ONLY pion_on_board_on_page
    ADD CONSTRAINT pk_pion_on_board_on_page PRIMARY KEY (id_page, diese_board_on_page, num_pion_on_board_on_page);

ALTER TABLE ONLY save_on_page
    ADD CONSTRAINT pk_save_on_page PRIMARY KEY (id_page, noeud_suivant);


ALTER TABLE ONLY stop_bac_on_page
    ADD CONSTRAINT pk_stop_bac_on_page PRIMARY KEY (id_page, num_bac_on_page, noeud_stop);


ALTER TABLE ONLY stop_board_on_page
    ADD CONSTRAINT pk_stop_board PRIMARY KEY (id_page, num_board_on_page, noeud_stop);


ALTER TABLE ONLY stop_box_on_page
    ADD CONSTRAINT pk_stop_box_on_page PRIMARY KEY (id_page, num_box_on_page, noeud_stop);


ALTER TABLE ONLY stop_pion_on_bac_on_page
    ADD CONSTRAINT pk_stop_pion_bac PRIMARY KEY (id_page, diese_bac_on_page, num_pion_on_bac_on_page, noeud_stop);


ALTER TABLE ONLY stop_pion_on_board_on_page
    ADD CONSTRAINT pk_stop_pion_board PRIMARY KEY (id_page, diese_board_on_page, num_pion_on_board_on_page, noeud_stop);


CREATE INDEX "fki_actionClicOnboard" ON joueur_on_jeu USING btree (action_clic_onboard);


CREATE INDEX fki_noeud_pion_bac ON pion_on_bac_on_page USING btree (id_page, noeud_start);


ALTER TABLE ONLY joueur_on_jeu
    ADD CONSTRAINT "fk_actionClicOnboard" FOREIGN KEY (action_clic_onboard) REFERENCES bdd_fonctions(id_fonction);


ALTER TABLE ONLY joueur_on_page
    ADD CONSTRAINT "fk_actionClickOnBoard" FOREIGN KEY (action_clic_onboard) REFERENCES bdd_fonctions(id_fonction);


ALTER TABLE ONLY joueur_on_page
    ADD CONSTRAINT "fk_actionScrollOnBoard" FOREIGN KEY (action_scroll_onboard) REFERENCES bdd_fonctions(id_fonction);


ALTER TABLE ONLY joueur_on_jeu
    ADD CONSTRAINT "fk_actionScrollOnboard" FOREIGN KEY (action_scroll_onboard) REFERENCES bdd_fonctions(id_fonction);


ALTER TABLE ONLY pion_on_bac_on_jeu
    ADD CONSTRAINT fk_bac FOREIGN KEY (id_jeu, diese_bac_on_jeu) REFERENCES bac_on_jeu(id_jeu, diese_bac_on_jeu);


ALTER TABLE ONLY carac_pions_joueur_on_jeu
    ADD CONSTRAINT fk_bac FOREIGN KEY (diese_bac_on_jeu, id_jeu) REFERENCES bac_on_jeu(diese_bac_on_jeu, id_jeu);


ALTER TABLE ONLY stop_bac_on_page
    ADD CONSTRAINT fk_bac_stop FOREIGN KEY (id_page, num_bac_on_page) REFERENCES bac_on_page(id_page, num_bac_on_page);


ALTER TABLE ONLY pion_on_board_on_jeu
    ADD CONSTRAINT fk_board FOREIGN KEY (diese_board_on_jeu, id_jeu) REFERENCES board_on_jeu(diese_board_on_jeu, id_jeu);


ALTER TABLE ONLY board_on_page
    ADD CONSTRAINT fk_board FOREIGN KEY (id_board) REFERENCES board(id_board);


ALTER TABLE ONLY stop_board_on_page
    ADD CONSTRAINT fk_board_stop FOREIGN KEY (id_page, num_board_on_page) REFERENCES board_on_page(id_page, num_board_on_page);


ALTER TABLE ONLY stop_box_on_page
    ADD CONSTRAINT fk_box_stop FOREIGN KEY (num_box_on_page, id_page) REFERENCES box_on_page(num_box_on_page, id_page);


ALTER TABLE ONLY carac_pions_page
    ADD CONSTRAINT "fk_changePion" FOREIGN KEY (change_pion) REFERENCES pion(id_pion);


ALTER TABLE ONLY carac_pions_jeu
    ADD CONSTRAINT fk_change_pion FOREIGN KEY (change_pion) REFERENCES pion(id_pion);


ALTER TABLE ONLY carac_pions_joueur_on_jeu
    ADD CONSTRAINT fk_change_pion FOREIGN KEY (change_pion) REFERENCES pion(id_pion);


ALTER TABLE ONLY carac_pions_joueur_on_page
    ADD CONSTRAINT fk_change_pion FOREIGN KEY (change_pion) REFERENCES pion(id_pion);


ALTER TABLE ONLY joueur_on_jeu
    ADD CONSTRAINT "fk_clickBoardCreateType" FOREIGN KEY (click_board_create_type) REFERENCES pion(id_pion);


ALTER TABLE ONLY joueur_on_jeu
    ADD CONSTRAINT "fk_clickBoardMoveType" FOREIGN KEY (click_board_move_type) REFERENCES pion(id_pion);


ALTER TABLE ONLY joueur_on_jeu
    ADD CONSTRAINT "fk_clickableBoard" FOREIGN KEY (clickable_board) REFERENCES bdd_fonctions(id_fonction);


ALTER TABLE ONLY joueur_on_page
    ADD CONSTRAINT "fk_clickableBoard" FOREIGN KEY (clickable_board) REFERENCES bdd_fonctions(id_fonction);


ALTER TABLE ONLY carac_pions_jeu
    ADD CONSTRAINT fk_diese FOREIGN KEY (diese_bac_on_jeu, id_jeu) REFERENCES bac_on_jeu(diese_bac_on_jeu, id_jeu);


ALTER TABLE ONLY board_on_jeu
    ADD CONSTRAINT fk_id_board FOREIGN KEY (id_board) REFERENCES board(id_board);


ALTER TABLE ONLY board_on_jeu
    ADD CONSTRAINT fk_id_jeu FOREIGN KEY (id_jeu) REFERENCES jeu(id_jeu);


ALTER TABLE ONLY bac_on_jeu
    ADD CONSTRAINT fk_id_jeu FOREIGN KEY (id_jeu) REFERENCES jeu(id_jeu);


ALTER TABLE ONLY save_on_page
    ADD CONSTRAINT fk_id_joueur FOREIGN KEY (id_page, id_joueur) REFERENCES joueur_on_page(id_page, id_joueur);


ALTER TABLE ONLY carac_pions_jeu
    ADD CONSTRAINT fk_id_pion FOREIGN KEY (id_pion) REFERENCES pion(id_pion);


ALTER TABLE ONLY box_on_jeu
    ADD CONSTRAINT fk_jeu FOREIGN KEY (id_jeu) REFERENCES jeu(id_jeu);


ALTER TABLE ONLY carac_pions_joueur_on_jeu
    ADD CONSTRAINT fk_joueur FOREIGN KEY (id_jeu, id_joueur) REFERENCES joueur_on_jeu(id_jeu, id_joueur);


ALTER TABLE ONLY carac_pions_joueur_on_page
    ADD CONSTRAINT fk_joueur_on_page FOREIGN KEY (id_page, id_joueur) REFERENCES joueur_on_page(id_page, id_joueur);


ALTER TABLE ONLY bac_on_page
    ADD CONSTRAINT fk_noeud FOREIGN KEY (noeud_start, id_page) REFERENCES save_on_page(noeud_suivant, id_page);


ALTER TABLE ONLY board_on_page
    ADD CONSTRAINT fk_noeud_board FOREIGN KEY (id_page, noeud_start) REFERENCES save_on_page(id_page, noeud_suivant);


ALTER TABLE ONLY box_on_page
    ADD CONSTRAINT fk_noeud_box FOREIGN KEY (id_page, noeud_start) REFERENCES save_on_page(id_page, noeud_suivant);


ALTER TABLE ONLY pion_on_bac_on_page
    ADD CONSTRAINT fk_noeud_pion_bac FOREIGN KEY (id_page, noeud_start) REFERENCES save_on_page(id_page, noeud_suivant);


ALTER TABLE ONLY pion_on_board_on_page
    ADD CONSTRAINT fk_noeud_pion_board FOREIGN KEY (noeud_start, id_page) REFERENCES save_on_page(noeud_suivant, id_page);


ALTER TABLE ONLY box_on_page
    ADD CONSTRAINT fk_page FOREIGN KEY (id_page) REFERENCES page(id_page);


ALTER TABLE ONLY bac_on_page
    ADD CONSTRAINT fk_page FOREIGN KEY (id_page) REFERENCES page(id_page);


ALTER TABLE ONLY chat_on_page
    ADD CONSTRAINT fk_page FOREIGN KEY (id_page) REFERENCES page(id_page);


ALTER TABLE ONLY carac_pions_joueur_on_page
    ADD CONSTRAINT fk_page FOREIGN KEY (id_page) REFERENCES page(id_page);


ALTER TABLE ONLY save_on_page
    ADD CONSTRAINT fk_page FOREIGN KEY (id_page) REFERENCES page(id_page);


ALTER TABLE ONLY pion_on_bac_on_page
    ADD CONSTRAINT fk_page FOREIGN KEY (id_page) REFERENCES page(id_page);


ALTER TABLE ONLY pion_on_board_on_page
    ADD CONSTRAINT fk_page FOREIGN KEY (id_page) REFERENCES page(id_page);


ALTER TABLE ONLY board_on_page
    ADD CONSTRAINT fk_page_board_on_page FOREIGN KEY (id_page) REFERENCES page(id_page);


ALTER TABLE ONLY joueur_on_page
    ADD CONSTRAINT fk_page_joueur_on_page FOREIGN KEY (id_page) REFERENCES page(id_page);


ALTER TABLE ONLY box_on_jeu
    ADD CONSTRAINT fk_pion FOREIGN KEY (id_pion) REFERENCES pion(id_pion);


ALTER TABLE ONLY pion_on_bac_on_jeu
    ADD CONSTRAINT fk_pion FOREIGN KEY (id_pion) REFERENCES pion(id_pion);


ALTER TABLE ONLY pion_on_board_on_jeu
    ADD CONSTRAINT fk_pion FOREIGN KEY (id_pion) REFERENCES pion(id_pion);


ALTER TABLE ONLY carac_pions_joueur_on_jeu
    ADD CONSTRAINT fk_pion FOREIGN KEY (id_pion) REFERENCES pion(id_pion);


ALTER TABLE ONLY box_on_page
    ADD CONSTRAINT fk_pion FOREIGN KEY (id_pion) REFERENCES pion(id_pion);


ALTER TABLE ONLY carac_pions_joueur_on_page
    ADD CONSTRAINT fk_pion FOREIGN KEY (id_pion) REFERENCES pion(id_pion);


ALTER TABLE ONLY carac_pions_page
    ADD CONSTRAINT fk_pion FOREIGN KEY (id_pion) REFERENCES pion(id_pion);


ALTER TABLE ONLY pion_on_bac_on_page
    ADD CONSTRAINT fk_pion FOREIGN KEY (id_pion) REFERENCES pion(id_pion);


ALTER TABLE ONLY pion_on_board_on_page
    ADD CONSTRAINT fk_pion FOREIGN KEY (id_pion) REFERENCES pion(id_pion);


ALTER TABLE ONLY joueur_on_page
    ADD CONSTRAINT "fk_pion_createType" FOREIGN KEY (click_board_create_type) REFERENCES pion(id_pion);


ALTER TABLE ONLY joueur_on_page
    ADD CONSTRAINT "fk_pion_moveType" FOREIGN KEY (click_board_move_type) REFERENCES pion(id_pion);


ALTER TABLE ONLY stop_pion_on_bac_on_page
    ADD CONSTRAINT fk_stop_pion_bac FOREIGN KEY (id_page, diese_bac_on_page, num_pion_on_bac_on_page) REFERENCES pion_on_bac_on_page(id_page, diese_bac_on_page, num_pion_on_bac_on_page);


ALTER TABLE ONLY stop_pion_on_board_on_page
    ADD CONSTRAINT fk_stop_pion_board FOREIGN KEY (id_page, diese_board_on_page, num_pion_on_board_on_page) REFERENCES pion_on_board_on_page(id_page, diese_board_on_page, num_pion_on_board_on_page);


ALTER TABLE ONLY carac_pions_page
    ADD CONSTRAINT page FOREIGN KEY (id_page) REFERENCES page(id_page);


ALTER TABLE ONLY page
    ADD CONSTRAINT pk_jeu_depart FOREIGN KEY (id_jeu_depart) REFERENCES jeu(id_jeu);



