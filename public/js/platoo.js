$(function() {
	
	//intersection of two arrys
	//stack overflow
	function intersect(a, b) {
		var t;
		if (b.length > a.length) t = b, b = a, a = t; // indexOf to loop over shorter
		return a.filter(function (e) {
			return b.indexOf(e) > -1;
		});
	}


	
	function tailleBoard(){
		
		if($('.board-content').length>1){
			$('.board-content').css('max-width','320px');
			$('.board-content').css('margin-bottom','5px');
			$('.board-content').css('margin-right','5px');
		}else{
			$('.board-content').css('max-width','');
			$('.board-content').css('margin-bottom','0px');
			$('.board-content').css('margin-right','0px');
 		}
		if($('.board-content').length>2){
			$('.board-content').css('margin-bottom','10px');
		}else{
			$('.board-content').css('margin-bottom','0px');
		}
		
		$('.board-content').filter('.actif').each(function(){			
			$(this).css('height','');
			$(this).css('width','');
			var h = Math.min($(this).height(),parseFloat($(this).css('max-height'))/100*$(this).parent().height());
			var w = Math.min($(this).width(),parseFloat($(this).css('max-width'))/100*$(this).parent().width())-20;
			var rap = ($(this).children('.board').children('.board-svg').height())/($(this).children('.board').children('.board-svg').width());
			if(h/w>rap){
				$(this).height(rap*w);
				$(this).width(w);
			}else if(h/w<rap){
				$(this).width(h/rap);
				$(this).height(h);
				//$(this).height($(this).height());
			}//else{
				//$(this).width($(this).width());
				//$(this).height($(this).height());
			//}
			//var h = Math.min($(this).height(),parseFloat($(this).css('max-height'))/100*$(this).parent().height());
			//var w = Math.min($(this).width(),parseFloat($(this).css('max-width'))/100*$(this).parent().width());
			//var rap = ($(this).children('.board-svg').height())/($(this).children('.board-svg').width());
		});
	}
	
	tailleBoard();

	$(window).resize(function() {
		tailleBoard();
	});

	
	////////////////////////////// id�es /////////////////////////////////////////////
	
	//flex wrap boards
	//reduire box et bac
	//resizable ?
	//clavier
	//scroll deux sens
	//mouse over groupe
	//onglet menu
	//input text add box
	//input text add board
	//methodes Jquery pour activer : board, pion onboard, groupe, box, bac
	//dans menu : option d'affichage des box/bac
	//masquer le menu
	//css special petit ecran
	//connexion � socket
	//faire double clic?

	
	/////////////////////////////// fonctions qui servent partout ////////////////////////////////////////////
	
	//calcul le Diese le plus �lev� sur un type d'objet
	// function maxDiese(obj) {
		// ExprDiese=/[0-9]*$/;
		// max=0;
		// arr = obj.map(function(){
				// num=ExprDiese.exec($(this).diese())[0];
				// if(num===''){
					// return null;
				// }else{
					// return parseInt(num);
				// }
			// }
		// );
		// max = Math.max.apply(null, arr);
		// return(max);
	// };
	
	//extrait le Diese d'un �l�ment
	$.fn.diese = function() {
        // return this.each(function(){
			// //return ($(this).attr('id').replace(/^.*-/,''));
			// return parseInt($(this).attr('id').replace(/^.*-/,''));
		// });
		return parseInt($(this).attr('diese').replace(/^.*-/,''));
    };
	
	$.fn.tp = function(){
		return $(this).attr('class').match(/tp\-[^ ]*/).toString().substring(3);
	}
	
	$.fn.csstop = function(){
		return parseFloat($(this).attr('style').match(/top: ?[0-9.]*/).toString().replace(/top: ?/,''));
		//return parseFloat($(this).css('top'));
	}

	$.fn.cssleft = function(){
		return parseFloat($(this).attr('style').match(/left: ?[0-9.]*/).toString().replace(/left: ?/,''));
		//return parseFloat($(this).css('left'));
	}
	
	$.fn.cssheight = function(){
		return parseFloat($(this).attr('style').match(/max-height: ?[0-9.]*/).toString().replace(/max-height: ?/,''));
		//return parseFloat($(this).css('max-height'));
	}
	
	$.fn.csstransform = function(){
		var res = $(this).attr('style');
		if(res === null){
			return null;
		}else{
			res = /; ?transform ?: ?rotate\([0-9]+deg\);/.exec(res);
			if(res === null){
				return null;
			}else{
				res = parseInt(/[0-9]+/.exec(res[0])[0])/90;
			}
		}
		return parseInt(/[0-9]+/.exec(/; ?transform ?: ?rotate\([0-9]+deg\);/.exec($(this).attr('style'))[0])[0])/90;
	}
	
	$.fn.setcsstransform = function(Rot){
		Rot*=90;
		$(this).attr('style',$(this).attr('style').replace(/transform ?: ?rotate\([0-9]+deg\);/g,'transform:rotate('+Rot+'deg);'));
		return $(this).attr('style');
	}

	
	
	function joueurActif(){
		return parseInt($('input[name=player]:checked').attr('value'));
	}
	
	function newBoxType(){
		return $(':selected','#add_box_type').attr('value');
	}
	
	function newBoardType(){
		return $(':selected','#add_board_type').attr('value');
	}
	
	function abs2real(left,top,board,taillePion){
		var nRot=$('#board-'+board).children('.board').children('.board-svg').csstransform();
		var left2= left;
		var top2 = top;
		for (var i = 0; i < nRot; i++){
			left2 = 100-top-taillePion;
			top2 = left;
			top=top2;
			left=left2;
		}
		return {left:left,top:top};
	}
	
	function real2abs(left,top,board,taillePion){
		var nRot=$('#board-'+board).children('.board').children('.board-svg').csstransform();
		var left2= left;
		var top2 = top;
		for (var i = 1; i <= nRot; i++){
			top2 = 100-left-taillePion;
			left2 = top;
			top=top2;
			left=left2;
		}
		return {left:left,top:top};	}
	
	//var newBoxType='dame_dame_blanc';
	//var newBoardType='dame';
		
	function afficheNoeud(noeud){
				
		var aff=[noeud];
		var new_noeud=0;
		while((aff.indexOf(0)==-1) && (noeuds_suivants.indexOf(aff[aff.length-1])!=-1)){
			new_noeud=noeuds_precedents[noeuds_suivants.indexOf(aff[aff.length-1])];
			aff.push(new_noeud);
		}
		if(aff[noeudAff.length-1]==0){
			aff.pop();
		}
		$('.actif, .inactif').each(function(i,e){
			
			if((!$(this).hasClass('board-content')) && (!$(this).hasClass('bac'))){				
				var noeud_start=parseInt($(this).attr('nstart'));
				
				//var noeuds_stop=$(this).attr('nstop').split(',');
				var noeuds_stop=$(this).attr('nstop');
				noeuds_stop=noeuds_stop.split(',');
				//if(typeof noeuds_stop=== 'string'){
				//	noeuds_stop=[noeuds_stop];
				//}
				
				for(var i=0;i<noeuds_stop.length;i++){
					noeuds_stop[i]=parseInt(noeuds_stop[i]);
				}
				noeuds_stop.sort().reverse();
				while(noeuds_stop[noeuds_stop.length-1]===0){
					noeuds_stop.pop();
				}
				

				
				if( (aff.indexOf(noeud_start)!= -1) && (intersect(aff,noeuds_stop).length===0) ){
					//display='actif';
					$(this).removeClass('inactif').removeClass('actif').addClass('actif');
				}else{
					//display='inactif';
					$(this).removeClass('actif').removeClass('inactif').addClass('inactif');
				}
			}
		}).promise().done(function(){
			//g�rer board et bac
			$('.board-content').each(function(){
				if($(this).children('.board').children('.board-svg.actif').length>0){
					$(this).removeClass('inactif').removeClass('actif').addClass('actif');
				}else{
					$(this).removeClass('inactif').removeClass('actif').addClass('inactif');
				}
			});
			$('.bac').each(function(){
				if($(this).children('.labelBac').children('span.actif').length>0){
					$(this).removeClass('inactif').removeClass('actif').addClass('actif');
				}else{
					$(this).removeClass('inactif').removeClass('actif').addClass('inactif');
				}
			});
			
		});
	}
	///////////////////////////// connexion � socket.io ////////////////////////////////
	
	var socket = io.connect(window.location.host);
	socket.emit('newplatoo',nomPlatoo);	

	

	
	
	
	///////////////// Fonctions de modif du html //////////////////////////////////////

	function Hadd_bac(diese,label,num,noeud){
		if($('.trash','.bacs.actif').length>0){
			$('<div class="bac actif" id="bac-'+diese+'" diese="bac-'+diese+'">'+
			'<p class="labelBac"><span class="actif"  num="pion-'+num+'" nstart="'+noeud+'" nstop="0">'+label+'</span><img src="/images/boutons/edit.svg" class="edit-bac" alt="edit"/></p>'+
			'<form action="#" class="inputLabelBac" onsubmit="return false;">'+
			'<input type="text" name="Label" size="10" maxlength="10" value="'+label+'"/>'+
			'</form>'+
			'<div class="bacBordure"></div>'+
			'</div>'
			).insertBefore($('.trash','.bacs.actif').first()).children('.bacBordure').droppable(droppableBac);
		}else{
			$('<div class="bac actif" id="bac-'+diese+'" diese="bac-'+diese+'">'+
			'<p class="labelBac"><span class="actif"  num="pion-'+num+'" nstart="'+noeud+'" nstop="0">'+label+'</span><img src="/images/boutons/edit.svg" class="edit-bac" alt="edit"/></p>'+
			'<form action="#" class="inputLabelBac" onsubmit="return false;">'+
			'<input type="text" name="Label" size="10" maxlength="10" value="'+label+'"/>'+
			'</form>'+
			'<div class="bacBordure"></div>'+
			'</div>'
			).appendTo($('.bacs')).children('.bacBordure').droppable(droppableBac);
		}
	}
	
	function Hadd_trash(){
		$('<div class="trash"></div>').appendTo($('.bacs')).droppable(droppableTrash);
	}

	function Hadd_box(nom,diese,svg,num,noeud){
		$('<div class="box actif" id="box-'+diese+'" diese="box-'+diese+'"><img src="/images/pions/'+svg+'" class="pion-onbox tp-'+nom+'" alt="pion_'+nom+
		'" num="box-'+num+'" nstart="'+noeud+'" nstop="0""/></div>'
		).appendTo('.boxes').children('.pion-onbox').draggable(draggableOnbox);		
	}
	
	function Hadd_board(nom,diese,svg,num,noeud){
		$('<div class="board-content actif" id="board-'+diese+'" diese="board-'+diese+'">'+
		'<div class="board"><img src="/images/boards/'+svg+'" class="board-svg board_'+nom+' actif" alt="board_'+nom+
		' style="-webkit-transform: rotate(0deg);-ms-transform: rotate(0deg);transform: rotate(0deg);"  num="board-'+num+
		'" nstart="'+noeud+'" nstop="0"/></div>'+
		'<div class="board-action">'+
		'<img src="/images/boutons/fleche.svg" class="bouton-board fleche" alt="fleche" style="-webkit-transform: rotate(0deg);-ms-transform: rotate(0deg);transform: rotate(0deg);"/>'+
		'<img src="/images/boutons/turnup.svg" class="bouton-board bouton-turnup" alt="turn up"/>'+
		'<img src="/images/boutons/turnright.svg" class="bouton-board bouton-turnright" alt="turn right"/>'+
		'<img src="/images/boutons/turnleft.svg" class="bouton-board bouton-turnleft" alt="turn left"/></div></div>	'
		).appendTo('.boards').promise().done(function(){
			$('#board-'+diese).children('.board').droppable(droppableBoard).on('click',clickable_board);
			$('#board-'+diese).children('.board').children('img').on('load',tailleBoard);
		});
		
		
	}
	
	function HcreatePionOnboard(type,diese,board,top,left,height,svg,directed,num,noeud){
		// var height=tabPionTaille[tabPionListe.indexOf(type)];
		var pionOnboard = '<img src="/images/pions/'+svg+'" class="pion-onboard tp-'+type+' actif" alt="pion_'+type+'" diese="pion-'+diese+'" style="'+
		'top:'+top+'%;left:'+left+'%;max-height:'+height+'%;z-index:1';
		if(directed===1){
			Rot=90* $('#board-'+board).children('.board').children('.board-svg').csstransform();
			pionOnboard += '-webkit-transform: rotate('+Rot+'deg);-ms-transform: rotate('+Rot+'deg);transform: rotate('+Rot+'deg);';
		}
		pionOnboard +='" id="pion-'+num+'" nstart="'+noeud+'" nstop="0"/>';
		$(pionOnboard).appendTo('#board-'+board+' > .board').draggable(draggableOnboard);
	}

	function HcreateGroupeOnbac(type,diese,n,bac,svg,num,noeud){
		var GroupeOnbac ='<div class="groupe-onbac tp-'+type+' actif" diese="groupe-'+diese+'" id="groupe-'+num+'" nstart="'+noeud+'" nstop="0"><img src="/images/pions/'+svg+'" class="pion-onbac tp-'+type+'" alt="pion_'+type+'"/><p class="nb-pions-onbac">'+n+'</p>';
		GroupeOnbac+='<form action="#" class="inputN" onsubmit="return false;"><input type="text" name="N" size="3" maxlength="3" value="'+n+'"/></form>';
		GroupeOnbac+='<div class="boutons-groupe"><img src="/images/boutons/del.svg" class="del-groupe" alt="del"/><img src="/images/boutons/edit.svg" class="edit-groupe" alt="edit"/></div>';
		GroupeOnbac+='</div>';
		$(GroupeOnbac).appendTo($('#bac-'+bac).children('.bacBordure')).draggable(draggableGroupeOnbac).children('.pion-onbac').draggable(draggableOnbac);
	}
	
	function HremoveElement(id,noeud){
		if(id==='trash'){
			$('.trash').last().remove().promise().done(function(){tailleBoard();});
		}else{
			//$('#'+id).remove().promise().done(function(){tailleBoard();});
			$('#'+id).attr('nstop',$('#'+id).attr('nstop')+','+noeud).removeClass('actif').addClass('inactif').promise().done(function(){tailleBoard();});
		}
		//tailleBoard();
		
	}
	
	//function HchangePionOnboard(diese,type,top,left,height,board,svg){
	//	$('#pion-'+diese).removeClass($('#pion-'+diese).attr('class').match(/tp\-[^ ]*/).toString()).addClass('tp-'+type).css('top',top+'%').css('left',left+'%').css('max-height',height+'%').detach().appendTo('#board-'+board+' > .board');
	/*	$('#pion-'+diese).attr('src','/images/pions/'+svg);
	}
	
	function HmodifyGroup(diese,n,bac){
		$('#groupe-'+diese).children('.nb-pions-onbac').text(n);
		if(bac!=$('#groupe-'+diese).parent().parent().diese()){
			$('#groupe-'+diese).detach().appendTo('#bac-'+bac);
		}
	}*/

	function Herase(noeud){
		$('.pion-onboard').each(function(){
			var nstop = $(this).attr('nstop').split(',');
			for(var i=0;i<nstop.length;i++){
				nstop[i] = parseInt(nstop[i]);
			}
			/*var aff=true;
			var i=0;
			var j=0;
			//while(aff && (i<noeudAff.length || j<nstop.length)){
			while(aff && j<nstop.length){
				if(noeudAff[i]==nstop[j]){
					aff=false;
				}
				i=i+1;
				if(i>=noeudAff.length){
					i=0;
					j=j+1;
				}
			}
			aff = aff && (noeudAff.indexOf($(this).attr('nstart'))!=-1)*/
			/*if(aff){
				$(this).attr('nstop',$(this).attr('nstop')+','+noeud).removeClass('actif').addClass('inactif');
			}*/
			if( (noeudAff.indexOf(parseInt($(this).attr('nstart')))!= -1) && (intersect(noeudAff,nstop).length===0) ){
				$(this).attr('nstop',$(this).attr('nstop')+','+noeud).removeClass('actif').addClass('inactif');
			}
		})
		$('.groupe-onbac').each(function(){
			var nstop = $(this).attr('nstop').split(',');
			for(var i=0;i<nstop.length;i++){
				nstop[i] = parseInt(nstop[i]);
			}
			/*var aff=true;
			var i=0;
			var j=0;
			while(aff && (i<noeudAff.length || j<nstop.length)){
			//while(aff && j<nstop.length){
				if(noeudAff[i]==nstop[j]){
					aff=false;
				}
				i=i+1;
				if(i>=noeudAff.length){
					i=0;
					j=j+1;
				}
			}
			aff = aff && (noeudAff.indexOf($(this).attr('nstart'))!=-1)
			if(aff){
				$(this).attr('nstop',$(this).attr('nstop')+','+noeud).removeClass('actif').addClass('inactif');
			}*/
			if( (noeudAff.indexOf(parseInt($(this).attr('nstart')))!= -1) && (intersect(noeudAff,nstop).length===0) ){
				$(this).attr('nstop',$(this).attr('nstop')+','+noeud).removeClass('actif').addClass('inactif');
			}
		})
	}

	function Hchat(date,couleur,auteur,message,id){
		heure=/\d{1,2}:\d{2}(?=:)/;
		date = heure.exec(date);
		R=parseInt(couleur.substr(1,2),16);
		G=parseInt(couleur.substr(3,2),16);
		B=parseInt(couleur.substr(5,2),16);
		if((G+R+B)>382){
		colT="#000000";
		}else{
			colT="#ffffff";
		}
		var message = '<div class="message-chat" id="chat-'+id+'" style="background-color:'+couleur+';color:'+colT+';">'+
					'<div id="head-chat">'+
						'<span class="auteur-chat">'+auteur+' :</span>'+
						'<span class="date-chat">'+date+'</span>'+
						'<div style="clear: both;"></div>'+
					'</div>'+
					'<p class="texte-chat">'+message+'<br/><br/></p>'+
				'</div>';
		$(message).prependTo($('#texte-chat'));
		if($('.message-chat','#texte-chat').length>20){
			$('.message-chat:gt(19)','#texte-chat').remove();
		}
		
	}
	
	function HchangeLabelBac(diese,label,oldnum,num,noeud){
		label = label.substring(0,20);
		//$('#bac-'+diese).children('.labelBac').children('span').css('display','none');
		$('#bac-'+diese).children('.labelBac').children('span[num="bac-'+oldnum+'"]').removeClass('actif'
		).addClass('inactif').attr('nstop',$('#bac-'+diese).children('.labelBac').children('span[num="bac-'+oldnum+'"]').attr('nstop')+','+noeud)
		
		$('<span class="actif"  num="bac-'+num+'" nstart="'+noeud+'" nstop="0">'+label+'</span>'
		).prependTo('#bac-'+diese).children('.labelBac');
		
		//$('#bac-'+diese).children('.labelBac').children('span').text(label);
		
		$('#bac-'+diese).children('.inputLabelBac').children('input').val(label);
	}
	
		
	////////////////// Fonctions de d�tections : envoie les infos au serveur, lance les fonctions de modif du html //////////////////////////////

	//// liste des actions possibles :
	
	// cliquer pour nouveau pion ok
	// cliquer por d�placer pion groupe->board ok
	// cliquer pour deplacer pion board->groupe ok
	// c&d pour cr�er ok
	// c&d pour d�placer ok
	// c&d pour groupe->board ok
	// c&d pour board->groupe ok
	
	// scroll pour changer ok
	
	//cliquer pour supprimer un pion ok
	//cliquer pour supprimer un groupe ok
	
	//supprimer un bac ok
	//supprimer un box ok
	//supprimer un board ok
	//supprimer une trash ok
	
	// bouton erase ok
	// bouton menu ok
	// bouton chat ok
	// bouton delete ok
	// bouton annuler delete ok
	
	
	
	
	function deletePionOnboard(e){
		if($(this).hasClass('noclic')){
			$(this).removeClass('noclic');
		}else{
			socket.emit('ModifJeu',{
				noeud_courant:noeudCourant,
				auteur:$('#auteurChat').val(),
				couleur:$('#couleurChat').val(),
				id_joueur:parseInt($('input[name=player]:checked').attr('value')),
				requetes:[{
					action:'removeElement',
					data:{
						id:$(this).attr('id')
					}
				}]
			});
			
			//HremoveElement($(this).attr('id'));
		}
	}
	
	
	function deleteGroupe(e){

		socket.emit('ModifJeu',{
			noeud_courant:noeudCourant,
			auteur:$('#auteurChat').val(),
			couleur:$('#couleurChat').val(),
			id_joueur:parseInt($('input[name=player]:checked').attr('value')),
			requetes:[{
				action:'removeElement',
				data:{
					id:$(this).parent().parent().attr('id')
				}
			}]
		});
		
		//HremoveElement($(this).parent().parent().attr('id'));
	}
	
	function editGroupe(e){
		if(flagN){
			flagN=false;
			var val = parseInt($(this).children('input').val());
			if(val>0 && val<1000){
				//HmodifyGroup($(this).parent().diese(),val,$(this).parent().parent().parent().diese());
				socket.emit('ModifJeu',{
					noeud_courant:noeudCourant,
					auteur:$('#auteurChat').val(),
					couleur:$('#couleurChat').val(),
					id_joueur:parseInt($('input[name=player]:checked').attr('value')),
					requetes:[{
						action:'modifyGroup',
						data:{
							diese : $(this).parent().diese(),
							n : val,bac: $(this).parent().parent().parent().diese(),
							type:$(this).parent().tp()
						}
					}]
				});
				$(this).parent().children('.nb-pions-onbac').css('display','block');
				$(this).parent().children('.inputN').css('display','none');
			}else if(val===0){
				$(this).parent().children('.boutons-groupe').children('.del-groupe').trigger('click');
			}else{
				$(this).children('input').val($(this).parent().children('.nb-pions-onbac').text());
				$(this).parent().children('.nb-pions-onbac').css('display','block');
				$(this).css('display','none');
			}
		}
		return false;
	}
	
	function displayEditGroupe(e){
		flagN=true;
		$(this).parent().parent().children('.inputN').children('input').val($(this).parent().parent().children('.nb-pions-onbac').text());
		$(this).parent().parent().children('.nb-pions-onbac').css('display','none');
		$(this).parent().parent().children('.inputN').css('display','block').children('input').focus();
		return false;
	}
	
	function displayEditLabelBac(e){
		flagLabel=true;
		$('.bacs').on('focusout','.inputLabelBac input',function(e){$(this).parent().submit();});
		$(this).parent().parent().children('.inputLabelBac').children('input').val(/(?=^[ ]*).*(?=[ ]*$)/.exec($(this).parent().children('span').text()));
		$(this).parent().css('display','none');
		$(this).parent().parent().children('.inputLabelBac').css('display','block').children('input').focus();
		return false;
	}
	
	function editLabelBac(e){
		if(flagLabel){
			flagLabel=false;
			$('.bacs').off('focusout','.inputLabelBac input',function(e){$(this).parent().submit();});
			var label = $(this).children('input').val().replace(/[ \t\n]*$/,'').replace(/^[ \t\n]*/,'').substring(0,20);
			//HchangeLabelBac($(this).parent().diese(),label);

			socket.emit('ModifJeu',{
				noeud_courant:noeudCourant,
				auteur:$('#auteurChat').val(),
				couleur:$('#couleurChat').val(),
				id_joueur:parseInt($('input[name=player]:checked').attr('value')),
				requetes:[{
					action:'changeLabelBac',
					data:{
						diese : $(this).parent().diese(),
						label : label
					}
				}]
			});
			$(this).parent().children('.labelBac').css('display','block');
			$(this).css('display','none');
		}
		return false;
	}
	
	function changePionOnboard(e){
		if($(this).hasClass('noclic')){
			$(this).removeClass('noclic');
		}else{
			var type = tabPionChange[tabPionListe.indexOf($(this).attr('class').match(/tp\-[^ ]*/).toString().substring(3))];
			var svg = tabPionSvg[tabPionListe.indexOf(type)];
			console.log(type)
			console.log(tabPionChange)
			console.log(tabPionListe)
			console.log($(this).attr('class'))
			console.log($(this).attr('class').match(/tp\-[^ ]*/).toString().substring(3))
			if(type === null){
				socket.emit('getTabPion',{id_joueur : joueurActif() });
			}else if(type !== -1){
			//var nom = type.substring(3);
				//HchangePionOnboard(diese=$(this).diese(),type=type,top=$(this).csstop(),left=$(this).cssleft(),height=$(this).cssheight(),board=$(this).parent().parent().diese(),svg=svg);
				var topleft=real2abs($(this).cssleft(),$(this).csstop(),$(this).parent().parent().diese(),$(this).cssheight());
				var top = topleft.top;
				var left = topleft.left;
				console.log('ee');
				socket.emit('ModifJeu',{
					noeud_courant:noeudCourant,
					auteur:$('#auteurChat').val(),
					couleur:$('#couleurChat').val(),
					id_joueur:parseInt($('input[name=player]:checked').attr('value')),
					requetes:[{
						action:'changePionOnboard',
						data:{
							diese : $(this).diese(),
							type:type,top:top,left:left,height:$(this).cssheight(),
							board:$(this).parent().parent().diese(),svg:svg
						}
					}]
				});
			}
		}
	}
	
	function movePionOnboard(e){
		if($(this).hasClass('noclic')){
			$(this).removeClass('noclic');
		} else{
			var bac =  tabPionMove[(tabPionListe.indexOf($(this).tp()))];
			if(bac === null){
				socket.emit('getTabPion',{id_joueur : joueurActif() });
			}else if(bac !== -1){
				var classTpMovable=$(this).tp();
				// socket.emit('movePionOnboard',{indexBoard: $(this).parent().diese(),indexPion : $(this).diese(), indexBac : bac});
				var TpExist=false;
				$('.groupe-onbac',$('#bac-'+bac).children('.bacBordure')).each(function(){
					if($(this).tp()==(classTpMovable) && $(this).hasClass('actif')){
						TpExist=true;
					}
				});
				if(TpExist){
					var diese =$('#bac-'+bac).children('.bacBordure').children('.tp-'+classTpMovable).filter('.actif').diese();
					var n = parseInt($('#bac-'+bac).children('.bacBordure').children('.tp-'+classTpMovable).filter('.actif').children('.nb-pions-onbac').text())+1;
					socket.emit('ModifJeu',{
						noeud_courant:noeudCourant,
						auteur:$('#auteurChat').val(),
						couleur:$('#couleurChat').val(),
						id_joueur:parseInt($('input[name=player]:checked').attr('value')),
						requetes:[{
							action:'modifyGroup',
							data:{
								diese : diese,n : n,bac: bac,type:classTpMovable
							}
						},{
							action:'removeElement',
							data:{
								id:$(this).attr('id')
							}
						}]
					});
					//HmodifyGroup(diese,n,bac);
					//HremoveElement($(this).attr('id'));
				} else {
					
					socket.emit('ModifJeu',{
						noeud_courant:noeudCourant,
						auteur:$('#auteurChat').val(),
						couleur:$('#couleurChat').val(),
						id_joueur:parseInt($('input[name=player]:checked').attr('value')),
						requetes:[{
							action:'createGroupeOnbac',
							data:{
								type : classTpMovable, n : 1, bac : bac
							}
						},{
							action:'removeElement',
							data:{
								id:$(this).attr('id')
							}
						}]
					});
					//HremoveElement($(this).attr('id'));

					//$(createGroupeOnbac(classTpMovable)).appendTo($('#bac-'+bac)).children('.pion-onbac').draggable(draggableOnbac);//on cr�e un groupe
				}
			}
		}
	}
	
	function clickableBoardNew(e){
		if($(e.target).hasClass('board-svg')){

		var height = tabPionTaille[tabPionListe.indexOf(click_board_create_type)];
			
			if(height===null|typeof height === 'undefined'){
				socket.emit('getTabPion',{id_joueur : joueurActif() });
			}else{
				var x = e.pageX;
				var y = e.pageY;
				var pix = height /100 * parseFloat($(this).css("height"));
				var l = ( 100 * (x-parseFloat($(this).offset().left)- pix/2) / parseFloat($(this).css("width")) );
				var t = ( 100 * (y-parseFloat($(this).offset().top)- pix/2) / parseFloat($(this).css("height")) );
//				var l = ( 100 * (x-parseFloat($(this).offset().left)- $('img:last-child',this).width()/2) / parseFloat($(this).css("width")) );
//				var t = ( 100 * (y-parseFloat($(this).offset().top)- $('img:last-child',this).height()/2) / parseFloat($(this).css("height")) );
				
				// HcreatePionOnboard(type,diese,board,top,left,height)
				
				// $(creatPionOnboard(click_board_create_type)).appendTo(this);
				// $('img:last-child',this).css('left',l);
				// $('img:last-child',this).css('top',t);
				// $('img:last-child',this).css('z-index',1);
				// $('img:last-child',this).draggable(draggableOnboard);//.on('click',action_clic_onboard);
				var topleft=real2abs(l,t,$(this).parent().diese(),height);
				var top = topleft.top;
				var left = topleft.left;

				socket.emit('ModifJeu',{
					noeud_courant:noeudCourant,
					auteur:$('#auteurChat').val(),
					couleur:$('#couleurChat').val(),
					id_joueur:parseInt($('input[name=player]:checked').attr('value')),
					requetes:[{
						action:'createPionOnboard',
						data:{
							type : click_board_create_type,
							board : $(this).parent().diese(),top : top,left : left, height : height
						}
					}]
				});
			}
		}
	}
	
	function clickableBoardMove(e){
		if($(e.target).hasClass('board-svg')){
			if($('#bac-'+click_board_move_bac).length == 1){
				if($('#bac-'+click_board_move_bac).children('.bacBordure').children('.tp-'+click_board_move_type).filter('.actif').length>0){

					if(parseInt($('#bac-'+click_board_move_bac).children('.bacBordure').children('.tp-'+click_board_move_type).filter('.actif').children('.nb-pions-onbac').text())>1){
						var diese = $('#bac-'+click_board_move_bac).children('.bacBordure').children('.tp-'+click_board_move_type).filter('.actif').diese();
						var n = parseInt($('#bac-'+click_board_move_bac).children('.bacBordure').children('.tp-'+click_board_move_type).filter('.actif').children('.nb-pions-onbac').text())-1;
						
						
						var req1={
							action:'modifyGroup',
							data:{
								diese : diese,n : n,bac: click_board_move_bac,type:click_board_move_type
							}
						};
						
						
						/*socket.emit('ModifJeu',{
							noeud_courant:noeudCourant,
							auteur:$('#auteurChat').val(),
							couleur:$('#couleurChat').val(),
							id_joueur:parseInt($('input[name=player]:checked').attr('value')),
							requetes:[{
								action:'modifyGroup',
								data:{
									diese : diese,n : n,bac: click_board_move_bac,type:click_board_move_type
								}
							}]
						});		*/				
						//HmodifyGroup(diese,n,click_board_move_bac);
					} else {
						
						var req1={
							action:'removeElement',
							data:{
								id:$('#bac-'+click_board_move_bac).children('.bacBordure').children('.tp-'+click_board_move_type).filter('.actif').attr('id')
							}
						};
						
						/*socket.emit('ModifJeu',{
							noeud_courant:noeudCourant,
							auteur:$('#auteurChat').val(),
							couleur:$('#couleurChat').val(),
							id_joueur:parseInt($('input[name=player]:checked').attr('value')),
							requetes:[{
								action:'removeElement',
								data:{
									id:$('#bac-'+click_board_move_bac).children('.bacBordure').children('.tp-'+click_board_move_type).filter('.actif').attr('id')
								}
							}]
						});	*/				
						//HremoveElement($('#bac-'+click_board_move_bac).children('.bacBordure').children('.tp-'+click_board_move_type).attr('id'));
					}
					var height = tabPionTaille[tabPionListe.indexOf(click_board_move_type)];
						
					if(height===null | typeof height === 'undifined'){
						socket.emit('getTabPion',{id_joueur : joueurActif() });
					}else{
						var x = e.pageX;
						var y = e.pageY;
						var pix = height /100 * parseFloat($(this).css("height"));
						var l = ( 100 * (x-parseFloat($(this).offset().left)- pix/2) / parseFloat($(this).css("width")) );
						var t = ( 100 * (y-parseFloat($(this).offset().top)- pix/2) / parseFloat($(this).css("height")) );
						// $('img:last-child',this).css('left',l);
						// $('img:last-child',this).css('top',t);
						// $('img:last-child',this).css('z-index',1);
						// $('img:last-child',this).draggable(draggableOnboard);//.on('click',action_clic_onboard);
						var topleft=real2abs(l,t,$(this).parent().diese(),height);
						var top = topleft.top;
						var left = topleft.left;

						socket.emit('ModifJeu',{
							noeud_courant:noeudCourant,
							auteur:$('#auteurChat').val(),
							couleur:$('#couleurChat').val(),
							id_joueur:parseInt($('input[name=player]:checked').attr('value')),
							requetes:[req1,{
								action:'createPionOnboard',
								data:{
									type : click_board_move_type,
									board : $(this).parent().diese(),top : top,left : left, height : height
								}
							}]
						});
					}
				}
			}
		}
	}
	
	function rien(){
		return(null);
	}
	
	function movePionOnboardShogi(){
		if($(this).hasClass('noclic')){
			$(this).removeClass('noclic');
		}else{
			
			var bac =  tabPionMove[(tabPionListe.indexOf($(this).tp()))];
			if(bac === null){
				socket.emit('getTabPion',{id_joueur : joueurActif() });
			}else if(bac !== -1){

				//le pion change de camp
				var type0 = $(this).tp()
				type = type0.replace(/_p$/,'').replace(/_nord/,'_zzz').replace(/_sud/,'_nord').replace(/_zzz/,'_sud');
				
				var svg = tabPionSvg[tabPionListe.indexOf(type)];
				if(svg === null){
					socket.emit('getTabPion',{id_joueur : joueurActif() });
				}else{
					//HchangePionOnboard(diese=$(this).diese(),type=type,top=$(this).csstop(),left=$(this).cssleft(),height=$(this).cssheight(),board=$(this).parent().parent().diese(),svg=svg);
					
					var topleft=real2abs($(this).cssleft(),$(this).csstop(),$(this).parent().parent().diese(),$(this).cssheight());
					var top = topleft.top;
					var left = topleft.left;

					/*socket.emit('ModifJeu',{
						noeud_courant:noeudCourant,
						auteur:$('#auteurChat').val(),
						couleur:$('#couleurChat').val(),
						id_joueur:parseInt($('input[name=player]:checked').attr('value')),
						requetes:[{
							action:'changePionOnboard',
							data:{
								diese : $(this).diese(),type:type,top:top,left:left,height:$(this).cssheight(),board:$(this).parent().parent().diese(),svg:svg
							}
						}]
					});*/
					
					//on bouge le pion

					//var classTpMovable=$(this).tp();
					// socket.emit('movePionOnboard',{indexBoard: $(this).parent().diese(),indexPion : $(this).diese(), indexBac : bac});
					var TpExist=false;
					$('.groupe-onbac',$('#bac-'+bac).children('.bacBordure')).each(function(){
						if($(this).tp()==(type)){
							TpExist=true;
						}
					});
					if(TpExist){
						var diese =$('#bac-'+bac).children('.bacBordure').children('.tp-'+type).diese();
						var n = parseInt($('#bac-'+bac).children('.bacBordure').children('.tp-'+type).children('.nb-pions-onbac').text())+1;
						socket.emit('ModifJeu',{
							noeud_courant:noeudCourant,
							auteur:$('#auteurChat').val(),
							couleur:$('#couleurChat').val(),
							id_joueur:parseInt($('input[name=player]:checked').attr('value')),
							requetes:[{
								action:'modifyGroup',
								data:{
									diese : diese,n : n,bac: bac,type:type
								}
							},{
								action:'removeElement',
								data:{
									id:$(this).attr('id')
								}
							}]
						});
						//HmodifyGroup(diese,n,bac);
						//HremoveElement($(this).attr('id'));
					} else {
						socket.emit('ModifJeu',{
							noeud_courant:noeudCourant,
							auteur:$('#auteurChat').val(),
							couleur:$('#couleurChat').val(),
							id_joueur:parseInt($('input[name=player]:checked').attr('value')),
							requetes:[{
								action:'createGroupeOnbac',
								data:{
									type : type, n : 1, bac : bac
								}
							},{
								action:'removeElement',
								data:{
									id:$(this).attr('id')
								}
							}]
						});						
						//HremoveElement($(this).attr('id'));
					}
				}

			}
		}
	}
	
	function turnupBoard(){
		$(this).parent().parent().children('.board').children('.pion-onboard').each(function(){
			var topleft=real2abs($(this).cssleft(),$(this).csstop(),$(this).parent().parent().diese(),$(this).cssheight());
			$(this).css('top',topleft.top+'%').css('left',topleft.left+'%');
			if(!isNaN($(this).csstransform())){
				$(this).setcsstransform(0);
			}
		});
		$(this).parent().parent().children('.board').children('.board-svg').setcsstransform(0);
		$(this).parent().children('.fleche').setcsstransform(0);
		// $(this).parent().parent().children('.board').children('.board-svg').css('transform','rotate(0deg)');
		// $(this).parent().parent().children('.board').children('.board-svg').css('-ms-transform','rotate(0deg)');
		// $(this).parent().parent().children('.board').children('.board-svg').css('-webkit-transform','rotate(0deg)');
	}
	
	function turnrightBoard(){
		var nRot=$(this).parent().parent().children('.board').children('.board-svg').csstransform();
		nRot=(nRot+1)%4;
		
		$(this).parent().parent().children('.board').children('.pion-onboard').each(function(){
			var left = 100-$(this).csstop()-$(this).cssheight();
			var top = $(this).cssleft();
			$(this).css('top',top+'%').css('left',left+'%');
			if(!isNaN($(this).csstransform())){
				$(this).setcsstransform(nRot);
			}
		});
		$(this).parent().parent().children('.board').children('.board-svg').setcsstransform(nRot);
		$(this).parent().children('.fleche').setcsstransform(nRot);
	}
	
	function turnleftBoard(){
		var nRot=$(this).parent().parent().children('.board').children('.board-svg').csstransform();
		nRot=(nRot+3)%4;
		$(this).parent().parent().children('.board').children('.pion-onboard').each(function(){
			var top = 100-$(this).cssleft()-$(this).cssheight();
			var left = $(this).csstop();
			$(this).css('top',top+'%').css('left',left+'%');
			if(!isNaN($(this).csstransform())){
				$(this).setcsstransform(nRot);
			}
		});
		$(this).parent().parent().children('.board').children('.board-svg').setcsstransform(nRot);
		$(this).parent().children('.fleche').setcsstransform(nRot);
	}
	
	////////////////////// drag & drop ///////////////////////////////
	
	var FlagDrop = 0; //drag onbac
	
	var borderBacWidth = $('.bacBordure').css('border-top-width');//drop onbac
	
	var border ='';
	
	var draggableOnboard = {
		scroll:true,
		appendTo:'.drag',
		revert : 'invalid',
		containment : '.drag',
		revertDuration : 200,
		helper:'clone',
		zIndex:5,
		start : function(e,ui){
			$(this).addClass('noclic');
			$(this).css('display','none');
		},
		stop : function(e,ui){
			$(this).css('display','');
		}
	}
	
	var draggableOnbac = {
		scroll:true,
		appendTo:'.drag',
		revert : 'invalid',
		containment : '.drag',
		revertDuration : 200,
		helper : 'clone',
		zIndex:5,
		start: function(e,ui){
			var taille = $('.pion-onboard').first().css('height');
			var classTpDraggable=$(ui.helper).attr('class').match(/tp\-[^ ]*/).toString().substring(3);
			var taille2 =$('.groupe-onbac').children('.tp-'+classTpDraggable).css('height');
			if(taille !== undefined){
				$(ui.helper).css('height',taille);
			}else if (taille2!==undefined){
				$(ui.helper).css('height',taille2);
			}
			
			$('.nb-pions-onbac',$(this).parent()).text(parseInt($('.nb-pions-onbac',$(this).parent()).text())-1);
		},
		stop: function(){
			if(FlagDrop===0){
				$('.nb-pions-onbac',$(this).parent()).text(parseInt($('.nb-pions-onbac',$(this).parent()).text())+1);
			}else if(parseInt($('.nb-pions-onbac',$(this).parent()).text())>0){
				$('.nb-pions-onbac',$(this).parent()).text(parseInt($('.nb-pions-onbac',$(this).parent()).text())+1);
			}else{
				$('.nb-pions-onbac',$(this).parent()).text(parseInt($('.nb-pions-onbac',$(this).parent()).text())+1);
			}
			FlagDrop = 0;
		}
	}
	
	var draggableOnbox = {
		scroll:true,
		appendTo:'.drag',
		revert : 'invalid',
		containment : '.drag',
		revertDuration : 200,
		helper : 'clone',
		zIndex : 5,
		start: function(e,ui){
			var taille = $('.pion-onboard').first().css('height');
			var classTpDraggable=$(ui.helper).attr('class').match(/tp\-[^ ]*/).toString().substring(3);
			var taille2 =$('.box').children('.tp-'+classTpDraggable).css('height');
			if(taille !== undefined){
				$(ui.helper).css('height',taille);
			}else if (taille2!==undefined){
				$(ui.helper).css('height',taille2);
			}
		}
	}
	
	var draggableGroupeOnbac = {
		scroll:true,
		revert : true,
		containment : '.bacs',
		revertDuration : 200,
		zIndex : 5
	}
	
	var droppableBoard ={
		accept : '.pion-onboard, .pion-onbac, .pion-onbox',
		over : function(e,ui){
			if(!isNaN($(ui.helper).csstransform())){
				$(ui.helper).setcsstransform($(this).children('.board-svg').csstransform());
			}
		},
		out : function(e,ui){
			if(!isNaN($(ui.helper).csstransform())){
				$(ui.helper).setcsstransform(0);
			}
		},
		drop : function(e,ui){
			var l = ( 100 * (parseFloat($(ui.helper).offset().left)-parseFloat($(this).offset().left)) / parseFloat($(this).css("width")) );
			var t = ( 100 * (parseFloat($(ui.helper).offset().top)-parseFloat($(this).offset().top)) / parseFloat($(this).css("height")) );
			//si class draggable= pion-onboard :
			if($(ui.draggable).hasClass('pion-onboard')){
				var topleft=real2abs(l,t,$(this).parent().diese(),$(ui.draggable).cssheight());
				var top = topleft.top;
				var left = topleft.left;

				socket.emit('ModifJeu',{
					noeud_courant:noeudCourant,
					auteur:$('#auteurChat').val(),
					couleur:$('#couleurChat').val(),
					id_joueur:parseInt($('input[name=player]:checked').attr('value')),
					requetes:[{
						action:'changePionOnboard',
						data:{
							diese : $(ui.draggable).diese(),
							type:$(ui.draggable).tp(),
							top:top,
							left:left,
							height:$(ui.draggable).cssheight(),
							board:$(this).parent().diese(),
							svg:$(ui.draggable).attr('src').replace(/^.*images\/pions\//,'')
						}
					}]
				});
				/*HchangePionOnboard(	diese=$(ui.draggable).diese(),
									type=$(ui.draggable).tp(),
									top=t,
									left=l,
									height=$(ui.draggable).cssheight(),
									board=$(this).parent().diese(),
									$(ui.draggable).attr('src').replace(/^.*images\/pions\//,'')
				);*/
				
				// //si le parent est une autre board : on d�place le pion onboard
				// if(!$(this).is($(ui.draggable).parent())){
					// //$(ui.draggable).appendTo(this).attr('style',$(ui.helper).attr('style'));//.draggable(draggableOnboard).on('click',action_clic_onboard);
					// $(ui.draggable).appendTo(this).css('top',t).css('left',l);//.draggable(draggableOnboard).on('click',action_clic_onboard);
				// } else{
					// $(ui.draggable).appendTo(this).css('top',t).css('left',l);
				// }
			}
			//si class draggable= pion-box:
			if($(ui.draggable).hasClass('pion-onbox')){
				var classTpDraggable=$(ui.draggable).attr('class').match(/tp\-[^ ]*/).toString().substring(3);
				var height = tabPionTaille[tabPionListe.indexOf(classTpDraggable)];
				if(height===null | typeof height === 'undifined'){
					socket.emit('getTabPion',{id_joueur : joueurActif() });
				}else{
					var topleft=real2abs(l,t,$(this).parent().diese(),height);
					var top = topleft.top;
					var left = topleft.left;

					socket.emit('ModifJeu',{
						noeud_courant:noeudCourant,
						auteur:$('#auteurChat').val(),
						couleur:$('#couleurChat').val(),
						id_joueur:parseInt($('input[name=player]:checked').attr('value')),
						requetes:[{
							action:'createPionOnboard',
							data:{
								type : classTpDraggable,
								board : $(this).parent().diese(),top : top,left : left, height : height
							}
						}]
					});
				}
				// //on cr�e un ouveau pion onboard
				// //on deplace le helper en modifiant classe etc
				// //$(ui.draggable).clone().addClass('pion-onboard').removeClass('pion-onbox').appendTo(this).css('top',$(ui.helper).offset().top).css('left',$(ui.helper).offset().left).draggable(draggableOnboard);//.on('click',action_clic_onboard);
				// var type = $(ui.draggable).attr('class').match(/tp\-[^ ]*/).toString().substring(3);
				// $(creatPionOnboard(type)).appendTo(this).css('left',l).css('top',t).draggable(draggableOnboard);//.on('click',action_clic_onboard);
				// //$(ui.draggable).clone().addClass('pion-onboard').removeClass('pion-onbox').appendTo(this).css('top',t).css('left',l).css('max-height',).draggable(draggableOnboard);//.on('click',action_clic_onboard);
			}
			//si class draggable=bion-onbac:
			if($(ui.draggable).hasClass('pion-onbac')){
				//on cr�e un ouveau pion onboard
				
				//on deplace le helper en modifiant classe etc
				FlagDrop = 1;
				
				//est-ce que le groupe d'origine est vide ?
				var pionsRestant = parseInt($('.nb-pions-onbac',$(ui.draggable).parent()).text())>0;
				
				//type de pion
				var type = $(ui.draggable).tp();
				
				//taille du pion
				var height = tabPionTaille[tabPionListe.indexOf(type)];
				if(height===null | typeof height === 'undifined'){
					socket.emit('getTabPion',{id_joueur : joueurActif() });
				}else{
					//creation du pion
					var topleft=real2abs(l,t,$(this).parent().diese(),height);
					var top = topleft.top;
					var left = topleft.left;					
					if(pionsRestant){
						//on actualise le bac
						socket.emit('ModifJeu',{
							noeud_courant:noeudCourant,
							auteur:$('#auteurChat').val(),
							couleur:$('#couleurChat').val(),
							id_joueur:parseInt($('input[name=player]:checked').attr('value')),
							requetes:[{
								action:'createPionOnboard',
								data:{
									type : type,board : $(this).parent().diese(),
									top : top,left : left, height : height
								}},{
								action:'modifyGroup',
								data:{
									diese:$(ui.draggable).parent().diese(),
									type:type,
									n:parseInt($('.nb-pions-onbac',$(ui.draggable).parent()).text()),bac:$(ui.draggable).parent().parent().parent().diese()
								}
							}]
						});
					}else{
						//on supprime le bac
						socket.emit('ModifJeu',{
							noeud_courant:noeudCourant,
							auteur:$('#auteurChat').val(),
							couleur:$('#couleurChat').val(),
							id_joueur:parseInt($('input[name=player]:checked').attr('value')),
							requetes:[{
								action:'createPionOnboard',
								data:{
									type : type,board : $(this).parent().diese(),
									top : top,left : left, height : height
								}},{
								action:'removeElement',
								data:{
									id:$(ui.draggable).parent().attr('id')
								}
							}]
						});
						//HremoveElement($(ui.draggable).parent().attr('id'));
					}
				}
				// $(creatPionOnboard(type)).appendTo(this).css('left',l).css('top',t).draggable(draggableOnboard);//.on('click',action_clic_onboard);
				// //$(ui.draggable).clone().appendTo(this).addClass('pion-onboard').removeClass('pion-onbac').css('top',t).css('left',l).draggable(draggableOnboard);//.on('click',action_clic_onboard);;
				// //on supprime le groupe s'il n'y a plus de jeton
				// if(parseInt($('.nb-pions-onbac',$(ui.draggable).parent()).text())<1){
					// $(ui.draggable).parent().remove();
				// }
			}
			
		}
	}
	
	var droppableBac ={
		accept : '.pion-onboard, .pion-onbac, .pion-onbox, .groupe-onbac',
		over : function(e,ui){
			//orderWidth = $(this).css('border-top-width');
			$(this).css('border-width',(Number(/^[0-9.]*/.exec(borderBacWidth))*2.0)+'px');
		},
		out : function(e,ui){
			$(this).css('border-width',borderBacWidth);
			// borderWidth = 0;
		},
				
		drop : function(e,ui){
			
			$(this).css('border-width',borderBacWidth);
			//borderWidth = 0;
			
			var classTpDraggable=$(ui.draggable).attr('class').match(/tp\-[^ ]*/).toString().substring(3);
			var TpExist=false;
			$('.groupe-onbac.actif',this).each(function(){
				if($(this).attr('class').match(/tp\-[^ ]*/).toString().substring(3)==(classTpDraggable)){
					TpExist=true;
				}
			});

			if($(ui.draggable).hasClass('groupe-onbac') && ($(this).parent().diese()!=$(ui.draggable).parent().parent().diese())){//ATT pas m�me case
				if(!$(this).is($(ui.draggable).parent())){
					if(TpExist){
						var diese = $(this).children('.tp-'+classTpDraggable).filter('.actif').diese();
						//on rajoute tous les pions
						var n = parseInt($(ui.draggable).children('.nb-pions-onbac').text())+parseInt($(this).children('.tp-'+classTpDraggable).filter('.actif').children('.nb-pions-onbac').text());
						socket.emit('ModifJeu',{
							noeud_courant:noeudCourant,
							auteur:$('#auteurChat').val(),
							couleur:$('#couleurChat').val(),
							id_joueur:parseInt($('input[name=player]:checked').attr('value')),
							requetes:[{
								action:'modifyGroup',
								data:{
									diese : diese,n : n,bac: $(this).parent().diese(),type:classTpDraggable
								}
							},{
								action:'removeElement',
								data:{
									id:$(ui.draggable).attr('id')
								}
							}]
						});
						//HmodifyGroup(diese,n,$(this).parent().diese());
						//HremoveElement($(ui.draggable).attr('id'));
						
						// $('.nb-pions-onbac',$(this).children('.tp-'+classTpDraggable)).text(parseInt($('.nb-pions-onbac',$(this).children('.tp-'+classTpDraggable)).text())+parseInt($('.nb-pions-onbac',$(ui.draggable)).text()));//on rajoute les pions
						// $(ui.draggable).remove();//on supprime le pion on-board
					} else {
						
						socket.emit('ModifJeu',{
							noeud_courant:noeudCourant,
							auteur:$('#auteurChat').val(),
							couleur:$('#couleurChat').val(),
							id_joueur:parseInt($('input[name=player]:checked').attr('value')),
							requetes:[{
								action:'createGroupeOnbac',
								data:{
									type : classTpDraggable,
									 n : parseInt($(ui.draggable).children('.nb-pions-onbac').text()),
									 bac : $(this).parent().diese()
								}
							},{
								action:'removeElement',
								data:{
									id:$(ui.draggable).attr('id')
								}
							}]
						});
						
						//HremoveElement($(ui.draggable).attr('id'));
						
						// $(createGroupeOnbac(classTpDraggable)).appendTo(this).draggable(draggableGroupeOnbac).children('.pion-onbac').draggable(draggableOnbac);//on cr�e un groupe
						// $('.nb-pions-onbac',$(this).children('.tp-'+classTpDraggable)).text(parseInt($('.nb-pions-onbac',$(ui.draggable)).text()));//on rajoute les pions
						// $(ui.draggable).remove();//on supprime l'ancien groupe
					}	
				}
			} else {
				
				if($(ui.draggable).hasClass('pion-onboard')){
					if(TpExist){
						var diese = $(this).children('.tp-'+classTpDraggable).diese();
						var n = parseInt($(this).children('.tp-'+classTpDraggable).children('.nb-pions-onbac').text())+1;

						socket.emit('ModifJeu',{
							noeud_courant:noeudCourant,
							auteur:$('#auteurChat').val(),
							couleur:$('#couleurChat').val(),
							id_joueur:parseInt($('input[name=player]:checked').attr('value')),
							requetes:[{
								action:'modifyGroup',
								data:{
									diese : diese,n : n,bac: $(this).parent().diese(),type:classTpDraggable
								}
							},{
								action:'removeElement',
								data:{
									id:$(ui.draggable).attr('id')
								}
							}]
						});
						//HmodifyGroup(diese,n,$(this).parent().diese());
						//HremoveElement($(ui.draggable).attr('id'));
					} else {
						
						socket.emit('ModifJeu',{
							noeud_courant:noeudCourant,
							auteur:$('#auteurChat').val(),
							couleur:$('#couleurChat').val(),
							id_joueur:parseInt($('input[name=player]:checked').attr('value')),
							requetes:[{
								action:'createGroupeOnbac',
								data:{
									type : classTpDraggable, n : 1, bac : $(this).parent().diese()
								}
							},{
								action:'removeElement',
								data:{
									id:$(ui.draggable).attr('id')
								}
							}]
						});
						//HremoveElement($(ui.draggable).attr('id'));
					}
					
					// if(TpExist){
						// $('.nb-pions-onbac',$(this).children('.tp-'+classTpDraggable)).text(parseInt($('.nb-pions-onbac',$(this).children('.tp-'+classTpDraggable)).text())+1);//on rajoute un pion
						// $(ui.draggable).remove();//on supprime le pion on-board
					// } else {
						// $(ui.draggable).remove();//on supprime le pion on-board
					// }
				}
				if($(ui.draggable).hasClass('pion-onbac') && ($(this).parent().diese()!=$(ui.draggable).parent().parent().parent().diese())){//ATT pas m�me case
					if(!$(this).is($(ui.draggable).parent())){
						
						FlagDrop = 1;
						//est-ce que le groupe d'origine est vide ?
						var pionsRestant = parseInt($('.nb-pions-onbac',$(ui.draggable).parent()).text());
						if(pionsRestant>0){
							if(TpExist){
								var diese = $(this).children('.tp-'+classTpDraggable).filter('.actif').diese();
								var n = parseInt($(this).children('.tp-'+classTpDraggable).filter('.actif').children('.nb-pions-onbac').text())+1;
								socket.emit('ModifJeu',{
									noeud_courant:noeudCourant,
									auteur:$('#auteurChat').val(),
									couleur:$('#couleurChat').val(),
									id_joueur:parseInt($('input[name=player]:checked').attr('value')),
									requetes:[{
										action:'modifyGroup',
										data:{
											diese : diese,n : n,bac: $(this).parent().diese(),type:classTpDraggable
										}
									},{	action:'modifyGroup',
										data:{
											diese :$(ui.draggable).parent().diese() ,n : pionsRestant,
											bac:$(ui.draggable).parent().parent().parent().diese() ,type:classTpDraggable
										}
									}]
								});
								//socket.emit('removeElement',{id:$(ui.draggable).attr('id')});
								//HmodifyGroup(diese,n,$(this).parent().diese());
								//HremoveElement($(ui.draggable).attr('id'));
							} else {
		
								socket.emit('ModifJeu',{
									noeud_courant:noeudCourant,
									auteur:$('#auteurChat').val(),
									couleur:$('#couleurChat').val(),
									id_joueur:parseInt($('input[name=player]:checked').attr('value')),
									requetes:[{
										action:'createGroupeOnbac',
										data:{
											type : classTpDraggable, n : 1, bac : $(this).parent().diese()
										}
									},{
										action:'modifyGroup',
										data:{
											diese :$(ui.draggable).parent().diese() ,n : pionsRestant,
											bac:$(ui.draggable).parent().parent().parent().diese() ,type:classTpDraggable
										}
									}]								
								});
								//socket.emit('removeElement',{id:$(ui.draggable).attr('id')});
								//HremoveElement($(ui.draggable).attr('id'));
							}
						}else{
							if(TpExist){
								var diese = $(this).children('.tp-'+classTpDraggable).filter('.actif').diese();
								var n = parseInt($(this).children('.tp-'+classTpDraggable).filter('.actif').children('.nb-pions-onbac').text())+1;
								socket.emit('ModifJeu',{
									noeud_courant:noeudCourant,
									auteur:$('#auteurChat').val(),
									couleur:$('#couleurChat').val(),
									id_joueur:parseInt($('input[name=player]:checked').attr('value')),
									requetes:[{
										action:'modifyGroup',
										data:{
											diese : diese,n : n,bac: $(this).parent().diese(),type:classTpDraggable
										}
									},{
										action:'removeElement',
										data:{id:$(ui.draggable).parent().attr('id')}
									}]
								});
								//socket.emit('removeElement',{id:$(ui.draggable).attr('id')});
								//HmodifyGroup(diese,n,$(this).parent().diese());
								//HremoveElement($(ui.draggable).attr('id'));
							} else {
		
								socket.emit('ModifJeu',{
									noeud_courant:noeudCourant,
									auteur:$('#auteurChat').val(),
									couleur:$('#couleurChat').val(),
									id_joueur:parseInt($('input[name=player]:checked').attr('value')),
									requetes:[{
										action:'createGroupeOnbac',
										data:{
											type : classTpDraggable, n : 1, bac : $(this).parent().diese()
										}
									},{
										action:'removeElement',
										data:{id:$(ui.draggable).parent().attr('id')}
									}]
								});
								//socket.emit('removeElement',{id:$(ui.draggable).attr('id')});
								//HremoveElement($(ui.draggable).attr('id'));
							}
						}
						
						
						
						// if(TpExist){
							// $('.nb-pions-onbac',$(this).children('.tp-'+classTpDraggable)).text(parseInt($('.nb-pions-onbac',$(this).children('.tp-'+classTpDraggable)).text())+1);//on rajoute un pion
						// } else {
							// $(createGroupeOnbac(classTpDraggable)).appendTo(this).draggable(draggableGroupeOnbac).children('.pion-onbac').draggable(draggableOnbac);//on cr�e un groupe
						// }
						// if(parseInt($('.nb-pions-onbac',$(ui.draggable).parent()).text())<1){
							// socket.emit('removeElement',{id:$(ui.draggable).parent().attr('id')});
							// HremoveElement($(ui.draggable).parent().attr('id'));
							// //$(ui.draggable).parent().remove();
						// }
						
					}
				}
				if($(ui.draggable).hasClass('pion-onbox')){
					
					if(TpExist){
						var diese = $(this).children('.tp-'+classTpDraggable).filter('.actif').diese();
						var n = parseInt($(this).children('.tp-'+classTpDraggable).filter('.actif').children('.nb-pions-onbac').text())+1;
						socket.emit('ModifJeu',{
							noeud_courant:noeudCourant,
							auteur:$('#auteurChat').val(),
							couleur:$('#couleurChat').val(),
							id_joueur:parseInt($('input[name=player]:checked').attr('value')),
							requetes:[{
								action:'modifyGroup',
								data:{
									diese : diese,n : n,bac: $(this).parent().diese(),type:classTpDraggable
								}
							}]
						});
						//HmodifyGroup(diese,n,$(this).parent().diese());
					} else {
						socket.emit('ModifJeu',{
							noeud_courant:noeudCourant,
							auteur:$('#auteurChat').val(),
							couleur:$('#couleurChat').val(),
							id_joueur:parseInt($('input[name=player]:checked').attr('value')),
							requetes:[{
								action:'createGroupeOnbac',
								data:{
									type : classTpDraggable, n : 1, bac : $(this).parent().diese()
								}
							}]
						});
					}					
					// if(TpExist){
						// $('.nb-pions-onbac',$(this).children('.tp-'+classTpDraggable)).text(parseInt($('.nb-pions-onbac',$(this).children('.tp-'+classTpDraggable)).text())+1);//on rajoute un pion
					// } else {
						// $(createGroupeOnbac(classTpDraggable)).appendTo(this).draggable(draggableGroupeOnbac).children('.pion-onbac').draggable(draggableOnbac);//on cr�e un groupe
					// }
				}
				
			}
		}
	}
	
	
	var droppableTrash ={
		accept : '.pion-onboard, .pion-onbac, .pion-onbox, .groupe-onbac',
		over : function(e,ui){
			//borderWidth = $(this).css('border-top-width');
			$(this).css('border-width',(Number(borderBacWidth.substr(0,borderBacWidth.length-3))*2.0)+'px');
		},
		out : function(e,ui){
			$(this).css('border-width',borderBacWidth);
			 //borderWidth = 0;
		},

		drop : function(e,ui){
			
			$(this).css('border-width',borderBacWidth);
			//borderWidth = 0;

			//si class draggable=bion-onbac:
			if($(ui.draggable).hasClass('pion-onbac')){
				FlagDrop = 1;
				if(parseInt($('.nb-pions-onbac',$(ui.draggable).parent()).text())<1){
					socket.emit('ModifJeu',{
						noeud_courant:noeudCourant,
						auteur:$('#auteurChat').val(),
						couleur:$('#couleurChat').val(),
						id_joueur:parseInt($('input[name=player]:checked').attr('value')),
						requetes:[{
							action:'removeElement',
							data:{
								id:$(ui.draggable).parent().attr('id')
							}
						}]
					});
					//HremoveElement($(ui.draggable).parent().attr('id'));
				}
			}else if($(ui.draggable).hasClass('pion-onbox')){
				
			}else{
				socket.emit('ModifJeu',{
					noeud_courant:noeudCourant,
					auteur:$('#auteurChat').val(),
					couleur:$('#couleurChat').val(),
					id_joueur:parseInt($('input[name=player]:checked').attr('value')),
					requetes:[{
						action:'removeElement',
						data:{
							id:$(ui.draggable).attr('id')
						}
					}]
				});
				//HremoveElement($(ui.draggable).attr('id'));
			}
			// if($(ui.draggable).hasClass('pion-onboard')){
				// $(ui.draggable).remove();
			// }
			// if($(ui.draggable).hasClass('groupe-onbac')){
				// $(ui.draggable).remove();
			// }
		}
	}
	

	
	
	//////////////////////////// Choix de variables ///////////////////////////
	
	// switch(action_clic_onboardStr){
		// case 'deletePionOnboard' : var action_clic_onboard = deletePionOnboard; break;
		// case 'changePionOnboard' : var action_clic_onboard = changePionOnboard; break;
		// case 'movePionOnboard' : var action_clic_onboard = movePionOnboard; break;case 'movePionOnboard' : var action_clic_onboard = movePionOnboard; break;
	// }

	// switch(action_scroll_onboardStr){
		// case 'deletePionOnboard' : var action_scroll_onboard = deletePionOnboard; break;
		// case 'changePionOnboard' : var action_scroll_onboard = changePionOnboard; break;
		// case 'movePionOnboard' : var action_scroll_onboard = movePionOnboard; break;
	// }

	// switch(clickable_boardStr){
		// case 'clickable_boardNew' : var clickable_board = clickable_boardNew; break;
		// case 'clickable_boardMove' : var clickable_board = clickable_boardMove; break;
	// }
	
	var action_clic_onboard = eval(action_clic_onboardStr);
	var action_scroll_onboard = eval(action_scroll_onboardStr);
	var clickable_board = eval(clickable_boardStr);

	socket.emit('getTabPion',{id_joueur : parseInt($('input[name=player]:checked').attr('value'))});
	socket.emit('getAction',{id_joueur : parseInt($('input[name=player]:checked').attr('value'))});	
	


	//alert(tabPionListe[0]); changer �a

	///////////////////////// boutons du menus ////////////////////////////////////
	function hideMenu2(part){
		$(part).css('display','none');
		$('.drag, .chat').off('click.flag');
		initEvents();
		Menu2actif = '';
	}
	
	function affMenu2(part){
		if(Menu2actif===''){
			$(part).css('display','flex');
			stopEvents();
			$('.drag, .chat').on('click.flag',function(){
				hideMenu2(part);
			});
			Menu2actif=part;
		}else if(Menu2actif===part){
			hideMenu2(part);
		}else{
			$(part).css('display','flex');
			$('.drag, .chat').off('click.flag');
			$(Menu2actif).css('display','none');
			$('.drag, .chat').on('click.flag',function(){
				hideMenu2(part);
			});
			Menu2actif=part;
		}
		
		
	}
	 var Menu2actif = '';
	
	
	
	$('#Btn-info').on('click',function(){
		affMenu2('#info2');
	});
	$('#Btn-erase').on('click',function(){
		affMenu2('#erase2');
	});
	$('#conf-erase').on('click',function(){
		//Herase();
		socket.emit('ModifJeu',{
			noeud_courant:noeudCourant,
			auteur:$('#auteurChat').val(),
			couleur:$('#couleurChat').val(),
			id_joueur:parseInt($('input[name=player]:checked').attr('value')),
			requetes:[{
				action:'erase',
				data:{}
			}]
		});
		hideMenu2('#erase2');
	});
	$('#ann-erase').on('click',function(){
		hideMenu2('#erase2');
	});
	
	$('#Btn-delete').on('click',function(){
		affMenu2('#delete2');
	});
	$('#conf-delete').on('click',function(){
		socket.emit('delete','');
		hideMenu2('#delete2');
	});
	$('#ann-delete').on('click',function(){
		hideMenu2('#delete2');
	});
	
	$('#Btn-raz').on('click',function(){
		affMenu2('#raz2');
	});
	$('#conf-raz').on('click',function(){
		socket.emit('raz','');
		hideMenu2('#raz2');
	});
	$('#ann-raz').on('click',function(){
		hideMenu2('#raz2');
	});
	
	$('#Btn-addmenu').on('click',function(){
		affMenu2('#add2');
	});
	$('#ann-add2').on('click',function(){
		hideMenu2('#add2');
	});
	$('#add_bac').on('click',function(){
		socket.emit('ModifJeu',{
			noeud_courant:noeudCourant,
			auteur:$('#auteurChat').val(),
			couleur:$('#couleurChat').val(),
			id_joueur:parseInt($('input[name=player]:checked').attr('value')),
			requetes:[{
				action:'add_bac',
				data:{
					label:'',
				}
			}]
		});
	});
	$('#add_trash').on('click',function(){
		Hadd_trash();
		socket.emit('add_trash','');
	});
	$('#add_box').on('click',function(e){
		var nom = newBoxType();
		// if (tabPionListe.indexOf('tp-'+nom)===-1){
			// tabPionListe.push('tp-'+nom);
			// tabPionChange.push('tp-'+nom);
			// tabPionMove.push(0);
		// }
		//socket.emit('setTabPion',{nom : nom});

		socket.emit('ModifJeu',{
			noeud_courant:noeudCourant,
			auteur:$('#auteurChat').val(),
			couleur:$('#couleurChat').val(),
			id_joueur:parseInt($('input[name=player]:checked').attr('value')),
			requetes:[{
				action:'add_box',
				data:{
					nom : nom,
				}
			}]
		});
	});
	$('#add_board').on('click',function(e){
		var nom = newBoardType();

		socket.emit('ModifJeu',{
			noeud_courant:noeudCourant,
			auteur:$('#auteurChat').val(),
			couleur:$('#couleurChat').val(),
			id_joueur:parseInt($('input[name=player]:checked').attr('value')),
			requetes:[{
				action:'add_board',
				data:{
					nom : nom,
				}
			}]
		});
		//tailleBoard();
	});
	
	$('#Btn-delete_element').on('click',function(e){

		stopEvents();
		
		$('.boards, .bacs, .boxes').on('click.del','.board-content, .box, .bac, .trash',function(e){
			if($(this).hasClass('trash')){
				
				socket.emit('remove_trash');
				/*socket.emit('ModifJeu',{
					noeud_courant:noeudCourant,
					auteur:$('#auteurChat').val(),
					couleur:$('#couleurChat').val(),
					id_joueur:parseInt($('input[name=player]:checked').attr('value')),
					requetes:[{
						action:'removeElement',
						data:{
							id : 'trash',
						}
					}]
				});*/
				//HremoveElement('trash');				
			}else{
				socket.emit('ModifJeu',{
					noeud_courant:noeudCourant,
					auteur:$('#auteurChat').val(),
					couleur:$('#couleurChat').val(),
					id_joueur:parseInt($('input[name=player]:checked').attr('value')),
					requetes:[{
						action:'removeElement',
						data:{
							id : $(this).attr('id'),
						}
					}]
				});
				//HremoveElement($(this).attr('id'));
			}
		});
		$('.boards, .bacs, .boxes').on('mouseenter.del','.board-content, .box, .bac, .trash',function(e){
			border = $(this).css('border');
			$(this).css('border','solid 5px rgb(130,20,20)');
			});
		$('.boards, .bacs, .boxes').on('mouseleave.del','.board-content, .box, .bac, .trash',function(e){
			$(this).css('border',border);
			border = '';
			});
			
		$('#Btn-delete_element').css('display','none');
		$('#Btn-annuler_delete_element').css('display','block');
	});
	
	$('#Btn-annuler_delete_element').on('click',function(e){
		$('.boards, .bacs, .boxes').off('.del','.board-content, .box, .bac, .trash');
		initEvents();
		$('#Btn-delete_element').css('display','block');
		$('#Btn-annuler_delete_element').css('display','none');
	});
	
	$('#Btn-changerjoueur').on('click',function(){
		affMenu2('#changerjoueur2');
	});
	
	$('#ann-changerjoueur2').on('click',function(){
		hideMenu2('#changerjoueur2');
	});
	
	$('input[name=player]').on('change',function(e){
		if($(this).is(':checked')){
			socket.emit('getTabPion',{id_joueur : parseInt($(this).attr('value'))});
			socket.emit('getAction',{id_joueur : parseInt($(this).attr('value'))});
			$('#Btn-changerjoueur').children('p').text($(this).next().text());
		}
		hideMenu2('#changerjoueur2');
	});
	(function(){
		socket.emit('getTabPion',{id_joueur : parseInt($('input[name=player]:checked').attr('value'))});
		socket.emit('getAction',{id_joueur : parseInt($('input[name=player]:checked').attr('value'))});
		$('#Btn-changerjoueur').children('p').text($('input[name=player]:checked').next().text());
	})();
	
	/* $('#envoyerChat').on('click',function(e){
		socket.emit('chatNew',{
			auteur:$('#auteurChat').val(),
			couleur:$('#couleurChat').val(),
			message:$('#texteChat').val()
		});
		$('#texteChat').val('');
	}); */
	
	$('#Btn-chatmenu').on('click',function(){
		if(Menu2actif===''){
			$('#menuchat2').css('display','flex');
			stopEvents();
			$('.drag, .chat').on('click.flag',function(){
				$('#formMenuchat2').trigger('submit');
			});
			Menu2actif='#menuchat2';
		}else if(Menu2actif==='#menuchat2'){
			$('#formMenuchat2').trigger('submit');
		}else{
			$('#menuchat2').css('display','flex');
			$('.drag, .chat').off('click.flag');
			$(Menu2actif).css('display','none');
			$('.drag, .chat').on('click.flag',function(){
				$('#formMenuchat2').trigger('submit');
			});
			Menu2actif='#menuchat2';
		}
	});
	$('#formMenuchat2').on('submit',function(e){
		hideMenu2('#menuchat2');
		$('#Btn-chatmenu').children('p').children('span').text($('#auteurChat').val());
		couleur=$('#couleurChat').val();
		R=parseInt(couleur.substr(1,2),16);
		G=parseInt(couleur.substr(3,2),16);
		B=parseInt(couleur.substr(5,2),16);
		if((G+R+B)>382){
		colT="#000000";
		}else{
			colT="#ffffff";
		}
		$('#Btn-chatmenu').children('p').children('span').css('background-color',couleur);
		$('#Btn-chatmenu').children('p').children('span').css('color',colT);
	});
	
	(function(){
		$('#Btn-chatmenu').children('p').children('span').text($('#auteurChat').val());
		couleur=$('#couleurChat').val();
		R=parseInt(couleur.substr(1,2),16);
		G=parseInt(couleur.substr(3,2),16);
		B=parseInt(couleur.substr(5,2),16);
		if((G+R+B)>382){
		colT="#000000";
		}else{
			colT="#ffffff";
		}
		$('#Btn-chatmenu').children('p').children('span').css('background-color',couleur);
		$('#Btn-chatmenu').children('p').children('span').css('color',colT);
	})();
	
	$('#formChat').on('submit',function(e){
		socket.emit('chatNew',{
			auteur:$('#auteurChat').val(),
			couleur:$('#couleurChat').val(),
			message:$('#texteChat').val()
		});
		$('#texteChat').val('');
	});
	
	
	//########""
	/*
	$('#BB-chat2').on('click',function(){
		$('.bacs').css('display','none');
		$('.chat').css('display','block');
		$('.drag').css('width','calc(100% - 200px)');
		$('.drag').css('justify-content','space-around');
	});
	
	$('#BB-bacs2').on('click',function(){
		$('.bacs').css('display','');
		$('.chat').css('display','');
		$('.drag').css('width','');
		$('.drag').css('justify-content','');

	});
	*/
	
	$('#BB-bacs').on('click',function(){
		$('#cssJS').text('');
	});
		
	$('#BB-arbre').on('click',function(){
		$('#cssJS').text(
			'.chat{display:none}'+
			'.arbre{display:block}'
		);
	});
	$('#BB-chat').on('click',function(){
		$('#cssJS').text(
		'.chat{display:block}'+
		'.arbre{display:none}'+
		'@media (max-width:800px) and (min-width:401px){'+
			'.bacs{display:none;}'+
			'.chat{display:block;}'+
			'.drag{width:calc(100% - 200px);justify-content:space-around;}'+
		'}'+
		'@media (max-width:600px) and (min-width:401px){'+
			'.drag{width:calc(100% - 150px);justify-content:space-between;}'+
		'}'
		);
	});
	$('#BB-chat4').on('click',function(){
		if($('#cssJSchat').text()===''){
			$('#cssJSchat').text(
				'@media (max-width:400px){'+
					'.chat{display:block;}'+
				'}'
			);
		}else{
			$('#cssJSchat').text('');
		}
	});
	$('#BB-boxes').on('click',function(){
		$('#cssJS').text(
		'@media (max-width:600px){'+
			'.bacs{display:none;}'+
			'.boxes{display:flex;}'+
		'}'
		);
	});
	$('#BB-menu').on('click',function(){
		$('#cssJS').text(
		'@media (max-width:400px){'+
			'.bacs{display:none;}'+
			'.menu-rang1{display:flex;}'+
			'.drag{top:100px;height:calc(100% - 100px);}'+
		'}'
		);
	});
	
	$('#Btn-couppre').on('mouseenter',function(){
		afficheNoeud(noeuds_precedents[noeuds_suivants.indexOf(noeudCourant)]);
	});
	
	$('#Btn-couppre').on('mouseleave',function(){
		afficheNoeud(noeudCourant);
	});
			
	$('.noeud').on('mouseenter',function(e){afficheNoeud(parseInt($(this).attr('id').replace(/^.*-/,'')));});
	$('.noeud').on('mouseleave',function(e){afficheNoeud(noeudCourant);});
	$('.noeudAutre').on('click',function(){socket.emit('changeNoeud',{noeud:parseInt($(this).attr('id').replace(/^.*-/,''))});});
	/////////////////////////// On lance tous les �v�nements //////////////////////////////////////////////////
	
	function initEvents(){
		$('.boards').on('click.standard','.pion-onboard',action_clic_onboard);
		$('.boards').on('mousewheel DOMMouseScroll','.pion-onboard',action_scroll_onboard);
		$('.pion-onboard').draggable(draggableOnboard);
		$('.pion-onbac').draggable(draggableOnbac);
		$('.pion-onbox').draggable(draggableOnbox);
		$('.bacBordure').droppable(droppableBac);
		$('.board').droppable(droppableBoard);
		$('.trash').droppable(droppableTrash);
		$('.boards').on('click.standard','.board',clickable_board);
		$('.boards').on('click.standard','.bouton-turnup',turnupBoard);
		$('.boards').on('click.standard','.bouton-turnright',turnrightBoard);
		$('.boards').on('click.standard','.bouton-turnleft',turnleftBoard);
		$('.groupe-onbac').draggable(draggableGroupeOnbac);
		//$('.boxes').on('click','.del-box',deleteBox);
		$('.bacs').on('click.standard','.del-groupe',deleteGroupe);
		$('.bacs').on('click.standard','.edit-groupe',displayEditGroupe);
		$('.bacs').on('submit','.inputN',editGroupe);
		$('.bacs').on('focusout','.inputN input',function(e){$(this).parent().submit();});
		$('.bacs').on('click.standard','.edit-bac',displayEditLabelBac);
		$('.bacs').on('submit','.inputLabelBac',editLabelBac);
		$('.bacs').on('focusout','.inputLabelBac input',function(e){$(this).parent().submit();});
	}
	initEvents();
	
	function stopEvents(){
		if($('.pion-onboard').length>0){
			$('.pion-onboard').draggable('destroy');
			$('.boards').off('click.standard','.pion-onboard',action_clic_onboard);
			$('.boards').off('click.standard','.pion-onboard',action_scroll_onboard);
		}
		if($('.pion-onbac').length>0){
			$('.pion-onbac').draggable('destroy');
			$('.bacs').off('click.standard','.del-groupe',deleteGroupe);
			$('.bacs').off('click.standard','.edit-groupe',displayEditGroupe);
			$('.bacs').off('submit','.inputN',editGroupe);
		}
		if($('.pion-onbox').length>0){
			$('.pion-onbox').draggable('destroy');
		}
		if($('.bac').length>0){
			$('.bacBordure').droppable('destroy');
		}
		if($('.board').length>0){
			$('.board').droppable('destroy');
			$('.boards').off('click.standard','.board',clickable_board);
			$('.boards').on('click.standard','.bouton-turnup',turnupBoard);
			$('.boards').on('click.standard','.bouton-turnright',turnrightBoard);
			$('.boards').on('click.standard','.bouton-turnleft',turnleftBoard);
		}
		if($('.trash').length>0){
			$('.trash').droppable('destroy');
		}
		if($('.groupe-onbac').length>0){
			$('.groupe-onbac').draggable('destroy');
		}
		//$('.boxes').on('click','.del-box',deleteBox);
		$('.bacs').off('click.standard','.del-groupe',deleteGroupe);
	}
	
	
	
	var flagLabel=false;
	var flagN=false;
	
	

	
	////////////////////////// Recevoir les donn�es du serveur //////////////////////////
	
	socket.on('noname',function(){
		socket.emit('newplatoo',nomPlatoo);
	});
	
	socket.on('erase',function(data){
		Herase(data.noeud);
	});
	
	socket.on('add_bac',function(data){
		Hadd_bac(data.diese,data.label,data.num,data.noeud);
	});
	
	socket.on('add_trash',function(){
		Hadd_trash();
	});
	
	socket.on('add_box',function(data){
		Hadd_box(data.nom,data.diese,data.svg,data.num,data.noeud);
	});
	
	socket.on('add_board',function(data){
		Hadd_board(data.nom,data.diese,data.svg,data.num,data.noeud)
	});
	
	socket.on('removeElement',function(data){
		HremoveElement(data.id,data.noeud);
	});
	
/*	socket.on('changePionOnboard',function(data){
		var topleft = abs2real(data.left,data.top,data.board,data.height);
		var left = topleft.left;
		var top = topleft.top;
		HchangePionOnboard(diese=data.diese,type=data.type,top=top,left=left,height=data.height,board=data.board,svg=data.svg);
	});*/
	
	socket.on('createGroupeOnbac',function(data){
		HcreateGroupeOnbac(type=data.type,diese=data.diese,n=data.n,bac=data.bac,svg=data.svg,num=data.num,noeud=data.noeud);
	});
	
	socket.on('createPionOnboard',function(data){
		var topleft = abs2real(data.left,data.top,data.board,data.height);
		var left = topleft.left;
		var top = topleft.top;
		HcreatePionOnboard(data.type,data.diese,data.board,top,left,data.height,data.svg,data.directed,data.num,data.noeud);
	});
	
	/*socket.on('modifyGroup',function(data){
		console.log('attatta');
		HmodifyGroup(data.diese,data.n,data.bac);
	});*/
	
	socket.on('getTabPion',function(){
		socket.emit('getTabPion',{id_joueur : joueurActif() });
	});

	socket.on('setTabPion',function(data){
		tabPionListe=data.tabPionListe;
		tabPionChange=data.tabPionChange;
		tabPionMove=data.tabPionMove;
		tabPionTaille=data.tabPionTaille;
		tabPionSvg=data.tabPionSvg;
	});
	
	socket.on('setAction',function(data){
		
		action_clic_onboardStr =  data.action_clic_onboard;
		action_scroll_onboardStr = data.action_scroll_onboard;
		clickable_boardStr = data.clickable_board;
		click_board_create_type = data.click_board_create_type;
		click_board_move_type = data.click_board_move_type;
		click_board_move_bac = data.click_board_move_bac;
		// switch(action_clic_onboardStr){
			// case 'deletePionOnboard' : var action_clic_onboard = deletePionOnboard; break;
			// case 'changePionOnboard' : var action_clic_onboard = changePionOnboard; break;
			// case 'movePionOnboard' : var action_clic_onboard = movePionOnboard; break;
		// }

		// switch(action_scroll_onboardStr){
			// case 'deletePionOnboard' : var action_scroll_onboard = deletePionOnboard; break;
			// case 'changePionOnboard' : var action_scroll_onboard = changePionOnboard; break;
			// case 'movePionOnboard' : var action_scroll_onboard = movePionOnboard; break;
		// }

		// switch(clickable_boardStr){
			// case 'clickable_boardNew' : var clickable_board = clickable_boardNew; break;
			// case 'clickable_boardMove' : var clickable_board = clickable_boardMove; break;
		// }
			
		var action_clic_onboard = eval(action_clic_onboardStr);
		var action_scroll_onboard = eval(action_scroll_onboardStr);
		var clickable_board = eval(clickable_boardStr);
	});
	
	socket.on('changeLabelBac',function(data){
		data.label = data.label.substring(0,20);
		HchangeLabelBac(data.diese,data.label,data.oldnum,data.num,data.noeud);
	});
	
	socket.on('refresh',function(){
		location.reload();
	});
	
	socket.on('chatNew',function(data){
		Hchat(data.time,data.couleur,data.auteur,data.message,data.id);
	});
	
	socket.on('newNoeud',function(data){
 		noeudCourant=data.noeud;
		
		//mise � jour de Save
		Save.push(data.save);
		
		
		//mise � jour de noeuds_precedents,noeuds_suivants
		//var noeuds_suivants=[];
		//var noeuds_precedents=[];
		
		//for(var i=0; i< Save.length;i++){
		//	noeuds_suivants.push(Save[i].noeud_suivant)
		//	noeuds_precedents.push(Save[i].noeud_precedent)
		//}
		
		noeuds_suivants.push(data.save.noeud_suivant)
		noeuds_precedents.push(data.save.noeud_precedent)

		//mise � jour des noeuds � afficher
		noeudAff = [noeudCourant];
		var new_noeud=0;
		while((noeudAff.indexOf(0)==-1) && (noeuds_suivants.indexOf(noeudAff[noeudAff.length-1])!=-1)){
			new_noeud=noeuds_precedents[noeuds_suivants.indexOf(noeudAff[noeudAff.length-1])];
			noeudAff.push(new_noeud);
		}
		if(noeudAff[noeudAff.length-1]==0){
			noeudAff.pop();
		}
		
		//mise � jour des param�tres x, y, xpre, ypre de Save
		
		var noeuds_sui=[1];
		var noeuds_pre=[0];
		var y=[0];
		var epaisseur=[1];
		//on a d�j� fait le premier noeud
		for(var i = 1; i<Save.length;i++){
			y_pre_ind=Save[noeuds_sui.indexOf(Save[i].noeud_precedent)].y;
			epais=epaisseur[noeuds_sui.indexOf(Save[i].noeud_precedent)];
			
			//Save.y au debut indique l'indice dans le vecteur y
			if(noeuds_pre.indexOf(Save[i].noeud_precedent)===-1){
				//il n'y a pas de nouveau embranchement
				new_y_ind=y_pre_ind;
			}else{
				//nouvel embranchement
				
				//la valeur du nouveau y
				new_y = y[y_pre_ind] + epais;
				
				//on corrige le vecteur y
				for(var j=0;j<y.length;j++){
					if(y[j]>=new_y){
						y[j]=y[j]+1;
					}
				}
				
				//on ajoute le nouveau y
				y.push(new_y)
				
				//nouvel indice
				new_y_ind=y.length-1;
				
				//on corrige l'epaisseur
				var noe = Save[i].noeud_precedent
				while(noe>0){
					epaisseur[noeuds_sui.indexOf(noe)]+=1;
					noe=Save[noeuds_sui.indexOf(noe)].noeud_precedent
				}
				epais=1;
			}
			
			Save[i].x=Save[noeuds_sui.indexOf(Save[i].noeud_precedent)].x+1
			Save[i].y=new_y_ind
			Save[i].ypre=y_pre_ind
			Save[i].xpre=Save[noeuds_sui.indexOf(Save[i].noeud_precedent)].x
			
			noeuds_sui.push(Save[i].noeud_suivant);
			noeuds_pre.push(Save[i].noeud_precedent);
			epaisseur.push(epais);
		}
		
		SaveYmax=Math.max(...y);
		
		//on cherche xmax et on remplace les indices de y par les vraies valeurs
		xmax=0;
		for(var i = 0; i<Save.length ;i++){
			if(Save[i].x>xmax){
				xmax=Save[i].x
			}
			Save[i].y = y[Save[i].y]
			Save[i].ypre = y[Save[i].ypre]
			
		}
		
		SaveXmax=xmax;		
		
		//mise � jour des noeuds svg : on reconstruit tout l'arbre
		var arbre='<svg id="arbre-svg" width="'+((SaveYmax+2)*20)+'px" height="'+((SaveXmax+2)*25)+'px">';
		for(var i=1;i<Save.length; i++){
			arbre += '<line y1="'+(Save[i].x+1)/(SaveXmax+2)*100+'%" x1="'+(Save[i].y+1)/(SaveYmax+2)*100+'%" y2="'+
			(Save[i].xpre+1)/(SaveXmax+2)*100+'%" x2="'+(Save[i].ypre+1)/(SaveYmax+2)*100+
			'%" style="stroke:rgb(0,0,0);stroke-width:2" id="lien-'+Save[i].noeud_precedent+'-'+Save[i].noeud_suivant+'"/>';
		}
		for(var i=0;i<Save.length; i++){
			if(Save[i].noeud_suivant==noeudCourant){
				var classN='noeudCourant';
			}else{
				var classN='noeudAutre';
			}
			arbre += '<circle class="noeud '+classN+'" cy="'+(Save[i].x+1)/(SaveXmax+2)*100+'%" cx="'+(Save[i].y+1)/(SaveYmax+2)*100+
			'%" id="noeud-'+Save[i].noeud_suivant+'"/>';
		}
		arbre += '</svg>';
		
		$('#arbre-svg').remove();
		$(arbre).appendTo('.arbre');
		$('.noeud').on('mouseenter',function(e){afficheNoeud(parseInt($(this).attr('id').replace(/^.*-/,'')));});
		$('.noeud').on('mouseleave',function(e){afficheNoeud(noeudCourant);});
		$('.noeud').off('click');
		$('.noeudAutre').on('click',function(){socket.emit('changeNoeud',{noeud:parseInt($(this).attr('id').replace(/^.*-/,''))});});
	});
		
		
	socket.on('changeNoeud',function(data){
		noeudCourant=data.noeud;
		
		//mise � jour des noeuds � afficher
		var noeudAff = [noeudCourant];
		var new_noeud=0;
		while((noeudAff.indexOf(0)==-1) && (noeuds_suivants.indexOf(noeudAff[noeudAff.length-1])!=-1)){
			new_noeud=noeuds_precedents[noeuds_suivants.indexOf(noeudAff[noeudAff.length-1])];
			noeudAff.push(new_noeud);
		}
		if(noeudAff[noeudAff.length-1]==0){
			noeudAff.pop();
		}
		
		//maj le noeud courant
		$('.noeud').removeClass('noeudCourant').addClass('noeudAutre');
		$('#noeud-'+noeudCourant).removeClass('noeudAutre').addClass('noeudCourant');
		
		});
	
	// socket.on('deletePionOnboard',function(data){
		// $('#board-'+data.indexBoard).children('#pion-'+data.indexPion).remove();
	// });
	// socket.on('deleteGroupe',function(data){
		// $('#bac-'+data.indexBac).children('#groupe-'+data.indexPion).remove();
	// });
	
	//////////////////////////Arranger la taille du plateau///////////////
	$( window ).on( "load",function(){tailleBoard();})
	$('.board-svg').on( "load",function(){tailleBoard();})
	//$(document).ready(function(){tailleBoard();});
	

	
});
