//on charge les modules et on initialise les paramètres par défaut
const express = require('express');
var app = express();
const server = require('http').Server(app);//https?
const io = require('socket.io')(server);

const bodyParser = require('body-parser');//changer payant ?
var urlencodedParser = bodyParser.urlencoded({ extended: false, parameterLimit:10 });//changer
const async = require('async');

/*const { Pool } = require('pg');
var  connection = new Pool({
  user: 'admin',
  host: '127.0.0.1',
  database: 'platoo',
  password: 'm1y2s3q4l5',
  port: 5432,
})*/

// notice here I'm requiring my database adapter file
// and not requiring node-postgres directly
const connection = require('./db')



//var sqlinjection = require('sql-injection');//necessaire ?
var pattUrl =/^[0-9A-Za-z_-àéèêîïëù]+$/;
var affTime=/[0-9]{2} [0-9]{4} [0-9]{2}:[0-9]{2}:[0-9]{2}/;
//var HTnoeud =/^(?:[^']|(?:\\'))*(?:(?:[^\\]'(?:[^']|(?:\\'))*){2})*#NOEUDS(?:[^']|(?:\\'))*(?:(?:[^\\]'(?:[^']|(?:\\'))*){2})*$/g
var HTnoeud =/(?<=^([^']|\\')*(('([^']|\\')*){2})*)#NOEUDS(?=([^']|\\')*(('([^']|\\')*){2})*$)/g
/* 	/^
	(?:[^']|(?=\\)')*
	(?:
		(?:
			(?!\\)'
			(?:[^']|(?=\\)')*
		){2}
	)*
	#NOEUD
	(?:[^']|(?=\\)')*
	(?:
		(?:
			(?!\\)'
			(?:[^']|(?=\\)')*
		){2}
	)*
	$/ */
	
	/* 	/
	(?<=^
		([^']|\\')*
	(
		(
			'
			([^']|\\')*
		){2}
	)*
	)
	#NOEUD
	(?=
		([^']|\\')*
	(
		(
			'
			([^']|\\')*
		){2}
	)*
	$)
	/g */


//aync
//ejs

//icone
//ent
//socketio

//const pool = new Pool({
/*const connection = new Pool({
  user: 'admin',
  host: '127.0.0.1',
  database: 'platoopg',
  password: 'm1y2s3q4l5',
  port: 5432,
})*/


var NBNOEUD=1024;

function randomString(length, chars) {
	var mask = '';
	if (chars.indexOf('a') > -1) mask += 'abcdefghijklmnopqrstuvwxyz';
	if (chars.indexOf('A') > -1) mask += 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
	if (chars.indexOf('#') > -1) mask += '0123456789';
	if (chars.indexOf('!') > -1) mask += '~`!@#$%^&*()_+-={}[]:";\'<>?,./|\\';
	var result = '';
	for (var i = length; i > 0; --i) result += mask[Math.floor(Math.random() * mask.length)];
	return result;
}

//intersection of two arrys
//stack overflow
function intersect(a, b) {
    var t;
    if (b.length > a.length) t = b, b = a, a = t; // indexOf to loop over shorter
    return a.filter(function (e) {
        return b.indexOf(e) > -1;
    });
}

//il s'agit d'une nouvelle méthode de socket. les socket ont été remplacé par this
function executeSocket(variables={},needsData=true,errMess='',data={},FUN=function(resIP){}){
	//console.log(data)
	if(this.nomPlatoo && typeof this.nomPlatoo==='string'){
		this.nomPlatoo=this.nomPlatoo.substring(0,50);

		if(needsData){
			var n=Object.keys(variables).length;
			var k = 0;
			var valide = (typeof data ==='object');
			while((valide)&&(k<n)){
				valide = valide && ( (typeof data[Object.keys(variables)[k]]) === typeof variables[Object.keys(variables)[k]]);
				if((typeof data[Object.keys(variables)[k]]) === 'string'){
					data[Object.keys(variables)[k]].substring(0,parseInt(variables[Object.keys(variables)[k]]));
				}
				k+=1;
			}
		}else{
			var valide = true;
		}

		if(valide){
			
			var refresh = function(){
				this.emit('refresh');
			}
			
			const query = {
			  text:  'SELECT id_page,noeud_courant,id_jeu_depart FROM page WHERE nom_page = $1;',
			  values: [this.nomPlatoo],
			}
			
			connection.query(query,function(err,resIP){
				if(err){
					console.log('erreur de '+errMess+' : '+err.stack);
				}else{
					if(resIP.rows.length>0){
						//results[0].length>0 & results[1].length>0 & results[2].length>0
						FUN(resIP.rows);
					}else{
						console.log('erreur de '+errMess+' : bad id_page');
					}
				}
			});
				
		}else{
			console.log(errMess+'.data incorrect dans executeSocket');
			this.emit('refresh');
		}
	}else {
		this.emit('noname');
	}
}

function checkValide(variables={},needsData=true,errMess='',data={}){
	if(needsData){
		var n=Object.keys(variables).length;
		var k = 0;
		var valide = (typeof data ==='object');
		while((valide)&&(k<n)){
			valide = valide && ( (typeof data[Object.keys(variables)[k]]) === typeof variables[Object.keys(variables)[k]]);
			if((typeof data[Object.keys(variables)[k]]) === 'string'){
				data[Object.keys(variables)[k]].substring(0,parseInt(variables[Object.keys(variables)[k]]));
			}
			k+=1;
		}
	}else{
		var valide = true;
	}
	return valide										
}

/*function NoeudsBDD(resIP,errFun,dataErrFun,queryString,FUN,errMess='Erreur in NoeudsBDD :'){
	const query = {
	  text:  'SELECT noeud_precedent,noeud_suivant FROM save_on_page WHERE id_page = $1;',
	  values: [resIP[0].id_page],
	}
	connection.query(query,function(errlinks,reslinks){
		if(errlinks){
				console.log(errMess+errlinks.stack);
				//res.redirect('/accueil');//passer le nom en paramètre?
				errFun(dataErrFun);
				return false;
		}else{
			var noeuds=[resIP[0].noeud_courant];
			var noeuds_suivants=[];
			var noeuds_precedents=[];
			for(var i=0; i< reslinks.rows.length;i++){
				noeuds_suivants.push(reslinks.rows[i].noeud_suivant)
			}
			for(var i=0; i< reslinks.rows.length;i++){
				noeuds_precedents.push(reslinks.rows[i].noeud_precedent)
			}
			//if(typeof noeuds_suivants == 'number'){
			//	noeuds_suivants=[noeuds_suivants];
			//}
			//if(typeof noeuds_precedents == 'number'){
			//	noeuds_precedents=[noeuds_precedents];
			//}
			while((noeuds.indexOf(0)==-1) && (noeuds_suivants.indexOf(noeuds[noeuds.length-1])!=-1)){
				noeuds.push(noeuds_precedents[noeuds_suivants.indexOf(noeuds[noeuds.length-1])]);
			}
			noeuds=noeuds.toString().replace(/,0/,'');
			noeuds=noeuds.toString().replace(/^0,/,'');
			//queryString.replace(HTnoeud,noeuds);
			queryString=queryString.replace(HTnoeud,noeuds);
			//console.log(queryString);
			connection.query(queryString,FUN);
			return true;
		}
	});
}*/

function queryChainedClient(client,query,callback,data,resTot){
	client.query(query,function(err,results){
		if(err){
			//err.message=err.stack;
			callback(err);
		}else{
			resTot.push(results.rows);
			callback(null,data,resTot);
		}
	});
};

connection.queryChained = function(query,callback,data,resTot){
	this.query(query,function(err,results){
		if(err){
			//err.message=err.stack;
			callback(err);
		}else{
			resTot.push(results.rows);
			callback(null,data,resTot);
		}
	});
};

server.listen(8080);

app.use(express.static(__dirname + '/public'));

//empêche le injection sql
//app.use(sqlinjection);

//on fait les routes avec express
app

.get('/',function(req,res){
	res.redirect('/accueil');
})

.get('/accueil/',function(req,res){
	
	/*connection.query('select * from information_schema.tables',function(e,r){
		if(e){
			console.log(e)
		}else{
			console.log(r)
		}
	})*/
	
	connection.query('SELECT nom_jeu FROM jeu ORDER BY  nom_jeu;',function(err,results){
		if(err){
			console.log('erreur de connexion : '+err.stack);
			var parametresAccueil = {
				url_defaut:'',
				nomsJeux:[]
			};
			res.render('accueil.ejs',parametresAccueil);
		}else{
			nomsJeux=[];
			for(var i =0;i<results.rows.length;i++){
				nomsJeux.push(results.rows[i].nom_jeu);
			}
			var parametresAccueil = {
				url_defaut:'',
				nomsJeux:nomsJeux
			};
			res.render('accueil.ejs',parametresAccueil);
		}
	});
})

.post('/accueil/',urlencodedParser,function(req,res){
	//on vérifie l'entrée
	
	//on essaie de se connecter à la bdd
	
	//on regarde si le nom du jeux et le nom de la page sont dans la bdd
	
	var valide_url = (req.body.url && typeof req.body.url === 'string' && pattUrl.test(req.body.url.substring(0,50)));
	var valide_jeu = req.body.jeu && typeof req.body.jeu ==='string' ;
	var valide_hasard = (req.body.hasard && req.body.hasard === 'on') && valide_jeu;
	
	
	if(valide_url && !valide_hasard){
		req.body.url = req.body.url.substring(0,50);
	}
	
	if(valide_hasard || valide_url){
		async.waterfall([
			function(callback){
				var jeu_ok = true;
				var page_dontexist = true;
				if(valide_hasard){
					var query ='SELECT nom_page FROM page;';
				}else{
					var query = {
						text:'SELECT nom_page FROM page WHERE nom_page = $1;',
						values:[req.body.url]
					};
				}
				connection.query(query,function(err,results){
					if(err){
						callback(err);
					}else{
						if(valide_hasard){
							page_dontexist=false;
							while(!page_dontexist){
								page_dontexist=true;
								req.body.url = randomString(20,'aA');
								var i = 0;
								while((page_dontexist) && i < results.length){
									if(req.body.url === results.rows[i].nom_page){
										page_dontexist=false;
									}
									i+=1;
								}
							}
						}else{
							if(results.rows.length>0){
								page_dontexist=false;
							}
						}
					}
					callback(null,jeu_ok,page_dontexist);
				});
			},
			
			function(jeu_ok,page_dontexist,callback){
				if(page_dontexist && valide_jeu){
					const query = {
						text:'SELECT nom_jeu FROM jeu WHERE nom_jeu = $1;',
						values:[req.body.jeu]
					};
					connection.query(query,function(err,results){
						if(err){
							callback(err);
						}else{
							if(results.rows.length===0){
								jeu_ok=false;
							}
							callback(null,jeu_ok,page_dontexist);
						}
					});
				}else if(!page_dontexist){
					res.redirect('/p/'+req.body.url);
					callback(false);
				}else{
					//texte à afficher sur la page: jeu pas valide
					//res.redirect('/accueil');
					callback({stack:'jeu non valide'});
				}
			},
			
			function(jeu_ok,page_dontexist,callback){
				//si ok:
				if(jeu_ok){
					//on crée une page
					async.waterfall([
						function(callback2){
							var id_page=0;
							var id_jeu=0;
							connection.query('SELECT MAX(id_page)+1 AS id FROM page;',function(err,results){
								if(err){
									//console.log('ERROR :'+err.stack);//ATT
									//err.message=err.stack
									callback2(err);
								}else{
									if(results.rows.length===0){
										id_page=1;
									}else{
										id_page=results.rows[0].id||1;//si null->id=1
									}
									callback2(null,id_page,id_jeu);
								}
							});
						},
						
						function(id_page,id_jeu,callback2){
							const query = {
								text:'SELECT id_jeu AS id FROM jeu WHERE nom_jeu = $1;',
								values:[req.body.jeu]
							};
							connection.query(query,function(err,results){
								if(err){
									//console.log('ERROR :'+err.stack);//ATT
									//err.message=err.stack
									callback2(err);
								}else{
									id_jeu=results.rows[0].id;
									callback2(null,id_page,id_jeu);
								}
							});
						},
						
						//On démarre une transaction, pour que tout soit ajouté d'un coup ou annulé ensemble
						
						function(id_page,id_jeu,callback2){
							//https://node-postgres.com/features/transactions
							connection.connect((err, client, done) => {
								
								//gere l'erreur et le rollback
								const shouldAbort = err => {
									if (err) {
										console.error('Error in transaction', err.stack)
										client.query('ROLLBACK', err => {
											// release the client back to the pool
											done();
											if (err) {
												console.error('Error rolling back client', err.stack)
												callback2(err);
											}
											
										})
									}
									return !!err
								}
								  
								//les instructions: on démarre l transaction
								  
								client.query('BEGIN', err => {
									if (shouldAbort(err)) return
									//un waterfall pour faire toutes les requêtes
									async.waterfall([
										function(callback3){
											const query={
												text:'INSERT INTO page (id_page,nom_page,id_jeu_depart,nombre_trashs,creating_date,noeud_courant) SELECT $1,$2,id_jeu,nombre_trashs,CURRENT_TIMESTAMP,1 FROM jeu WHERE id_jeu = $3; ',
												values:[id_page,req.body.url,id_jeu]
											};
											client.query(query,function(err,results){
												if(err){
													callback3(err);
												}else{
													//resTot.push(results);
													resTot=[results.rows];
													data={id_page:id_page,id_jeu:id_jeu};
													callback3(null,data,resTot);
												}
											});
										},
										function(data,resTot,callback3){
											const query={
												text:'INSERT INTO joueur_on_page (id_page,id_joueur,label_joueur,action_clic_onboard,action_scroll_onboard,clickable_board,click_board_create_type,click_board_move_type,click_board_move_bac) SELECT $1,'+'id_joueur,label_joueur,action_clic_onboard,action_scroll_onboard,clickable_board,click_board_create_type,click_board_move_type,click_board_move_bac FROM joueur_on_jeu WHERE id_jeu = $2; ',
												values:[data.id_page,data.id_jeu]
											};
											queryChainedClient(client,query,callback3,data,resTot);
										},
										function(data,resTot,callback3){
											const query={
												text:'INSERT INTO save_on_page (id_page,noeud_precedent,noeud_suivant,date,type_action,auteur,couleur,id_joueur) SELECT $1,0,1,CURRENT_TIMESTAMP,\'create\',NULL,NULL,1;',
												values:[data.id_page]
											};
											queryChainedClient(client,query,callback3,data,resTot);
										},
										

										function(data,resTot,callback3){
											const query={
												text:'INSERT INTO box_on_page (id_page,diese_box_on_page,num_box_on_page,id_pion,noeud_start) SELECT $1,diese_box_on_jeu,num_box_on_jeu,id_pion,1 FROM box_on_jeu WHERE  id_jeu = $2; ',
												values:[data.id_page,data.id_jeu]
											};
											queryChainedClient(client,query,callback3,data,resTot);
										},
										function(data,resTot,callback3){
											const query={
												text:'INSERT INTO stop_box_on_page (id_page,num_box_on_page,noeud_stop) SELECT $1,num_box_on_jeu,0 FROM box_on_jeu WHERE  id_jeu = $2; ',
												values:[data.id_page,data.id_jeu]
											};
											queryChainedClient(client,query,callback3,data,resTot);
										},
										function(data,resTot,callback3){
											const query={
												text:'INSERT INTO bac_on_page (id_page,diese_bac_on_page,num_bac_on_page,label_bac,noeud_start) SELECT $1,diese_bac_on_jeu,num_bac_on_jeu,label_bac,1 FROM bac_on_jeu WHERE id_jeu = $2; ',
												values:[data.id_page,data.id_jeu]
											};
											queryChainedClient(client,query,callback3,data,resTot);
										},
										function(data,resTot,callback3){
											const query={
												text:'INSERT INTO stop_bac_on_page (id_page,num_bac_on_page,noeud_stop) SELECT $1,num_bac_on_jeu,0 FROM bac_on_jeu WHERE id_jeu = $2; ',
												values:[data.id_page,data.id_jeu]
											};
											queryChainedClient(client,query,callback3,data,resTot);
										},
										function(data,resTot,callback3){
											const query={
												text:'INSERT INTO board_on_page (id_page,diese_board_on_page,num_board_on_page,id_board,noeud_start) SELECT $1,diese_board_on_jeu,num_board_on_jeu,id_board,1 FROM board_on_jeu WHERE id_jeu = $2; ',
												values:[data.id_page,data.id_jeu]
											};
											queryChainedClient(client,query,callback3,data,resTot);
										},
										function(data,resTot,callback3){
											const query={
												text:'INSERT INTO stop_board_on_page (id_page,num_board_on_page,noeud_stop) SELECT $1,num_board_on_jeu,0 FROM board_on_jeu WHERE id_jeu = $2; ',
												values:[data.id_page,data.id_jeu]
											};
											queryChainedClient(client,query,callback3,data,resTot);
										},
										function(data,resTot,callback3){
											const query={
												text:'INSERT INTO pion_on_bac_on_page (id_page,diese_bac_on_page,id_pion,nombre_pions,diese_pion_on_bac_on_page,num_pion_on_bac_on_page,noeud_start) SELECT $1,'+'diese_bac_on_jeu,id_pion,nombre_pions,diese_pion_on_bac_on_jeu,num_pion_on_bac_on_jeu,1 FROM pion_on_bac_on_jeu WHERE id_jeu = $2; ',
												values:[data.id_page,data.id_jeu]
											};
											queryChainedClient(client,query,callback3,data,resTot);
										},
										function(data,resTot,callback3){
											const query={
												text:'INSERT INTO stop_pion_on_bac_on_page (id_page,diese_bac_on_page,num_pion_on_bac_on_page,noeud_stop) SELECT $1,diese_bac_on_jeu,num_pion_on_bac_on_jeu,0 FROM pion_on_bac_on_jeu WHERE id_jeu = $2; ',
												values:[data.id_page,data.id_jeu]
											};
											queryChainedClient(client,query,callback3,data,resTot);
										},
										function(data,resTot,callback3){
											const query={
												text:'INSERT INTO pion_on_board_on_page (id_page,diese_board_on_page,id_pion,position_x,position_y,taille,diese_pion_on_board_on_page,num_pion_on_board_on_page,noeud_start) SELECT $1,'+'diese_board_on_jeu,id_pion,position_x,position_y,taille,diese_pion_on_board_on_jeu,num_pion_on_board_on_jeu,1 FROM pion_on_board_on_jeu WHERE id_jeu = $2; ',
												values:[data.id_page,data.id_jeu]
											};
											queryChainedClient(client,query,callback3,data,resTot);
										},
										function(data,resTot,callback3){
											const query={
												text:'INSERT INTO stop_pion_on_board_on_page (id_page,diese_board_on_page,num_pion_on_board_on_page,noeud_stop) SELECT $1,'+'diese_board_on_jeu,num_pion_on_board_on_jeu,0 FROM pion_on_board_on_jeu WHERE id_jeu = $2; ',
												values:[data.id_page,data.id_jeu]
											};
											queryChainedClient(client,query,callback3,data,resTot);
										},
										function(data,resTot,callback3){
											const query={
												text:'INSERT INTO carac_pions_page (id_page,id_pion,change_pion,diese_bac_on_page,taille) SELECT $1,'+'id_pion,change_pion,diese_bac_on_jeu,taille FROM carac_pions_jeu WHERE  id_jeu = $2; ',
												values:[data.id_page,data.id_jeu]
											};
											queryChainedClient(client,query,callback3,data,resTot);
										},
										function(data,resTot,callback3){
											const query={
												text:'INSERT INTO carac_pions_joueur_on_page (id_page,id_joueur,id_pion,change_pion,diese_bac_on_page,taille) SELECT $1,id_joueur,id_pion,change_pion,diese_bac_on_jeu,taille FROM carac_pions_joueur_on_jeu WHERE id_jeu = $2;',
												values:[data.id_page,data.id_jeu]
											};
											client.query(query,function(err,results){
												if(err){
													callback3(err);
												}else{
													//resTot.push(results.rows);
													resTot.push(results.rows);
													callback3(null,resTot);
												}
											});
										}
										
									],function(err){
										if(shouldAbort(err)){
											return false;
										}else{
											client.query('COMMIT', err => {
												if (err) {
													console.error('Error committing transaction', err.stack);
													callback2(err);
												}else{
													res.redirect('/p/'+req.body.url);
													callback2(null);
												}
												done();
											});
										}
									});
								});
							});
						}
					],
						function(err) {
							if(err){
								console.log('erreur dans la création de la page : ' + err.stack);
								//res.redirect('/accueil');
								callback(err);
							}else{
								callback(null);
							}
						}
					);

				}else{
					//texte à afficher sur la page : jeu n'existe pas
					//res.redirect('/accueil');
					callback({stack:'le jeu n existe pas'});
				}
			}
			
		],
			function(err) {
				if(err){
					console.log('erreur lors de la demande de connection: ' + err.stack);
					res.redirect('/accueil');
				}
			}
		);
	}else{
		res.redirect('/accueil');
		//message : pas bon formulaire (nom url ou check)
	}
})

.get('/p/:pageid/',function(req,res){
	//vérifier id -> necesaire ? remplacer " " par _ ? 
	if(!pattUrl.test(req.params.pageid)){
		res.writeHead(403,{'Content-Type': 'text/plain'});
		res.write('Forbidden characters in URL');
		res.end();
	}else{
		
		connection.connect((err, client, done) => {		
			//gere l'erreur et le rollback
			const shouldAbort = err => {
				if (err) {
					client.query('ROLLBACK', err => {
						if (err) {
							console.log('erreur de suppression de la page : '+err.stack);
						}
						// release the client back to the pool
						done();
					})
				}
				return !!err
			}
			
			async.waterfall([
			
				function(callback){
					client.query('BEGIN',function(err){
						if(err){
							callback(err);
						}else{
							callback(null);
						}
					});
				},
				
				function(callback){
					const query={
						text:'SELECT id_page,noeud_courant FROM page WHERE nom_page =$1;',
						values:[req.params.pageid]
					};
					client.query(query,function(err,results_1){
						if(err){
							//console.log('erreur sql :'+err.stack);
							//res.redirect('/accueil');//passer le nom en paramètre?
							callback(err);
						}else{
							if(results_1.rows.length>1){
								//console.log('plusieurs pages');
								//res.redirect('/accueil');//supprimer les pages ?
								callback('plusieurs pages');
							}else if(results_1.rows.length==0){	
								//res.redirect('/accueil');//passer le nom en paramètre?					
								callback('page absente');
							}else{
								//renvoyer la page à partir de la bdd		
								//NoeudsBDD(results,errFun=function(res){res.redirect('/accueil')},dataErrFun=res,
								var id_page = results_1.rows[0].id_page;
								callback(null,id_page);
							}
						}
					});
				},
				
				function(id_page,callback){
					const query={
						text:'SELECT nombre_trashs,noeud_courant,creating_date FROM page WHERE id_page =$1; ',
						values:[id_page]
					};
					client.query(query,function(err,results){
						if(err){
							callback(err);
						}else{
							//resTot.push(results.rows);
							results_2=[results.rows];
							callback(null,id_page,results_2);
						}
					});
				},
				
				function(id_page,results_2,callback){
					const query={
						text:'SELECT fa.label_fonction AS action_clic_onboard, '+
							'fb.label_fonction AS action_scroll_onboard, '+
							'fc.label_fonction AS clickable_board, '+
							'pa.nom_pion AS click_board_create_type, '+
							'pb.nom_pion AS click_board_move_type, '+
							'click_board_move_bac FROM joueur_on_page '+
							'JOIN bdd_fonctions fa ON action_clic_onboard = fa.id_fonction '+
							'JOIN bdd_fonctions fb ON action_scroll_onboard = fb.id_fonction '+
							'JOIN bdd_fonctions fc ON clickable_board = fc.id_fonction '+
							'JOIN pion pa ON click_board_create_type = pa.id_pion '+
							'JOIN pion pb ON click_board_move_type = pb.id_pion '+
							'WHERE id_joueur = 1 AND id_page =$1; ',
						values:[id_page]
					};
					queryChainedClient(client,query,callback,id_page,results_2);
				},					
				function(id_page,results_2,callback){
					const query={
						text:'SELECT pa.nom_pion AS id_pion, pb.nom_pion AS change_pion, pa.svg_pion AS svg_pion, '+
							'c.diese_bac_on_page,c.taille FROM carac_pions_joueur_on_page c '+
							'JOIN pion pa ON c.id_pion = pa.id_pion '+
							'JOIN pion pb ON c.change_pion = pb.id_pion '+
							'WHERE c.id_joueur = 1 AND c.id_page =$1; ',
						values:[id_page]
					};
					queryChainedClient(client,query,callback,id_page,results_2);
				},					
				function(id_page,results_2,callback){
					const query={
						text:'SELECT p.svg_pion,p.nom_pion,b.diese_box_on_page,p.directed,b.num_box_on_page,b.noeud_start,s.nstop noeud_stop FROM box_on_page b'+
							' JOIN pion p ON b.id_pion = p.id_pion'+
							' JOIN (SELECT ARRAY_AGG(noeud_stop) nstop,MAX(num_box_on_page) num,MAX(id_page) id FROM stop_box_on_page WHERE id_page =$1 GROUP BY num_box_on_page) AS s'+
							' ON b.id_page=s.id AND b.num_box_on_page=s.num'+
							' WHERE b.id_page =$1 ORDER BY b.diese_box_on_page; ',
						values:[id_page]
					};
					queryChainedClient(client,query,callback,id_page,results_2);
				},					
				function(id_page,results_2,callback){
					const query={
						text:'SELECT diese_bac_on_page,num_bac_on_page,label_bac,noeud_start,s.nstop noeud_stop FROM bac_on_page'+
							' JOIN (SELECT ARRAY_AGG(noeud_stop) nstop,MAX(num_bac_on_page) num,MAX(id_page) id FROM stop_bac_on_page WHERE id_page =$1 GROUP BY num_bac_on_page) AS s'+
							' ON id_page=s.id AND num_bac_on_page=s.num'+
							' WHERE id_page =$1 ORDER BY diese_bac_on_page;',
						values:[id_page]
					};
					queryChainedClient(client,query,callback,id_page,results_2);
				},					
				function(id_page,results_2,callback){
					const query={
						text:'SELECT pp.diese_bac_on_page,p.svg_pion,p.nom_pion,pp.nombre_pions,pp.diese_pion_on_bac_on_page,p.directed,pp.num_pion_on_bac_on_page,pp.noeud_start,s.nstop noeud_stop FROM pion_on_bac_on_page pp'+
							' JOIN pion p ON pp.id_pion = p.id_pion'+
							' JOIN (SELECT ARRAY_AGG(noeud_stop) nstop,MAX(num_pion_on_bac_on_page) num,MAX(diese_bac_on_page) diese,MAX(id_page) id FROM stop_pion_on_bac_on_page WHERE id_page =$1 GROUP BY num_pion_on_bac_on_page) AS s'+
							' ON pp.id_page=s.id AND pp.num_pion_on_bac_on_page=s.num AND pp.diese_bac_on_page=s.diese'+
							' WHERE pp.id_page = $1 ORDER BY pp.num_pion_on_bac_on_page; ',
						values:[id_page]
					};
					queryChainedClient(client,query,callback,id_page,results_2);
				},					
				function(id_page,results_2,callback){
					const query={
						text:'SELECT bp.num_board_on_page,bp.diese_board_on_page,b.svg_board,b.nom_board,bp.noeud_start,s.nstop noeud_stop FROM board_on_page bp'+
							' JOIN board b ON bp.id_board = b.id_board'+
							' JOIN (SELECT ARRAY_AGG(noeud_stop) nstop,MAX(num_board_on_page) num,MAX(id_page) id FROM stop_board_on_page WHERE id_page =$1 GROUP BY num_board_on_page) AS s'+
							' ON bp.id_page=s.id AND bp.num_board_on_page=s.num'+
							' WHERE bp.id_page = $1 ORDER BY diese_board_on_page; ',
						values:[id_page]
					};
					queryChainedClient(client,query,callback,id_page,results_2);
				},					
				function(id_page,results_2,callback){
					const query={
						text:'SELECT pp.diese_board_on_page,pp.diese_pion_on_board_on_page,pp.num_pion_on_board_on_page,p.nom_pion,p.svg_pion,pp.position_x,pp.position_y,pp.taille,p.directed,pp.noeud_start,s.nstop noeud_stop FROM pion_on_board_on_page pp'+
							' JOIN pion p ON pp.id_pion = p.id_pion'+
							' JOIN (SELECT ARRAY_AGG(noeud_stop) nstop,MAX(num_pion_on_board_on_page) num,MAX(diese_board_on_page) diese,MAX(id_page) id FROM stop_pion_on_board_on_page WHERE id_page = $1 GROUP BY num_pion_on_board_on_page) AS s'+
							' ON pp.id_page=s.id AND pp.num_pion_on_board_on_page=s.num AND pp.diese_board_on_page=s.diese'+
							' WHERE pp.id_page =$1 ORDER BY num_pion_on_board_on_page;',
						values:[id_page]
					};
					queryChainedClient(client,query,callback,id_page,results_2);
				},					
				function(id_page,results_2,callback){
					const query={
						text:'SELECT id_joueur,label_joueur FROM joueur_on_jeu j JOIN page p ON j.id_jeu=p.id_jeu_depart WHERE id_page =$1 ORDER BY label_joueur; ',
						values:[id_page]
					};
					queryChainedClient(client,query,callback,id_page,results_2);
				},					
				function(id_page,results_2,callback){
					const query={
						text:'SELECT DISTINCT id_board,nom_board FROM board ORDER BY nom_board; ',
						values:[]
					};
					queryChainedClient(client,query,callback,id_page,results_2);
				},					
				function(id_page,results_2,callback){
					const query={
						text:'SELECT DISTINCT id_pion,nom_pion FROM pion ORDER BY nom_pion; ',
						values:[]
					};
					queryChainedClient(client,query,callback,id_page,results_2);
				},					
				function(id_page,results_2,callback){
					const query={
						text:'SELECT time,message,auteur,couleur,id_message FROM chat_on_page WHERE id_page = $1 ORDER BY time DESC; ',
						values:[id_page]
					};
					queryChainedClient(client,query,callback,id_page,results_2);
				},					
				function(id_page,results_2,callback){
					const query={
						text:'SELECT j.wiki AS wiki FROM page p JOIN jeu j ON p.id_jeu_depart=j.id_jeu;',
						values:[]
					};
					queryChainedClient(client,query,callback,id_page,results_2);
				},					
				function(id_page,results_2,callback){
					const query={
						text:'SELECT s.noeud_precedent, s.noeud_suivant, s.date, s.auteur, s.couleur, j.label_joueur FROM save_on_page s JOIN page p ON s.id_page=p.id_page JOIN joueur_on_jeu j ON s.id_joueur = j.id_joueur AND p.id_jeu_depart=j.id_jeu WHERE s.id_page =$1;',
						values:[id_page]
					};
					client.query(query,function(err,results){
						if(err){
							err.message=err.stack;
							callback(err);
						}else{
							results_2.push(results.rows);
							callback(null,results_2);
						}
					});	
				},
				function(results_2,callback){
					client.query('COMMIT',function(err,results){
						if(err){
							err.message=err.stack;
							callback(err);
						}else{
							callback(null,results_2);
						}
					});
				}
			],
			function(err,results_2){
				if(shouldAbort(err)){
					console.log('Erreur de chargement de la page depuis la BDD : '+err.stack);
					res.redirect('/accueil');//passer le nom en paramètre?
				}else{
					done();
					var noeuds=[results_2[0][0].noeud_courant];
					var noeuds_suivants=[];
					var noeuds_precedents=[];
					for(var i=0; i< results_2[13].length;i++){
						noeuds_suivants.push(results_2[13][i].noeud_suivant)
						noeuds_precedents.push(results_2[13][i].noeud_precedent)
					}
					var new_noeud=0;
					while((noeuds.indexOf(0)==-1) && (noeuds_suivants.indexOf(noeuds[noeuds.length-1])!=-1)){
						new_noeud=noeuds_precedents[noeuds_suivants.indexOf(noeuds[noeuds.length-1])];
						noeuds.push(new_noeud);
					}
					if(noeuds[noeuds.length-1]==0){
						noeuds.pop();
					}
												
					var params ={
						objBoxes : [],
						objBoards : [],
						objBacs : [],
						NTrashs : 0,
						action_clic_onboardStr : '',
						action_scroll_onboardStr : '',
						clickable_boardStr : '',
						click_board_create_type : 0,
						click_board_move_type : 0,
						click_board_move_bac : '',
						tabPionListe : [],
						tabPionChange : [],
						tabPionMove : [],
						tabPionTaille : [],
						tabPionSvg : [],
						nomPlatoo : req.params.pageid,
						Joueurs : [],
						Boxes :[],
						Boards : [],
						Chat : [],
						Save : [],
						wiki:''//,
						// maxDiesePion : 0,
						// maxDieseBoard : 0,
						// maxDieseBac : 0,
						// maxDieseGroupe : 0,
						// maxDieseBox : 0
					}
													
					params.NTrashs = results_2[0][0].nombre_trashs;
					params.noeud_courant = results_2[0][0].noeud_courant;
					
					params.action_clic_onboardStr = results_2[1][0].action_clic_onboard;
					params.action_scroll_onboardStr = results_2[1][0].action_scroll_onboard;
					params.clickable_boardStr = results_2[1][0].clickable_board;
					params.click_board_create_type = results_2[1][0].click_board_create_type;
					if(params.click_board_create_type === null){
						params.click_board_create_type=-1;
					}
					params.click_board_move_type = results_2[1][0].click_board_move_type;
					if(params.click_board_move_type === null){
						params.click_board_move_type=-1;
					}
					params.click_board_move_bac = results_2[1][0].click_board_move_bac;

					if(params.click_board_move_bac === null){
						params.click_board_move_bac=-1;
					}
					
					params.tabPionListe = [];
					params.tabPionChange = [];
					params.tabPionMove = [];
					params.tabPionTaille = [];
					params.tabPionSvg = [];
					
					for(var i = 0; i < results_2[2].length; i++){
						params.tabPionListe.push(results_2[2][i].id_pion);
						if(results_2[2][i].change_pion === null){
							results_2[2][i].change_pion=-1;
						}
						params.tabPionChange.push(results_2[2][i].change_pion);
						if(results_2[2][i].diese_bac_on_page === null){
							results_2[2][i].diese_bac_on_page=-1;
						}
						params.tabPionMove.push(results_2[2][i].diese_bac_on_page);
						params.tabPionTaille.push(results_2[2][i].taille);
						params.tabPionSvg.push(results_2[2][i].svg_pion);
					}
					
					params.objBoxes = [];
					var display ='';
					for(var i = 0; i<results_2[3].length;i++){
						/*var noeuds_stop = results_2[3][i].noeud_stop.split(',').map(function (x) { 
							return parseInt(x, 10); 
						});*/
						var noeuds_stop = results_2[3][i].noeud_stop

						//if( (noeuds.indexOf(results_2[3][i].noeud_start)!= -1) && (noeuds.indexOf(results_2[3][i].noeud_stop)==-1)) {
						if( (noeuds.indexOf(results_2[3][i].noeud_start)!= -1) && (intersect(noeuds,noeuds_stop).length===0) ){
							display='actif';
						}else{
							display='inactif';
						}
						
						
						params.objBoxes.push({
							nom : results_2[3][i].nom_pion,
							svg : results_2[3][i].svg_pion,
							diese : results_2[3][i].diese_box_on_page,
							num : results_2[3][i].num_box_on_page,
							directed : results_2[3][i].directed,
							noeud_start : results_2[3][i].noeud_start,
							noeud_stop : results_2[3][i].noeud_stop,
							display : display
						});
						// if(results_2[3][i].diese_box_on_page>params.maxDieseBox){
							// params.maxDieseBox = results_2[3][i].diese_box_on_page;
						// }
					}
					
					params.objBacs = [];
					var i=0;
					var c=0;
					while(i<results_2[4].length){
						var diese=results_2[4][i].diese_bac_on_page;
						params.objBacs.push({
							bacs :[],
							groupes : [],
							diese : results_2[4][i].diese_bac_on_page
						});
						while(i<results_2[4].length && diese===results_2[4][i].diese_bac_on_page){
							/*var noeuds_stop = results_2[4][i].noeud_stop.split(',').map(function (x) { 
								return parseInt(x, 10); 
							});*/
							var noeuds_stop = results_2[4][i].noeud_stop
							if( (noeuds.indexOf(results_2[4][i].noeud_start)!= -1) && (intersect(noeuds,noeuds_stop).length===0) ){
								display='actif';
							}else{
								display='inactif';
							}
							params.objBacs[c].bacs.push({
								label : results_2[4][i].label_bac,
								num : results_2[4][i].num_bac_on_page,
								noeud_start : results_2[4][i].noeud_start,
								noeud_stop : results_2[4][i].noeud_stop,
								display : display
								
							});
							i=i+1
						}
						c=c+1;								
					}
					
					
					
					var j = 0;
					for(var i = 0;i<results_2[5].length;i++){
						var n=0;
						var cont=true;
						while(params.objBacs[j].diese != results_2[5][i].diese_bac_on_page || cont){
							j++;
							if(j>=params.objBacs.length){
								n++;
								j = 0;
								if(n==2){
									cont=false;
								}
							}
						}
						
						/*var noeuds_stop = results_2[5][i].noeud_stop.split(',').map(function (x) { 
							return parseInt(x, 10); 
						});*/
						var noeuds_stop = results_2[5][i].noeud_stop;

						if( (noeuds.indexOf(results_2[5][i].noeud_start)!= -1) && (intersect(noeuds,noeuds_stop).length===0) ){
							display='actif';
						}else{
							display='inactif';
						}
						params.objBacs[j].groupes.push({
							nom : results_2[5][i].nom_pion,
							svg : results_2[5][i].svg_pion,
							N : results_2[5][i].nombre_pions,
							diese : results_2[5][i].diese_pion_on_bac_on_page,
							num : results_2[5][i].num_pion_on_bac_on_page,
							directed : results_2[5][i].directed,
							noeud_start : results_2[5][i].noeud_start,
							noeud_stop : results_2[5][i].noeud_stop,
							display : display
						});
						// if(results_2[5][i].diese_pion_on_bac_on_page>params.maxDieseGroupe){
							// params.maxDieseGroupe = results_2[5][i].diese_pion_on_bac_on_page;
						// }

					}
					
					params.objBoards = [];
					var i=0;
					var c=0;
					while(i<results_2[6].length){
						var diese=results_2[6][i].diese_board_on_page;
						params.objBoards.push({
							boards :[],
							pions : [],
							diese : results_2[6][i].diese_board_on_page
						});


						while(i<results_2[6].length && diese===results_2[6][i].diese_board_on_page){
							/*var noeuds_stop = results_2[6][i].noeud_stop.split(',').map(function (x) { 
								return parseInt(x, 10); 
							});*/
							var noeuds_stop = results_2[6][i].noeud_stop;
							if( (noeuds.indexOf(results_2[6][i].noeud_start)!= -1) && (intersect(noeuds,noeuds_stop).length===0) ){
								display='actif';
							}else{
								display='inactif';
							}
							params.objBoards[c].boards.push({
								svg : results_2[6][i].svg_board,
								nom : results_2[6][i].nom_board,
								num : results_2[6][i].num_board_on_page,
								noeud_start : results_2[6][i].noeud_start,
								noeud_stop : results_2[6][i].noeud_stop,
								display : display
								
							});
							i=i+1
						}
						c=c+1;								
					}
						
					var j = 0;
					for(var i = 0;i<results_2[7].length;i++){
						var n=0;
						var cont=true;
						while(params.objBoards[j].diese != results_2[7][i].diese_board_on_page || cont){
							j++;
							if(j>=params.objBoards.length){
								n++;
								j = 0;
								if(n==2){
									cont=false;
								}
							}
						}
						/*var noeuds_stop = results_2[7][i].noeud_stop.split(',').map(function (x) { 
							return parseInt(x, 10); 
						});*/
						var noeuds_stop = results_2[7][i].noeud_stop;
						if( (noeuds.indexOf(results_2[7][i].noeud_start)!= -1) && (intersect(noeuds,noeuds_stop).length===0) ){
							display='actif';
						}else{
							display='inactif';
						}
						params.objBoards[j].pions.push({
							nom : results_2[7][i].nom_pion,
							svg : results_2[7][i].svg_pion,
							x : results_2[7][i].position_x,
							y : results_2[7][i].position_y,
							height: results_2[7][i].taille,
							diese : results_2[7][i].diese_pion_on_board_on_page,
							num : results_2[7][i].num_pion_on_board_on_page,
							directed : results_2[7][i].directed,
							noeud_start : results_2[7][i].noeud_start,
							noeud_stop : results_2[7][i].noeud_stop,
							display : display
						});
					}
					
					for(var i=0;i<results_2[8].length;i++){
							params.Joueurs.push({
							id : results_2[8][i].id_joueur,
							nom : results_2[8][i].label_joueur
						});
					}
					
					for(var i=0;i<results_2[9].length;i++){
						params.Boards.push({
							id : results_2[9][i].id_board,
							nom : results_2[9][i].nom_board
						});
					}
					
					for(var i=0;i<results_2[10].length;i++){
						params.Boxes.push({
							id : results_2[10][i].id_pion,
							nom : results_2[10][i].nom_pion
						});
					}
					
					for(var i=0;i<results_2[11].length;i++){
						params.Chat.push({
							id : results_2[11][i].id_message,
							auteur : results_2[11][i].auteur,
							message : results_2[11][i].message,
							couleur : results_2[11][i].couleur,
							time : affTime.exec(results_2[11][i].time)
						});
					}
					params.wiki = results_2[12][0].wiki;
					
					params.Save = [{
						noeud_precedent : 0,
						noeud_suivant : 1,
						date : results_2[0][0].creating_date,
						auteur : "",
						couleur : "black",
						label_joueur : "",
						x:0,
						y:0
					}];
					var noeuds_sui=[1];
					var noeuds_pre=[0];
					var y=[0];
					var epaisseur=[1];
					//on a déjà fait le premier noeud
					for(var i = 1; i<results_2[13].length;i++){
						y_pre_ind=params.Save[noeuds_sui.indexOf(results_2[13][i].noeud_precedent)].y;
						epais=epaisseur[noeuds_sui.indexOf(results_2[13][i].noeud_precedent)];
						
						//Save.y au debut indique l'indice dans le vecteur y
						if(noeuds_pre.indexOf(results_2[13][i].noeud_precedent)===-1){
							//il n'y a pas de nouveau embranchement
							new_y_ind=y_pre_ind;
						}else{
							//nouvel embranchement

							//la valeur du nouveau y
							new_y = y[y_pre_ind] + epais;
							
							//on corrige le vecteur y
							for(var j=0;j<y.length;j++){
								if(y[j]>=new_y){
									y[j]=y[j]+1;
								}
							}
							
							//on ajoute le nouveau y
							y.push(new_y)
							
							//nouvel indice
							new_y_ind=y.length-1;
							
							//on corrige l'epaisseur
							var noe = results_2[13][i].noeud_precedent;
							while(noe>0){
								epaisseur[noeuds_sui.indexOf(noe)]+=1;
								noe=params.Save[noeuds_sui.indexOf(noe)].noeud_precedent;
							}
							
							epais=1;
						}
						
						params.Save.push({
							noeud_precedent : results_2[13][i].noeud_precedent,
							noeud_suivant : results_2[13][i].noeud_suivant,
							date : results_2[13][i].date,
							auteur : results_2[13][i].auteur,
							couleur : results_2[13][i].couleur,
							label_joueur : results_2[13][i].label_joueur,
							x:params.Save[noeuds_sui.indexOf(results_2[13][i].noeud_precedent)].x+1,
							y:new_y_ind,
							ypre:y_pre_ind,
							xpre:params.Save[noeuds_sui.indexOf(results_2[13][i].noeud_precedent)].x
						});
						noeuds_sui.push(results_2[13][i].noeud_suivant);
						noeuds_pre.push(results_2[13][i].noeud_precedent);
						epaisseur.push(epais);
					}
					
					params.SaveYmax=Math.max(...y);
					
					//on cherche xmax et on remplace les indices de y par les vraies valeurs
					xmax=0;
					for(var i = 0; i<params.Save.length ;i++){
						if(params.Save[i].x>xmax){
							xmax=params.Save[i].x
						}
						params.Save[i].y = y[params.Save[i].y]
						params.Save[i].ypre = y[params.Save[i].ypre]
						
					}
					
					params.SaveXmax=xmax;
					
					res.render('platoo.ejs',params);
				}
			});
		});
	}
})

.use(function(req, res, next){
    res.writeHead(404,{'Content-Type': 'text/plain'});
	res.write('Page not found');
    res.end();
});

//gérer les autres erreurs que 404
//faire une page 404


//on lance le WebSocket

io.on('connection',function(socket){
	
	//on ajoute la méthode executeSocket
	socket.executeSocket=executeSocket;
	//socket.runModifJeu=runModifJeu;
	//socket.runModifJeuWrap=runModifJeuWrap;
	
	socket.on('newplatoo',function(nomPlatoo){
		//securiser nomPlatoo
		if(typeof nomPlatoo==='string' && pattUrl.test(nomPlatoo)){
			nomPlatoo = nomPlatoo.substring(0,50);
			if(socket.nomPlatoo){
				socket.leave(socket.nomPlatoo);
			}
			socket.nomPlatoo=nomPlatoo;
			socket.join(nomPlatoo);
		}else{
			console.log("nomPlatoo incorrect");
			socket.emit('refresh');
		}

	});	
	
		
	//supprime la page
	//ok
	socket.on('delete',function(){
		socket.executeSocket(variables={},needsData=false,errMess='delete',data={},FUN=function(resIP){
			connection.connect((err, client, done) => {		
				//gere l'erreur et le rollback
				const shouldAbort = err => {
					if (err) {
						console.error('erreur de suppression de la page', err.stack);
						client.query('ROLLBACK', err => {
							if (err) {
								console.log('erreur de suppression de la page : '+err.stack);
							}
							socket.emit('refresh');
							// release the client back to the pool
							done();
						})
					}
					return !!err
				}
				  
				//les instructions: on démarre l transaction
				  
				client.query('BEGIN', err => {
					if (shouldAbort(err)) return
					
					//un waterfall pour faire toutes les requêtes
					async.waterfall([
						function(callback){
							const query={
								text:'DELETE FROM stop_pion_on_bac_on_page WHERE id_page = $1 ; ',
								values:[resIP[0].id_page]
							};
							client.query(query,function(err,results){
								if(err){
									callback(err);
								}else{
									//resTot.push(results);
									resTot=[results.rows];
									id_page=resIP[0].id_page;
									callback(null,id_page,resTot);
								}
							});
						},

						function(id_page,resTot,callback){
							const query={
								text:'DELETE FROM stop_pion_on_board_on_page WHERE id_page = $1 ; ',
								values:[id_page]
							};
							queryChainedClient(client,query,callback,id_page,resTot);
						},
						function(id_page,resTot,callback){
							const query={
								text:'DELETE FROM pion_on_bac_on_page WHERE id_page = $1 ; ',
								values:[id_page]
							};
							queryChainedClient(client,query,callback,id_page,resTot);
						},
						function(id_page,resTot,callback){
							const query={
								text:'DELETE FROM carac_pions_joueur_on_page WHERE id_page = $1 ; ',
								values:[id_page]
							};
							queryChainedClient(client,query,callback,id_page,resTot);
						},
						function(id_page,resTot,callback){
							const query={
								text:'DELETE FROM pion_on_board_on_page WHERE id_page = $1 ; ',
								values:[id_page]
							};
							queryChainedClient(client,query,callback,id_page,resTot);
						},
						function(id_page,resTot,callback){
							const query={
								text:'DELETE FROM carac_pions_page WHERE id_page = $1 ; ',
								values:[id_page]
							};
							queryChainedClient(client,query,callback,id_page,resTot);
						},
						function(id_page,resTot,callback){
							const query={
								text:'DELETE FROM stop_bac_on_page WHERE id_page = $1 ; ',
								values:[id_page]
							};
							queryChainedClient(client,query,callback,id_page,resTot);
						},
						function(id_page,resTot,callback){
							const query={
								text:'DELETE FROM bac_on_page WHERE id_page = $1 ; ',
								values:[id_page]
							};
							queryChainedClient(client,query,callback,id_page,resTot);
						},
						function(id_page,resTot,callback){
							const query={
								text:'DELETE FROM stop_box_on_page WHERE id_page = $1 ; ',
								values:[id_page]
							};
							queryChainedClient(client,query,callback,id_page,resTot);
						},
						function(id_page,resTot,callback){
							const query={
								text:'DELETE FROM box_on_page WHERE id_page = $1 ; ',
								values:[id_page]
							};
							queryChainedClient(client,query,callback,id_page,resTot);
						},
						function(id_page,resTot,callback){
							const query={
								text:'DELETE FROM stop_board_on_page WHERE id_page = $1 ; ',
								values:[id_page]
							};
							queryChainedClient(client,query,callback,id_page,resTot);
						},
						function(id_page,resTot,callback){
							const query={
								text:'DELETE FROM board_on_page WHERE id_page = $1 ; ',
								values:[id_page]
							};
							queryChainedClient(client,query,callback,id_page,resTot);
						},
						function(id_page,resTot,callback){
							const query={
								text:'DELETE FROM chat_on_page WHERE id_page = $1 ; ',
								values:[id_page]
							};
							queryChainedClient(client,query,callback,id_page,resTot);
						},
						function(id_page,resTot,callback){
							const query={
								text:'DELETE FROM save_on_page WHERE id_page = $1 ; ',
								values:[id_page]
							};
							queryChainedClient(client,query,callback,id_page,resTot);
						},
						function(id_page,resTot,callback){
							const query={
								text:'DELETE FROM joueur_on_page WHERE id_page = $1 ; ',
								values:[id_page]
							};
							queryChainedClient(client,query,callback,id_page,resTot);
						},
						function(id_page,resTot,callback){
							const query={
								text:'DELETE FROM page WHERE id_page = $1 ; ',
								values:[id_page]
							};
							
							client.query(query,function(err,results){
								if(err){
									callback(err);
								}else{
									//resTot.push(results.rows);
									resTot.push(results.rows);
									id_page=id_page;
									callback(null,resTot);
								}
							});						
						}
					],function(err,resTot){
						if(shouldAbort(err)){
							//socket.emit('refresh');
						}else{
							client.query('COMMIT', err => {
								if (err) {
									console.log('erreur de suppression de la page : '+err.stack);
									socket.emit('refresh');

								}else{
									socket.broadcast.to(socket.nomPlatoo).emit('refresh');
									socket.emit('refresh');
								}
								done();
							});
						}
					});
				});		
			});
		});
	});
	
	//remise à zéro de la page
	//ok
	socket.on('raz',function(){
		socket.executeSocket(variables={},needsData=false,errMess='RAZ',data={},FUN=function(resIP){
			connection.connect((err, client, done) => {
				//gere l'erreur et le rollback
				const shouldAbort = err => {
					if (err) {
						console.error('erreur de remise à zéro ', err.stack)
						client.query('ROLLBACK', err => {
							if (err) {
								console.log('erreur de remise à zéro : '+err.stack);
								//socket.emit('refresh');
							}
							socket.emit('refresh');
							// release the client back to the pool
							done();
						})
					}
					return !!err
				}
				  
				//les instructions: on démarre l transaction
				  
				client.query('BEGIN', err => {
					if (shouldAbort(err)) return
					
					//un waterfall pour faire toutes les requêtes
					async.waterfall([
						function(callback){
							const query={
								text:'DELETE FROM stop_pion_on_bac_on_page WHERE id_page = $1 ; ',
								values:[resIP[0].id_page]
							};
							client.query(query,function(err,results){
								if(err){
									callback(err);
								}else{
									//resTot.push(results.rows);
									resTot=[results.rows];
									data={id_page:resIP[0].id_page,id_jeu_depart:resIP[0].id_jeu_depart}
									callback(null,data,resTot);
								}
							});
						},

						function(data,resTot,callback){
							const query={
								text:'DELETE FROM pion_on_bac_on_page WHERE id_page = $1 ; ',
								values:[data.id_page]
							};
							queryChainedClient(client,query,callback,data,resTot);
						},
						function(data,resTot,callback){
							const query={
								text:'DELETE FROM stop_pion_on_board_on_page WHERE id_page = $1 ; ',
								values:[data.id_page]
							};
							queryChainedClient(client,query,callback,data,resTot);
						},
						function(data,resTot,callback){
							const query={
								text:'DELETE FROM pion_on_board_on_page WHERE id_page = $1 ; ',
								values:[data.id_page]
							};
							queryChainedClient(client,query,callback,data,resTot);
						},
						function(data,resTot,callback){
							const query={
								text:'DELETE FROM carac_pions_joueur_on_page WHERE id_page = $1 ; ',
								values:[data.id_page]
							};
							queryChainedClient(client,query,callback,data,resTot);
						},
						function(data,resTot,callback){
							const query={
								text:'DELETE FROM carac_pions_page WHERE id_page = $1 ; ',
								values:[data.id_page]
							};
							queryChainedClient(client,query,callback,data,resTot);
						},
						function(data,resTot,callback){
							const query={
								text:'DELETE FROM stop_bac_on_page WHERE id_page = $1 ; ',
								values:[data.id_page]
							};
							queryChainedClient(client,query,callback,data,resTot);
						},
						function(data,resTot,callback){
							const query={
								text:'DELETE FROM bac_on_page WHERE id_page = $1 ; ',
								values:[data.id_page]
							};
							queryChainedClient(client,query,callback,data,resTot);
						},
						function(data,resTot,callback){
							const query={
								text:'DELETE FROM stop_box_on_page WHERE id_page = $1 ; ',
								values:[data.id_page]
							};
							queryChainedClient(client,query,callback,data,resTot);
						},
						function(data,resTot,callback){
							const query={
								text:'DELETE FROM box_on_page WHERE id_page = $1 ; ',
								values:[data.id_page]
							};
							queryChainedClient(client,query,callback,data,resTot);
						},
						function(data,resTot,callback){
							const query={
								text:'DELETE FROM stop_board_on_page WHERE id_page = $1 ; ',
								values:[data.id_page]
							};
							queryChainedClient(client,query,callback,data,resTot);
						},
						function(data,resTot,callback){
							const query={
								text:'DELETE FROM board_on_page WHERE id_page = $1 ; ',
								values:[data.id_page]
							};
							queryChainedClient(client,query,callback,data,resTot);
						},					
						function(data,resTot,callback){
							const query={
								text:'DELETE FROM chat_on_page WHERE id_page = $1 ; ',
								values:[data.id_page]
							};
							queryChainedClient(client,query,callback,data,resTot);
						},
						function(data,resTot,callback){
							const query={
								text:'DELETE FROM save_on_page WHERE id_page = $1 ; ',
								values:[data.id_page]
							};
							queryChainedClient(client,query,callback,data,resTot);
						},
						function(data,resTot,callback){
							const query={
								text:'DELETE FROM joueur_on_page WHERE id_page = $1 ; ',
								values:[data.id_page]
							};
							queryChainedClient(client,query,callback,data,resTot);
						},
						function(data,resTot,callback){
							const query={
								text:'INSERT INTO joueur_on_page (id_page,id_joueur,label_joueur,action_clic_onboard,action_scroll_onboard,clickable_board,click_board_create_type,click_board_move_type,click_board_move_bac) SELECT $1,id_joueur,label_joueur,action_clic_onboard,action_scroll_onboard,clickable_board,click_board_create_type,click_board_move_type,click_board_move_bac FROM joueur_on_jeu WHERE id_jeu = $2; ',
								values:[data.id_page,data.id_jeu_depart]
							};
							queryChainedClient(client,query,callback,data,resTot);
						},
						function(data,resTot,callback){
							const query={
								text:'INSERT INTO save_on_page (id_page,noeud_precedent,noeud_suivant,date,type_action,auteur,couleur,id_joueur) SELECT $1,0,1,CURRENT_TIMESTAMP,\'create\',NULL,NULL,1;',
								values:[data.id_page]
							};
							queryChainedClient(client,query,callback,data,resTot);
						},
						function(data,resTot,callback){
							const query={
								text:'INSERT INTO box_on_page (id_page,diese_box_on_page,num_box_on_page,id_pion,noeud_start) SELECT $1,'+'diese_box_on_jeu,num_box_on_jeu,id_pion,1 FROM box_on_jeu WHERE  id_jeu = $2; ',
								values:[data.id_page,data.id_jeu_depart]
							};
							queryChainedClient(client,query,callback,data,resTot);
						},
						function(data,resTot,callback){
							const query={
								text:'INSERT INTO stop_box_on_page (id_page,num_box_on_page,noeud_stop) SELECT $1,num_box_on_jeu,0 FROM box_on_jeu WHERE  id_jeu = $2; ',
								values:[data.id_page,data.id_jeu_depart]
							};
							queryChainedClient(client,query,callback,data,resTot);
						},
						function(data,resTot,callback){
							const query={
								text:'INSERT INTO bac_on_page (id_page,diese_bac_on_page,num_bac_on_page,label_bac,noeud_start) SELECT $1,'+'diese_bac_on_jeu,num_bac_on_jeu,label_bac,1 FROM bac_on_jeu WHERE id_jeu = $2; ',
								values:[data.id_page,data.id_jeu_depart]
							};
							queryChainedClient(client,query,callback,data,resTot);
						},
						function(data,resTot,callback){
							const query={
								text:'INSERT INTO stop_bac_on_page (id_page,num_bac_on_page,noeud_stop) SELECT $1,num_bac_on_jeu,0 FROM bac_on_jeu WHERE id_jeu = $2; ',
								values:[data.id_page,data.id_jeu_depart]
							};
							queryChainedClient(client,query,callback,data,resTot);
						},
						function(data,resTot,callback){
							const query={
								text:'INSERT INTO board_on_page (id_page,diese_board_on_page,num_board_on_page,id_board,noeud_start) SELECT $1,diese_board_on_jeu,num_board_on_jeu,id_board,1 FROM board_on_jeu WHERE id_jeu = $2; ',
								values:[data.id_page,data.id_jeu_depart]
							};
							queryChainedClient(client,query,callback,data,resTot);
						},
						function(data,resTot,callback){
							const query={
								text:'INSERT INTO stop_board_on_page (id_page,num_board_on_page,noeud_stop) SELECT $1,num_board_on_jeu,0 FROM board_on_jeu WHERE id_jeu = $2; ',
								values:[data.id_page,data.id_jeu_depart]
							};
							queryChainedClient(client,query,callback,data,resTot);
						},
						function(data,resTot,callback){
							const query={
								text:'INSERT INTO pion_on_bac_on_page (id_page,diese_bac_on_page,id_pion,nombre_pions,diese_pion_on_bac_on_page,num_pion_on_bac_on_page,noeud_start) SELECT $1,'+'diese_bac_on_jeu,id_pion,nombre_pions,diese_pion_on_bac_on_jeu,num_pion_on_bac_on_jeu,1 FROM pion_on_bac_on_jeu WHERE id_jeu = $2; ',
								values:[data.id_page,data.id_jeu_depart]
							};
							queryChainedClient(client,query,callback,data,resTot);
						},
						function(data,resTot,callback){
							const query={
								text:'INSERT INTO stop_pion_on_bac_on_page (id_page,diese_bac_on_page,num_pion_on_bac_on_page,noeud_stop) SELECT $1,diese_bac_on_jeu,num_pion_on_bac_on_jeu,0 FROM pion_on_bac_on_jeu WHERE id_jeu = $2; ',
								values:[data.id_page,data.id_jeu_depart]
							};
							queryChainedClient(client,query,callback,data,resTot);
						},
						function(data,resTot,callback){
							const query={
								text:'INSERT INTO pion_on_board_on_page (id_page,diese_board_on_page,id_pion,position_x,position_y,taille,diese_pion_on_board_on_page,num_pion_on_board_on_page,noeud_start) SELECT $1,'+'diese_board_on_jeu,id_pion,position_x,position_y,taille,diese_pion_on_board_on_jeu,num_pion_on_board_on_jeu,1 FROM pion_on_board_on_jeu WHERE id_jeu = $2; ',
								values:[data.id_page,data.id_jeu_depart]
							};
							queryChainedClient(client,query,callback,data,resTot);
						},
						function(data,resTot,callback){
							const query={
								text:'INSERT INTO stop_pion_on_board_on_page (id_page,diese_board_on_page,num_pion_on_board_on_page,noeud_stop) SELECT $1,diese_board_on_jeu,num_pion_on_board_on_jeu,0 FROM pion_on_board_on_jeu WHERE id_jeu = $2; ',
								values:[data.id_page,data.id_jeu_depart]
							};
							queryChainedClient(client,query,callback,data,resTot);
						},
						function(data,resTot,callback){
							const query={
								text:'INSERT INTO carac_pions_page (id_page,id_pion,change_pion,diese_bac_on_page,taille) SELECT $1,id_pion,change_pion,diese_bac_on_jeu,taille FROM carac_pions_jeu WHERE  id_jeu = $2; ',
								values:[data.id_page,data.id_jeu_depart]
							};
							queryChainedClient(client,query,callback,data,resTot);
						},
						function(data,resTot,callback){
							const query={
								text:'INSERT INTO carac_pions_joueur_on_page (id_page,id_joueur,id_pion,change_pion,diese_bac_on_page,taille) SELECT $1,id_joueur,id_pion,change_pion,diese_bac_on_jeu,taille FROM carac_pions_joueur_on_jeu WHERE id_jeu = $2;',
								values:[data.id_page,data.id_jeu_depart]
							};
							queryChainedClient(client,query,callback,data,resTot);
						},
						
						function(id_page,resTot,callback){
							const query={
								text:'UPDATE page SET noeud_courant=1 WHERE id_page = $1; ',
								values:[data.id_page]
							};
							
							client.query(query,function(err,results){
								if(err){
									callback(err);
								}else{
									//resTot.push(results.rows);
									resTot.push(results.rows);
									id_page=id_page;
									callback(null,resTot);
								}
							});						
						}
					],function(err,resTot){
						if(shouldAbort(err)){
							//socket.emit('refresh');
						}else{
							client.query('COMMIT', err => {
								if (err) {
									console.log('erreur de remise à zéro : '+err.stack);
									socket.emit('refresh');
								}else{
									socket.broadcast.to(socket.nomPlatoo).emit('refresh');
									socket.emit('refresh');
								}
								done();
							});
						}
					});
				});		
			});			
		});
	});
	
	//ok
	socket.on('getAction',function(data){
		socket.executeSocket(variables={id_joueur:1},needsData=true,errMess='getAction',data=data,FUN=function(resIP){
			var query ={
				text: 'SELECT fa.label_fonction AS action_clic_onboard, fb.label_fonction AS action_scroll_onboard, fc.label_fonction AS clickable_board, '+
				'pa.nom_pion AS click_board_create_type, pb.nom_pion AS click_board_move_type, j.click_board_move_bac '+
				'FROM joueur_on_page j JOIN pion pa ON j.click_board_create_type=pa.id_pion JOIN pion pb ON j.click_board_move_type=pb.id_pion '+
				'JOIN bdd_fonctions fa ON j.action_clic_onboard = fa.id_fonction JOIN bdd_fonctions fb ON j.action_scroll_onboard = fb.id_fonction JOIN bdd_fonctions fc ON j.clickable_board = fc.id_fonction '+
				'WHERE id_page = $1 AND id_joueur = $2; ',
				values:[resIP[0].id_page,data.id_joueur]
			}
			connection.query(query,function(err,res2){
				if(err){
					console.log('erreur de getAction : '+err.stack);
					socket.emit('refresh');
				}else{
					var action_clic_onboard =res2.rows[0].action_clic_onboard;
					var action_scroll_onboard =res2.rows[0].action_scroll_onboard;
					var clickable_board =res2.rows[0].clickable_board;
					var click_board_create_type =res2.rows[0].click_board_create_type;
					var click_board_move_type =res2.rows[0].click_board_move_type;
					var click_board_move_bac =res2.rows[0].click_board_move_bac;
					
					socket.emit('setAction',{
						action_clic_onboard:action_clic_onboard,
						action_scroll_onboard:action_scroll_onboard,
						clickable_board:clickable_board,
						click_board_create_type:click_board_create_type,
						click_board_move_type:click_board_move_type,
						click_board_move_bac:click_board_move_bac
					});
				}
			});
		});
	});
	
	//ok
	socket.on('chatNew',function(data){
		socket.executeSocket(variables={message:'200',
								auteur:'20',
								couleur:'20'},
								needsData=true,errMess='chat_new',data=data,FUN=function(resIP){
			connection.connect((err, client, done) => {
				//gere l'erreur et le rollback
				const shouldAbort = err => {
					if (err) {
						console.log('erreur de chatNew : '+err.stack);
						socket.emit('refresh');
						client.query('ROLLBACK', err => {
							if (err) {
								console.log('erreur de chatNew : '+err.stack);
							}
							// release the client back to the pool
							done();
						})
					}
					return !!err
				}
				  
				//les instructions: on démarre l transaction
				  
				client.query('BEGIN', err => {
					if (shouldAbort(err)) return
					
					//un waterfall pour faire toutes les requêtes
					async.waterfall([
						function(callback){
							const query={
								text:'INSERT INTO chat_on_page (id_page,id_message,auteur,message,couleur,time) '+
									'SELECT $1,CASE WHEN MAX(id_message) IS NULL THEN 1 ELSE MAX(id_message)+1 END,'+
									'$2,$3,$4,CURRENT_TIMESTAMP FROM chat_on_page WHERE id_page = $5; ',
								values:[resIP[0].id_page,data.auteur,data.message,data.couleur,resIP[0].id_page]
							};
							client.query(query,function(err,results){
								if(err){
									callback(err);
								}else{
									//resTot.push(results.rows);
									res2=[results.rows];
									data={
										auteur:data.auteur,
										couleur:data.couleur,
										message:data.message,
										id_page:resIP[0].id_page
									}
									callback(null,data,res2);
								}
							});
						},

						function(id_page,res2,callback){
							const query={
								text:'SELECT id_message,time FROM chat_on_page WHERE id_page = $1 ORDER BY id_message DESC LIMIT 1 ; ',
								values:[data.id_page]
							};
							queryChainedClient(client,query,callback,data,res2);
						},

						function(id_page,res2,callback){
							const query={
								text:'DELETE FROM chat_on_page WHERE id_page = $1 AND time <'+
									'(SELECT c.time FROM (SELECT time FROM chat_on_page WHERE id_page = $2 ORDER BY time DESC LIMIT 1 OFFSET 19 ) AS c) ; ',
								values:[data.id_page,data.id_page]
							};
							
							client.query(query,function(err,results){
								if(err){
									callback(err);
								}else{
									res2.push(results.rows);
									//resTot=[results.rows];
									callback(null,res2);
								}
							});						
						},
						function(res2,callback){
							client.query('COMMIT',function(err){
								if(shouldAbort(err)) return
								callback(null,res2);
							});
						}
					],function(err,res2){
						if(shouldAbort(err)){
							//console.log('erreur cat on page');
							//socket.emit('refresh');
						}else if(res2[1].length>0){
							var time = affTime.exec(res2[1][0].time);
							socket.emit('chatNew',{
								message:data.message,
								id:res2[1][0].id_message,
								auteur:data.auteur,
								couleur:data.couleur,
								time:time
							});
							socket.broadcast.to(socket.nomPlatoo).emit('chatNew',{
								message:data.message,
								id:res2[1][0].id_message,
								auteur:data.auteur,
								couleur:data.couleur,
								time:time
							});
							done();
						}else{ 
							console.log('erreur cat on page');
							socket.emit('refresh');
							done();
						}
					});
				});		
			});		
		});
	});
		
	//ok
	socket.on('getTabPion',function(data){
		socket.executeSocket(variables={id_joueur:1},needsData=true,errMess='getTabPion',data=data,FUN=function(resIP){
			const query={
				text:'SELECT pa.nom_pion AS id_pion,pa.svg_pion AS svg_pion,pb.nom_pion AS change_pion,diese_bac_on_page,c.taille'+
				' AS taille FROM carac_pions_joueur_on_page c JOIN pion pa ON c.id_pion=pa.id_pion JOIN pion pb ON c.change_pion=pb.id_pion WHERE id_page = $1'+
				' AND id_joueur = $2;',
				values:[resIP[0].id_page,data.id_joueur]
			}
			connection.query(query,
				function(err,res2){
					if(err){
						console.log('erreur de getTabPion : '+err.stack);
						socket.emit('refresh');
					}else{
						var tabPionListe =[];
						var tabPionChange =[];
						var tabPionMove =[];
						var tabPionTaille=[];
						var tabPionSvg=[];
						for(var i=0;i<res2.rows.length;i++){
							tabPionListe.push(res2.rows[i].id_pion);
							tabPionChange.push(res2.rows[i].change_pion);
							tabPionMove.push(res2.rows[i].diese_bac_on_page);
							tabPionTaille.push(res2.rows[i].taille);
							tabPionSvg.push(res2.rows[i].svg_pion);
						}
						socket.emit('setTabPion',{tabPionListe:tabPionListe, tabPionChange:tabPionChange,tabPionMove:tabPionMove,tabPionTaille:tabPionTaille,tabPionSvg:tabPionSvg});
					}
				}
			);
		});
	});
		
	//ok
	socket.on('add_trash',function(){
		socket.executeSocket(variables={},needsData=false,errMess='add_trash',data={},FUN=function(resIP){
			const query = {
				text:'UPDATE page SET nombre_trashs = (nombre_trashs + 1) WHERE id_page = $1 ;',
				values:[resIP[0].id_page]
			}		
			connection.query(query,function(err){
				if(err){
					console.log('erreur de add_trash : '+err.stack);
					socket.emit('refresh');
				}else{
					socket.broadcast.to(socket.nomPlatoo).emit('add_trash');
				}	
			});
		});
	});
	
	//ok
	socket.on('remove_trash',function(){
		socket.executeSocket(variables={},needsData=false,errMess='remove_trash',data={},FUN=function(resIP){
			const query = {
				text:'UPDATE page SET nombre_trashs = GREATEST(nombre_trashs -1,0) WHERE id_page = $1 ; ',
				values:[resIP[0].id_page]
			}		
			connection.query(query,function(err){
				if(err){
					console.log('erreur de remove_trash : '+err.stack);
					socket.emit('refresh');
				}else{
					socket.broadcast.to(socket.nomPlatoo).emit('removeElement',{id:'trash',noeud:null});
					socket.emit('removeElement',{id:'trash',noeud:null});
				}	
			});
		});
	});
	
	//ok
	socket.on('changeNoeud',function(data){

		socket.executeSocket(variables={noeud:1},needsData=true,errMess='changeNoeud',data=data,FUN=function(resIP){
			const query ={
				text:'SELECT noeud_suivant FROM save_on_page WHERE noeud_suivant = $1 AND id_page = $2 ;',
				values:[data.noeud,resIP[0].id_page]
			}
			connection.query(query,
				function(err,res1){
					if(err){
						console.log('erreur de changeNoeud1 : '+err.stack);
						socket.emit('refresh');
					}else if(resIP.length===0){
						console.log('bad noeud de changeNoeud : '+err.stack);
						socket.emit('refresh');
					}else{
						const query ={
							text:'UPDATE page SET noeud_courant = $1 WHERE id_page = $2 ;',
							values:[data.noeud,resIP[0].id_page]
						}
						connection.query(query,
							function(err2,res2){
								if(err2){
									console.log('erreur de changeNoeud2 : '+err2.stack);
									socket.emit('refresh');
								}else{
									socket.broadcast.to(socket.nomPlatoo).emit('changeNoeud',{noeud:data.noeud});
									socket.emit('changeNoeud',{noeud:data.noeud});
								}
							}
						);
					}
				}
			);
		});
	});
	
	
	socket.on('ModifJeu',function(dataTot){
				
		socket.executeSocket(variables={auteur:'20',
										couleur:'20',
										id_joueur:1,
										noeud_courant:1},needsData=true,errMess='ModifJeu',data=dataTot,FUN=function(resIP){		
		
			//on vérifie le noeud courant
			//console.log(dataTot);
			//console.log(dataTot.noeud_courant);
			//console.log(resIP[0].noeud_courant);
			if(dataTot.noeud_courant !== resIP[0].noeud_courant){
				//si ça n'est pas le même: il y a un decalage, on ne fait rien
				console.log('mauvais noeud');
				socket.emit('refresh');
			}else{
				connection.connect((err, client, done) => {
					//gere l'erreur et le rollback
					const shouldAbort = err => {
						if (err) {
							console.log('erreur de ModifJeu1 : '+err.stack);
							
							client.query('ROLLBACK', err => {
								if (err) {
									console.log('erreur de ModifJeu2 : '+err.stack);
								}else{
									//console.log('rollbackok');
								}
								socket.emit('refresh');
								// release the client back to the pool
								done();
							})
						}
						return !!err
					}
					
					//un waterfall pour faire toutes les requêtes
					var Array_waterfall = [
						function(callback){
							//les instructions: on démarre l transaction
							client.query('BEGIN',function(err,results){
								if(err){
									callback(err);
								}else{
									callback(null);
								}
							});
						},						
						function(callback){
							const query={
								text:'SELECT noeud_precedent,noeud_suivant FROM save_on_page WHERE id_page = $1;',
								values:[resIP[0].id_page]
							};
							client.query(query,function(err,results){
								if(err){
									callback(err);
								}else{
									//resTot.push(results.rows);
									resNoeuds=[results.rows];
									data={
										auteur:dataTot.auteur,
										couleur:dataTot.couleur,
										message:dataTot.message,
										noeud_courant:dataTot.noeud_courant,
										id_joueur:dataTot.id_joueur,
										id_page:resIP[0].id_page
									}
									callback(null,data,resNoeuds);
								}
							});
						},

						function(data,resNoeuds,callback){
							const query={
								text:'INSERT INTO save_on_page (id_page,noeud_precedent,noeud_suivant,date,type_action,auteur,couleur,id_joueur) SELECT '+
									'$1,$2,CASE WHEN MAX(noeud_suivant) IS NULL THEN 1 ELSE MAX(noeud_suivant)+1 END,CURRENT_TIMESTAMP,\'add_groupe\',$3,'+
									'$4,$5 FROM save_on_page WHERE id_page=$1;',
								values:[data.id_page,data.noeud_courant,data.auteur,data.couleur,data.id_joueur]
							};
							queryChainedClient(client,query,callback,data,resNoeuds);
						},
						function(data,resNoeuds,callback){
							const query={
								text:'UPDATE page AS p SET noeud_courant = s.new_noeud'+
									' FROM (SELECT MAX(id_page) id_p,MAX(noeud_suivant) new_noeud FROM save_on_page GROUP BY id_page) AS s '+
									' WHERE p.id_page=s.id_p AND p.id_page = $1;',
								values:[data.id_page]	
							};
							queryChainedClient(client,query,callback,data,resNoeuds);
						},

						function(data,resNoeuds,callback){
							const query={
								text:'SELECT noeud_courant AS new_noeud,CURRENT_TIMESTAMP AS date FROM page WHERE id_page=$1;',
								values:[data.id_page]
							};
							
							client.query(query,function(err,results){
								if(err){
									callback(err);
								}else{
									resNoeuds.push(results.rows);
									callback(null,resNoeuds);
								}
							});						
						},
						function(resNoeuds,callback){
							emit_broad.push({text:'newNoeud',data:{noeud:resNoeuds[3][0].new_noeud,save:{
								noeud_precedent : resIP[0].noeud_courant,
								noeud_suivant : resNoeuds[3][0].new_noeud,
								date : resNoeuds[3][0].date,
								auteur : resIP[0].auteur,
								couleur : resIP[0].couleur,
								label_joueur : resIP[0].label_joueur,
								x:-1,
								y:-1,
								xpre:-1,
								ypre:-1
							}}});
							emit.push({text:'newNoeud',data:{noeud:resNoeuds[3][0].new_noeud,save:{
								noeud_precedent : resIP[0].noeud_courant,
								noeud_suivant : resNoeuds[3][0].new_noeud,
								date : resNoeuds[3][0].date,
								auteur : resIP[0].auteur,
								couleur : resIP[0].couleur,
								label_joueur : resIP[0].label_joueur,
								x:-1,
								y:-1,
								xpre:-1,
								ypre:-1

							}}});						
							//on calcule les noeuds à ajouter
							var noeuds=[resIP[0].noeud_courant];
							var noeuds_suivants=[];
							var noeuds_precedents=[];
							for(var i=0; i< resNoeuds[0].length;i++){
								noeuds_suivants.push(resNoeuds[0][i].noeud_suivant)
							}
							for(var i=0; i< resNoeuds[0].length;i++){
								noeuds_precedents.push(resNoeuds[0][i].noeud_precedent)
							}
							
							while((noeuds.indexOf(0)==-1) && (noeuds_suivants.indexOf(noeuds[noeuds.length-1])!=-1)){
								noeuds.push(noeuds_precedents[noeuds_suivants.indexOf(noeuds[noeuds.length-1])]);
							}
							//noeuds=noeuds.toString().replace(/,0/,'');
							//noeuds=noeuds.toString().replace(/^0,/,'');
							const index = noeuds.indexOf(0);
							if (index > -1) {
								noeuds.splice(index, 1);
							}
							
							/*noeudsObj = {
								noeuds:noeuds,
								toPostgres:function(value){
									return noeuds.toString()
								}
							};*/
							resNoeuds.push(noeuds);
							callback(null,resNoeuds);
						}];
					//on parcours les différentes requetes qui sont englobées dans le mouvement
					var Abort = false;
					
					var emit = [];
					var emit_broad = [];
					
					var requetes_tot= [];
					
					var nb_requete = 0;
					
					for(var i=0;i<dataTot.requetes.length;i++){
						var requete=dataTot.requetes[i];
						requetes_tot.push(requete);
						//action erase-------------------------------------------------------------------------
						if(requete.action ==='erase'){//ok
							if(!checkValide(variables={},needsData=false,errMess='erase',data=requete.data)){
								console.log(errMess+'.data incorrect dans erase');
								Abort=true;
							}else{
								//un waterfall pour faire toutes les requêtes
								Array_waterfall=Array_waterfall.concat([
								
									function(resNoeuds,callback){
										const query={
											text:'INSERT INTO stop_pion_on_bac_on_page (id_page,diese_bac_on_page,num_pion_on_bac_on_page,noeud_stop)'+
												' SELECT id_page,diese_bac_on_page,num_pion_on_bac_on_page,$1 FROM pion_on_bac_on_page p'+
												' LEFT JOIN (SELECT COUNT(noeud_stop) nbstop,MAX(id_page) id, MAX(num_pion_on_bac_on_page) num, MAX(diese_bac_on_page) diese FROM stop_pion_on_bac_on_page'+
												' WHERE id_page = $2 AND ARRAY[noeud_stop] <@ $3 GROUP BY id_page,diese_bac_on_page,num_pion_on_bac_on_page) AS g'+
												' ON p.id_page=g.id AND p.num_pion_on_bac_on_page = g.num AND p.diese_bac_on_page=g.diese'+
												' WHERE id_page = $2 AND ARRAY[noeud_start] <@ $3 AND ( COALESCE(g.nbstop,0)=0 OR g.nbstop=0) ;',
											values:[resNoeuds[3][0].new_noeud,resIP[0].id_page,resNoeuds[4]]
										};
										client.query(query,function(err,results){
											if(err){
												callback(err);
											}else{
												//resTot.push(results.rows);
												resTot=[results.rows];
												callback(null,resNoeuds,resTot);
											}
										});
									},


									function(resNoeuds,resTot,callback){
										const query={
											text:'INSERT INTO stop_pion_on_board_on_page (id_page,diese_board_on_page,num_pion_on_board_on_page,noeud_stop)'+
												' SELECT id_page,diese_board_on_page,num_pion_on_board_on_page,$1 FROM pion_on_board_on_page p'+
												' LEFT JOIN (SELECT COUNT(noeud_stop) nbstop,MAX(id_page) id, MAX(num_pion_on_board_on_page) num, MAX(diese_board_on_page) diese FROM stop_pion_on_board_on_page'+
												' WHERE id_page = $2 AND ARRAY[noeud_stop] <@ $3 GROUP BY id_page,diese_board_on_page,num_pion_on_board_on_page) AS g'+
												' ON p.id_page=g.id AND p.num_pion_on_board_on_page = g.num AND p.diese_board_on_page=g.diese'+
												' WHERE id_page = $2 AND ARRAY[noeud_start] <@ $3 AND ( COALESCE(g.nbstop,0)=0 OR g.nbstop=0) ;',
											values:[resNoeuds[3][0].new_noeud,resIP[0].id_page,resNoeuds[4]]
										};
										
										client.query(query,function(err,results){
											if(err){
												callback(err);
											}else{
												//resTot.push(results.rows);
												resTot.push(results.rows);
												callback(null,resNoeuds,resTot);
											}
										});						
									},
									function(resNoeuds,resTot,callback){
										emit.push({text:'erase',data:{noeud:resNoeuds[3][0].new_noeud}});
										emit_broad.push({text:'erase',data:{noeud:resNoeuds[3][0].new_noeud}});
										nb_requete +=1;
										callback(null,resNoeuds);
									}
								]);
							}

						//action add_bac--------------------------------------------------------------
						}else if(requete.action ==='add_bac'){//ok
							if(!checkValide(variables={label:'20'},needsData=true,errMess='erase',data=requete.data)){
								console.log(errMess+'.data incorrect dans add_bac');
								Abort = true;
							}else{		
								//un waterfall pour faire toutes les requêtes
								Array_waterfall=Array_waterfall.concat([
								
									function(resNoeuds,callback){
										const query={
											text:'INSERT INTO bac_on_page (id_page,diese_bac_on_page,num_bac_on_page,label_bac,noeud_start) SELECT $1'+
												', CASE WHEN MAX(diese_bac_on_page) IS NULL THEN 1 ELSE MAX(diese_bac_on_page)+1 END, '+
												'CASE WHEN MAX(num_bac_on_page) IS NULL THEN 0 ELSE MAX(num_bac_on_page)+1 END,'+
												'$2,$3 FROM bac_on_page WHERE id_page = $1 ;',
											values:[resIP[0].id_page,requetes_tot[nb_requete].data.label,resNoeuds[3][0].new_noeud],
										};
										client.query(query,function(err,results){
											if(err){
												callback(err);
											}else{
												//resTot.push(results.rows);
												resTot=[results.rows];
												callback(null,resNoeuds,resTot);
											}
										});
									},

									function(resNoeuds,resTot,callback){
										const query={
											text:'INSERT INTO stop_bac_on_page (id_page,num_bac_on_page,noeud_stop) SELECT $1'+
												' ,MAX(num_bac_on_page),0 FROM bac_on_page WHERE id_page = $1 GROUP BY id_page;',
											values:[resIP[0].id_page]
										};
										
										queryChainedClient(client,query,callback,resNoeuds,resTot);						
									},
									function(resNoeuds,resTot,callback){
										const query={
											text:'SELECT MAX(diese_bac_on_page) diese,MAX(num_bac_on_page) num FROM bac_on_page WHERE id_page = $1 GROUP BY id_page;',
											values:[resIP[0].id_page]
										};
										
										client.query(query,function(err,results){
											if(err){
												callback(err);
											}else{
												//resTot.push(results.rows);
												resTot.push(results.rows);
												callback(null,resNoeuds,resTot);
											}
										});						
									},
									function(resNoeuds,resTot,callback){
										emit.push({text:'add_bac',data:{diese:resTot[2][0].diese,label:requetes_tot[nb_requete].data.label,num:resTot[2][0].num,noeud:resNoeuds[3][0].new_noeud}});
										emit_broad.push({text:'add_bac',data:{diese:resTot[2][0].diese,label:requetes_tot[nb_requete].data.label,num:resTot[2][0].num,noeud:resNoeuds[3][0].new_noeud}});
										nb_requete +=1;
										callback(null,resNoeuds);
									}
								]);
							}									
						//action add_box--------------------------------------------------------------
						}else if(requete.action ==='add_box'){//ok
							if(!checkValide(variables={nom:'45'},needsData=true,errMess='add_box',data=requete.data)){
								console.log(errMess+'.data incorrect dans add_box');
								Abort = true;
							}else{	
								Array_waterfall=Array_waterfall.concat([
									function(resNoeuds,callback){
										const query = {
											text:'SELECT id_pion,taille,svg_pion FROM pion WHERE nom_pion = $1; ',
											values:[requetes_tot[nb_requete].data.nom]
										}
										client.query(query,function(err,results){
											if(err){
												callback(err);
											}else{
												//resTot.push(results.rows);
												resPion=[results.rows];
												callback(null,resNoeuds,resPion);
											}
										});
									},
									function(resNoeuds,resPion,callback){
										const query = {
											text:'SELECT id_pion FROM carac_pions_page c JOIN page p ON c.id_page = p.id_page WHERE p.id_page = $1;',
											values:[resIP[0].id_page]
										}
										client.query(query,function(err,results){
											if(err){
												callback(err);
											}else{
												if(resPion[0].length!=1 ){
													//mauvais diese
													console.log(resPion);
													callback({stack:'mauvais diese add_box'});
													//return false;
												}else{
													resPion.push(results.rows);
													callback(null,resNoeuds,resPion);
												}
											}
										});
									},
									function(resNoeuds,resPion,callback){
										//test pour savoir si on doit ajouter des caractéristiques de pion à la page
										var test = false;
										var j = 0;
										while(!test && (j<resPion[1].length)){
											if(resPion[1][j].id_pion === resPion[0][0].id_pion){
												test = true;
											}
											j++;
										}
										//si test est true, alors il y a un commun entre ceux sur la carac_pion_page et le nouveau
										
										test = !test;
										
										//maintenant, test = true indique qu'on doit rajouter la caractéristique
										
										if(!test){
											var N = 2;
										}else{
											var N=4;
										}
										
										var r={
											resPion:resPion,
											resNoeuds:resNoeuds,
											test:test,
											N:N
										}										
										const query={
											text:'INSERT INTO box_on_page (id_page,id_pion,diese_box_on_page,num_box_on_page,noeud_start) SELECT $1'+
												',$2, new_diese,  new_num, $3  FROM box_on_page b JOIN (SELECT '+
												'CASE WHEN MAX(diese_box_on_page) IS NULL THEN 1 ELSE MAX(diese_box_on_page)+1 END new_diese,'+
												'CASE WHEN MAX(num_box_on_page) IS NULL THEN 1 ELSE MAX(num_box_on_page)+1 END new_num,MAX(id_page) id_pageb'+
												' FROM box_on_page GROUP BY id_page) AS bb ON b.id_page=bb.id_pageb'+
												' WHERE id_page = $1 LIMIT 1; ',
											values:[resIP[0].id_page,r.resPion[0][0].id_pion,r.resNoeuds[3][0].new_noeud],
										};
										client.query(query,function(err,results){
											if(err){
												callback(err);
											}else{
												//resTot.push(results.rows);
												resTot=[results.rows];
												callback(null,r,resTot);
											}
										});
									},
									function(r,resTot,callback){
										const query={
											text:'INSERT INTO stop_box_on_page (id_page,num_box_on_page,noeud_stop) SELECT $1'+
												' ,MAX(num_box_on_page),0 FROM box_on_page WHERE id_page = $1 GROUP BY id_page;',
											values:[resIP[0].id_page]
										};
										queryChainedClient(client,query,callback,r,resTot);						
									},
									function(r,resTot,callback){		
										if(r.test){
											const query={
												text:'INSERT INTO carac_pions_page (id_page,id_pion,change_pion,diese_bac_on_page,taille) SELECT $1,$2,$2,1,$3 ; ',
												values:[resIP[0].id_page,r.resPion[0][0].id_pion,r.resPion[0][0].taille]
											};
											client.query(query,function(err,results){
												if(err){
													callback(err);
												}else{
													//resTot.push(results.rows);
													resTot.push(results.rows);
													callback(null,r,resTot);
												}
											});
										}else{
											callback(null,r,resTot);
										}
															
									},
									function(r,resTot,callback){	
										if(r.test){
											const query={
												text:'INSERT INTO carac_pions_joueur_on_page (id_joueur,id_page,id_pion,change_pion,diese_bac_on_page,taille) SELECT id_joueur,'+
													'$1,$2,$2,1,$3 FROM carac_pions_joueur_on_page WHERE id_page = $1 LIMIT 1; ',
												values:[resIP[0].id_page,r.resPion[0][0].id_pion,r.resPion[0][0].taille]
											};
											client.query(query,function(err,results){
												if(err){
													callback(err);
												}else{
													//resTot.push(results.rows);
													resTot.push(results.rows);
													callback(null,r,resTot);
												}
											});
										}else{
											callback(null,r,resTot);
										}
															
									},
									function(r,resTot,callback){	
										const query={
											text:'SELECT MAX(diese_box_on_page) diese,MAX(num_box_on_page) num FROM box_on_page WHERE id_page = $1; ',
											values:[resIP[0].id_page]
										};
										
										client.query(query,function(err,results){
											if(err){
												callback(err);
											}else{
												//resTot.push(results.rows);
												resTot.push(results.rows);
												callback(null,r,resTot);
											}
										});					
									},
									function(r,resTot,callback){
										if(r.test){
											emit.push({text:'getTabPion',data:null});
											emit_broad.push({text:'getTabPion',data:null});
										}
										emit.push({text:'add_box',data:{nom:requetes_tot[nb_requete].data.nom,diese:resTot[r.N][0].diese,svg:r.resPion[0][0].svg_pion,num:resTot[r.N][0].num,noeud:r.resNoeuds[3][0].new_noeud}});
										emit_broad.push({text:'add_box',data:{nom:requetes_tot[nb_requete].data.nom,diese:resTot[r.N][0].diese,svg:r.resPion[0][0].svg_pion,num:resTot[r.N][0].num,noeud:r.resNoeuds[3][0].new_noeud}});
										nb_requete +=1;
										callback(null,r.resNoeuds);
									}
								]);
							}
						//action add_board--------------------------------------------------------------
						}else if(requete.action ==='add_board'){//ok
							if(!checkValide(variables={nom:'45'},needsData=true,errMess='add_board',data=requete.data)){
								console.log(errMess+'.data incorrect dans add_board');
								Abort = true;
							}else{
								Array_waterfall=Array_waterfall.concat([
									function(resNoeuds,callback){
										const query={
											text:'SELECT id_board,svg_board FROM board WHERE nom_board = $1;',
											values:[requetes_tot[nb_requete].data.nom]
										};
										client.query(query,
											function(err2,resPion){
												if(err2){
													console.log('erreur de add_board : '+err2.stack);
													callback(err2);
													//return false;
												}else{
													//verif que le pion et la board existent
													if(resPion.rows.length!=1 ){
														//mauvais diese
														console.log('erreur de add_board: mauvais pion');
														callback({stack:'erreur de add_board: mauvais pion'});
														//return false;
													}else{
														var r = {
															resPion:resPion.rows,
															resNoeuds,resNoeuds
														}
														callback(null,r);
													}
												}
											}
										);
									},												
									function(r,callback){
										const query={
											text:'INSERT INTO board_on_page (id_page,diese_board_on_page,num_board_on_page,id_board,noeud_start) SELECT '+
												'$1, new_diese, new_num, $2,$3'+
												' FROM page b LEFT JOIN (SELECT '+
												'CASE WHEN MAX(diese_board_on_page) IS NULL THEN 1 ELSE MAX(diese_board_on_page)+1 END new_diese, '+
												'CASE WHEN MAX(num_board_on_page) IS NULL THEN 1 ELSE MAX(num_board_on_page)+1 END new_num,MAX(id_page) id_pageb'+
												' FROM board_on_page GROUP BY id_page) AS bb ON b.id_page=bb.id_pageb'+
												' WHERE id_page = $1 LIMIT 1; ',
											values:[resIP[0].id_page,r.resPion[0].id_board,r.resNoeuds[3][0].new_noeud],
										};
										client.query(query,function(err,results){
											if(err){
												callback(err);
											}else{
												//resTot.push(results.rows);
												resTot=[results.rows];
												callback(null,r,resTot);
											}
										});
									},

									function(r,resTot,callback){
										const query={
											text:'INSERT INTO stop_board_on_page (id_page,num_board_on_page,noeud_stop) SELECT $1'+
												' ,MAX(num_board_on_page),0 FROM board_on_page WHERE id_page = $1 GROUP BY id_page;',
											values:[resIP[0].id_page]
										};
										
										queryChainedClient(client,query,callback,r,resTot);						
									},
									function(r,resTot,callback){
										const query={
											text:' SELECT MAX(diese_board_on_page) diese,MAX(num_board_on_page) num FROM board_on_page  WHERE id_page = $1 GROUP BY id_page;',
											values:[resIP[0].id_page]
										};
										client.query(query,function(err,results){
											if(err){
												callback(err);
											}else{
												//resTot.push(results.rows);
												resTot.push(results.rows);
												callback(null,r,resTot);
											}
										});						
									},

									function(r,resTot,callback){
										emit.push({text:'add_board',data:{nom:requetes_tot[nb_requete].data.nom,diese:resTot[2][0].diese,svg:r.resPion[0].svg_board,num:resTot[2][0].num,noeud:r.resNoeuds[3][0].new_noeud}});
										emit_broad.push({text:'add_board',data:{nom:requetes_tot[nb_requete].data.nom,diese:resTot[2][0].diese,svg:r.resPion[0].svg_board,num:resTot[2][0].num,noeud:r.resNoeuds[3][0].new_noeud}});
										nb_requete +=1;
										callback(null,r.resNoeuds)
									}
								]);
							}															
						//action removeElement--------------------------------------------------------------
						}else if(requete.action ==='removeElement'){//
							if(!checkValide(variables={id:'45'},needsData=true,errMess='removeElement',data=requete.data)){
								console.log(errMess+'.data incorrect dans removeElement');
								Abort = true;
							}else{
								var classe = /^[a-z]+/.exec(requete.data.id).toString();
								var id = parseInt(/[0-9]+$/.exec(requete.data.id),10);
								if(isNaN(id) && classe !='trash'){
									console.log('mauvais diese1');
								}else if(classe==='board'){//ok
									
									Array_waterfall=Array_waterfall.concat([
									
										function(resNoeuds,callback){
											const query={
												text:' SELECT id_page,num_board_on_page,$1 FROM board_on_page p'+
													' LEFT JOIN (SELECT COUNT(noeud_stop) nbstop,MAX(id_page) id, MAX(num_board_on_page) num FROM stop_board_on_page'+
													' WHERE id_page = $2 AND ARRAY[noeud_stop] <@ $3 GROUP BY id_page,num_board_on_page) AS g'+
													' ON p.id_page=g.id AND p.num_board_on_page = g.num'+
													' WHERE id_page = $2 AND diese_board_on_page = $4 AND ARRAY[noeud_start] <@ $3 AND ( COALESCE(g.nbstop,0)=0 OR g.nbstop=0) ;',
												values:[resIP[0].noeud_courant,resIP[0].id_page,resNoeuds[4],id]
											}
											
											client.query(query,
												function(err,res2){
													if(err){
														console.log('erreur remove board: '+err.stack);
														callback(err);
													}else if(res2.rows.length!=1){
														//console.log('bad id remove board');
														callback({stack:'bad id remove board'});
													}else{
														callback(null,resNoeuds);
													}
												}
											);
										},
												
										function(resNoeuds,callback){
											const query={
												text:'INSERT INTO stop_pion_on_board_on_page (id_page,diese_board_on_page,num_pion_on_board_on_page,noeud_stop)'+
													' SELECT id_page,diese_board_on_page,num_pion_on_board_on_page,$1 FROM pion_on_board_on_page p'+
													' LEFT JOIN (SELECT COUNT(noeud_stop) nbstop,MAX(id_page) id, MAX(num_pion_on_board_on_page) num, MAX(diese_board_on_page) diese FROM stop_pion_on_board_on_page'+
													' WHERE id_page = $2 AND diese_board_on_page = $3 AND ARRAY[noeud_stop] <@ $4 GROUP BY id_page,diese_board_on_page,num_pion_on_board_on_page) AS g'+
													' ON p.id_page=g.id AND p.num_pion_on_board_on_page = g.num AND p.diese_board_on_page=g.diese'+
													' WHERE id_page = $2 AND diese_board_on_page = $3 AND ARRAY[noeud_start] <@ $4 AND ( COALESCE(g.nbstop,0)=0 OR g.nbstop=0) ;',
												values:[resNoeuds[3][0].new_noeud,resIP[0].id_page,id,resNoeuds[4]],
											};
											client.query(query,function(err,results){
												if(err){
													callback(err);
												}else{
													//resTot.push(results.rows);
													resTot=[results.rows];
													callback(null,resNoeuds,resTot);
												}
											});
										},

										function(resNoeuds,resTot,callback){
											const query={
												text:'INSERT INTO stop_board_on_page (id_page,num_board_on_page,noeud_stop)'+
													' SELECT id_page,num_board_on_page,$1 FROM board_on_page p'+
													' LEFT JOIN (SELECT COUNT(noeud_stop) nbstop,MAX(id_page) id, MAX(num_board_on_page) num FROM stop_board_on_page'+
													' WHERE id_page = $2 AND ARRAY[noeud_stop] <@ $3 GROUP BY id_page,num_board_on_page) AS g'+
													' ON p.id_page=g.id AND p.num_board_on_page = g.num'+
													' WHERE id_page = $2 AND diese_board_on_page = $4 AND ARRAY[noeud_start] <@ $3 AND ( COALESCE(g.nbstop,0)=0 OR g.nbstop=0) ;',
												values:[resNoeuds[3][0].new_noeud,resIP[0].id_page,resNoeuds[4],id]
											};
											
											client.query(query,function(err,results){
												if(err){
													callback(err);
												}else{
													//resTot.push(results.rows);
													resTot.push(results.rows);
													callback(null,resNoeuds,resTot);
												}
											});						
										},

										function(resNoeuds,resTot,callback){
											emit.push({text:'removeElement',data:{id:classe+'-'+id,noeud:resNoeuds[3][0].new_noeud}});
											emit_broad	.push({text:'removeElement',data:{id:classe+'-'+id,noeud:resNoeuds[3][0].new_noeud}});
											nb_requete +=1;
											callback(null,resNoeuds);
										}
									]);
								}else if(classe==='bac'){//ok
									
									Array_waterfall=Array_waterfall.concat([
									
										function(resNoeuds,callback){
											const query={
												text:' SELECT num_bac_on_page FROM bac_on_page p'+
												' LEFT JOIN (SELECT COUNT(noeud_stop) nbstop,MAX(id_page) id, MAX(num_bac_on_page) num FROM stop_bac_on_page'+
												' WHERE id_page = $1 AND ARRAY[noeud_stop] <@ $2 GROUP BY id_page,num_bac_on_page) AS g'+
												' ON p.id_page=g.id AND p.num_bac_on_page = g.num'+
												' WHERE id_page = $1 AND diese_bac_on_page = $3 AND ARRAY[noeud_start] <@ $2 AND ( COALESCE(g.nbstop,0)=0 OR g.nbstop=0) ;',
												values:[resIP[0].id_page,resNoeuds[4],id]
											}
											
											client.query(query,
												function(err,res2){
													if(err){
														console.log('erreur remove bac: '+err.stack);
														callback(err)
													}else if(res2.rows.length!=1){
														//console.log('bad id remove bac');
														callback({stack:'bad id remove bac'});
													}else{
														callback(null,resNoeuds);
													}
												}
											);
										},
																										
										function(resNoeuds,callback){
											const query={
												text:'UPDATE joueur_on_page SET click_board_move_bac = null WHERE id_page = $1 AND click_board_move_bac = $2 ; ',
												values:[resIP[0].id_page,id]
											};
											client.query(query,function(err,results){
												if(err){
													callback(err);
												}else{
													//resTot.push(results.rows);
													resTot=[results.rows];
													callback(null,resNoeuds,resTot);
												}
											});
										},
										
										function(resNoeuds,resTot,callback){
											const query={
												text:'UPDATE carac_pions_page SET diese_bac_on_page = null WHERE id_page = $1 AND diese_bac_on_page = $2 ; ',
												values:[resIP[0].id_page,id]
											};
											
											queryChainedClient(client,query,callback,resNoeuds,resTot);
										},

										function(resNoeuds,resTot,callback){
											const query={
												text:'UPDATE carac_pions_joueur_on_page SET diese_bac_on_page = null WHERE id_page = $1 AND diese_bac_on_page = $2 ; ',
												values:[resIP[0].id_page,id]
											};
											
											queryChainedClient(client,query,callback,resNoeuds,resTot);
										},
										
										function(resNoeuds,resTot,callback){
											const query={
												text:'INSERT INTO stop_pion_on_bac_on_page (id_page,diese_bac_on_page,num_pion_on_bac_on_page,noeud_stop)'+
													' SELECT id_page,diese_bac_on_page,num_pion_on_bac_on_page,$1 FROM pion_on_bac_on_page p'+
													' LEFT JOIN (SELECT COUNT(noeud_stop) nbstop,MAX(id_page) id, MAX(num_pion_on_bac_on_page) num, MAX(diese_bac_on_page) diese FROM stop_pion_on_bac_on_page'+
													' WHERE id_page = $2 AND diese_bac_on_page = $3 AND ARRAY[noeud_stop] <@ $4 GROUP BY id_page,diese_bac_on_page,num_pion_on_bac_on_page) AS g'+
													' ON p.id_page=g.id AND p.num_pion_on_bac_on_page = g.num AND p.diese_bac_on_page=g.diese'+
													' WHERE id_page = $2 AND diese_bac_on_page = $3 AND ARRAY[noeud_start] <@ $4 AND ( COALESCE(g.nbstop,0)=0 OR g.nbstop=0) ;',
												values:[resNoeuds[3][0].new_noeud,resIP[0].id_page,id,resNoeuds[4]]
											};
											
											queryChainedClient(client,query,callback,resNoeuds,resTot);
										},
										
										function(resNoeuds,resTot,callback){
											const query={
												text:'INSERT INTO stop_bac_on_page (id_page,num_bac_on_page,noeud_stop)'+
													' SELECT id_page,num_bac_on_page,$1 FROM bac_on_page p'+
													' LEFT JOIN (SELECT COUNT(noeud_stop) nbstop,MAX(id_page) id, MAX(num_bac_on_page) num FROM stop_bac_on_page'+
														' WHERE id_page = $2 AND ARRAY[noeud_stop] <@ $3 GROUP BY id_page,num_bac_on_page) AS g'+
													' ON p.id_page=g.id AND p.num_bac_on_page = g.num'+
													' WHERE id_page = $2 AND diese_bac_on_page = $4 AND ARRAY[noeud_start] <@ $3 AND ( COALESCE(g.nbstop,0)=0 OR g.nbstop=0) ;',
												
												values:[resNoeuds[3][0].new_noeud,resIP[0].id_page,resNoeuds[4],id]
											};
											
											client.query(query,function(err,results){
												if(err){
													callback(err);
												}else{
													//resTot.push(results.rows);
													resTot.push(results.rows);
													callback(null,resNoeuds,resTot);
												}
											});						
										},
										function(resNoeuds,resTot,callback){
											emit.push({text:'removeElement',data:{id:classe+'-'+id,noeud:resNoeuds[3][0].new_noeud}});
											emit_broad.push({text:'removeElement',data:{id:classe+'-'+id,noeud:resNoeuds[3][0].new_noeud}});
											nb_requete +=1;
											callback(null,resNoeuds);
										}
									]);
								}else if(classe==='box'){//ok
									
									Array_waterfall=Array_waterfall.concat([
									
										function(resNoeuds,callback){
											
											const query={
												text:' SELECT num_box_on_page,$1 FROM box_on_page p'+
												' LEFT JOIN (SELECT COUNT(noeud_stop) nbstop,MAX(id_page) id, MAX(num_box_on_page) num FROM stop_box_on_page'+
													' WHERE id_page = $2 AND ARRAY[noeud_stop] <@ $3 GROUP BY id_page,num_box_on_page) AS g'+
												' ON p.id_page=g.id AND p.num_box_on_page = g.num'+
												' WHERE id_page = $2 AND diese_box_on_page = $4 AND ARRAY[noeud_start] <@ $3 AND ( COALESCE(g.nbstop,0)=0 OR g.nbstop=0) ;',
												values:[resIP[0].noeud_courant,resIP[0].id_page,resNoeuds[4],id]
											}
											
											client.query(query,
												function(err,res2){
													if(err){
														//console.log('erreur remove box: '+err.stack);
														scallback(err);
													}else if(res2.rows.length!=1){
														//console.log('bad id remove box');
														//socket.emit('refresh');
														callback({stack:'bad id remove box'});
													}else{
														callback(null,resNoeuds);
													}
												}
											);
										},
												
										function(resNoeuds,callback){
											const query={
												text:'INSERT INTO stop_box_on_page (id_page,num_box_on_page,noeud_stop)'+
												' SELECT id_page,num_box_on_page,$1 FROM box_on_page p'+
												' LEFT JOIN (SELECT COUNT(noeud_stop) nbstop,MAX(id_page) id, MAX(num_box_on_page) num FROM stop_box_on_page'+
													' WHERE id_page = $2 AND ARRAY[noeud_stop] <@ $3 GROUP BY id_page,num_box_on_page) AS g'+
												' ON p.id_page=g.id AND p.num_box_on_page = g.num'+
												' WHERE id_page = $2 AND diese_box_on_page = $4 AND ARRAY[noeud_start] <@ $3 AND ( COALESCE(g.nbstop,0)=0 OR g.nbstop=0) ;',					
												values:[resNoeuds[3][0].new_noeud,resIP[0].id_page,resNoeuds[4],id]
											}
											
											client.query(query,
												function(err){
													if(err){
														console.log('erreure de remove : '+err.stack);
														//socket.emit('refresh');
														callback(err);
														//return false;
													}else{
														emit.push({text:'removeElement',data:{id:classe+'-'+id,noeud:resNoeuds[3][0].new_noeud}});
														emit_broad.push({text:'removeElement',data:{id:classe+'-'+id,noeud:resNoeuds[3][0].new_noeud}});
														nb_requete +=1;
														callback(null,resNoeuds);
													}
												}
											);
										}
									]);
								}else if(classe==='pion'){//ok
									Array_waterfall=Array_waterfall.concat([
										function(resNoeuds,callback){
											const query={
												text:' SELECT num_pion_on_board_on_page FROM pion_on_board_on_page p'+
													' LEFT JOIN (SELECT COUNT(noeud_stop) nbstop,MAX(id_page) id, MAX(num_pion_on_board_on_page) num, MAX(diese_board_on_page) diese FROM stop_pion_on_board_on_page'+
													' WHERE id_page = $1 AND ARRAY[noeud_stop] <@ $2 GROUP BY id_page,diese_board_on_page,num_pion_on_board_on_page) AS g'+
													' ON p.id_page=g.id AND p.num_pion_on_board_on_page = g.num AND p.diese_board_on_page=g.diese'+
													' WHERE id_page = $1 AND num_pion_on_board_on_page = $3 AND ARRAY[noeud_start] <@ $2 AND ( COALESCE(g.nbstop,0)=0 OR g.nbstop=0) ;',
												values:[resIP[0].id_page,resNoeuds[4],id]
											}
											
											client.query(query,
												function(err,res2){
													if(err){
														//console.log('erreur remove pion: '+err.stack);
														//socket.emit('refresh');
														callback(err);
													}else if(res2.rows.length!=1){
														//console.log('bad id remove pion');
														callback({stack:'bad id remove pion'});
														//socket.emit('refresh');
													}else{
														callback(null,resNoeuds);
													}
												}
											);
										},
										
										function(resNoeuds,callback){
											const query={
												text:'INSERT INTO stop_pion_on_board_on_page (id_page,diese_board_on_page,num_pion_on_board_on_page,noeud_stop)'+
													' SELECT id_page,diese_board_on_page,num_pion_on_board_on_page,$1 FROM pion_on_board_on_page p'+
													' LEFT JOIN (SELECT COUNT(noeud_stop) nbstop,MAX(id_page) id, MAX(num_pion_on_board_on_page) num, MAX(diese_board_on_page) diese FROM stop_pion_on_board_on_page'+
													' WHERE id_page = $2 AND ARRAY[noeud_stop] <@ $3 GROUP BY id_page,diese_board_on_page,num_pion_on_board_on_page) AS g'+
													' ON p.id_page=g.id AND p.num_pion_on_board_on_page = g.num AND p.diese_board_on_page=g.diese'+
													' WHERE id_page = $2 AND num_pion_on_board_on_page = $4 AND ARRAY[noeud_start] <@ $3 AND ( COALESCE(g.nbstop,0)=0 OR g.nbstop=0) ;',
												values:[resNoeuds[3][0].new_noeud,resIP[0].id_page,resNoeuds[4],id]
											}
								
											client.query(query,
												function(err,res2){
													if(err){
														console.log('erreure de remove : '+err.stack);
														//socket.emit('refresh');
														callback(err);
													}else{
														//console.log(res2)
														emit.push({text:'removeElement',data:{id:classe+'-'+id,noeud:resNoeuds[3][0].new_noeud}});
														emit_broad.push({text:'removeElement',data:{id:classe+'-'+id,noeud:resNoeuds[3][0].new_noeud}});
														nb_requete +=1;
														callback(null,resNoeuds);
													}
												}
											);
										}
									]);
								}else if(classe==='groupe'){//ok
									
									Array_waterfall=Array_waterfall.concat([
									
										function(resNoeuds,callback){
											const query={
												text:'SELECT num_pion_on_bac_on_page FROM pion_on_bac_on_page p'+
												' LEFT JOIN (SELECT COUNT(noeud_stop) nbstop,MAX(id_page) id, MAX(num_pion_on_bac_on_page) num, MAX(diese_bac_on_page) diese FROM stop_pion_on_bac_on_page'+
													' WHERE id_page = $1 AND ARRAY[noeud_stop] <@ $2 GROUP BY id_page,diese_bac_on_page,num_pion_on_bac_on_page) AS g'+
												' ON p.id_page=g.id AND p.num_pion_on_bac_on_page = g.num AND p.diese_bac_on_page=g.diese'+
												' WHERE id_page = $1 AND num_pion_on_bac_on_page = $3 AND ARRAY[noeud_start] <@ $2 AND ( COALESCE(g.nbstop,0)=0 OR g.nbstop=0) ;',
												values:[resIP[0].id_page,resNoeuds[4],id]
											}
											client.query(query,
												function(err,res2){
													if(err){
														//console.log('erreur remove groupe: '+err.stack);
														//socket.emit('refresh');
														callback(err);
													}else if(res2.rows.length!=1){
														//console.log('bad id remove groupe');
														//socket.emit('refresh');
														callback({stack:'bad id remove groupe'});
													}else{
														callback(null,resNoeuds);
													}
												}
											);
										},
										function(resNoeuds,callback){							
											const query={
												text:'INSERT INTO stop_pion_on_bac_on_page (id_page,diese_bac_on_page,num_pion_on_bac_on_page,noeud_stop)'+
												' SELECT id_page,diese_bac_on_page,num_pion_on_bac_on_page,$1 FROM pion_on_bac_on_page p'+
												' LEFT JOIN (SELECT COUNT(noeud_stop) nbstop,MAX(id_page) id, MAX(num_pion_on_bac_on_page) num, MAX(diese_bac_on_page) diese FROM stop_pion_on_bac_on_page'+
													' WHERE id_page = $2 AND ARRAY[noeud_stop] <@ $3 GROUP BY id_page,diese_bac_on_page,num_pion_on_bac_on_page) AS g'+
												' ON p.id_page=g.id AND p.num_pion_on_bac_on_page = g.num AND p.diese_bac_on_page=g.diese'+
												' WHERE id_page = $2 AND num_pion_on_bac_on_page = $4 AND ARRAY[noeud_start] <@ $3 AND ( COALESCE(g.nbstop,0)=0 OR g.nbstop=0) ;',
												values:[resNoeuds[3][0].new_noeud,resIP[0].id_page,resNoeuds[4],id]
											}
											
											client.query(query,
												function(err){
													if(err){
														//console.log('erreure de remove : '+err.stack);
														//socket.emit('refresh');
														callback(err);
														//return false;
													}else{
														emit.push({text:'removeElement',data:{id:classe+'-'+id,noeud:resNoeuds[3][0].new_noeud}});
														emit_broad.push({text:'removeElement',data:{id:classe+'-'+id,noeud:resNoeuds[3][0].new_noeud}});
														nb_requete +=1;
														callback(null,resNoeuds);
													//return true;
													}
												}
											);
										}
									]);
								}else{
									console.log('erreur de remove : mauvaise classe');
									Abort=true;
									//return false;
								}
							}
								
						//action changePionOnboard--------------------------------------------------------------

						}else if(requete.action ==='changePionOnboard'){//ok
							if(!checkValide(variables={
								diese:1,
								type:'45',
								top:1,
								left:1,
								height:1,
								board:1},needsData=true,errMess='changePionOnboard',data=requete.data)){
								
								console.log(requete.data);
								
								console.log(errMess+'.data incorrect dans changePionOnboard');
								Abort = true
							}else{
								Array_waterfall=Array_waterfall.concat([
									function(resNoeuds,callback){
										const query = {
											text:'SELECT num_pion_on_board_on_page,diese_board_on_page FROM pion_on_board_on_page p'+
												' LEFT JOIN (SELECT COUNT(noeud_stop) nbstop,MAX(id_page) id, MAX(num_pion_on_board_on_page) num, MAX(diese_board_on_page) diese FROM stop_pion_on_board_on_page'+
												' WHERE id_page = $1 AND ARRAY[noeud_stop] <@ $2 GROUP BY id_page,diese_board_on_page,num_pion_on_board_on_page) AS g'+
												' ON p.id_page=g.id AND p.num_pion_on_board_on_page = g.num AND p.diese_board_on_page=g.diese'+
												' WHERE id_page = $1 AND diese_pion_on_board_on_page = $3'+
												' AND ARRAY[noeud_start] <@ $2 AND ( COALESCE(g.nbstop,0)=0 OR g.nbstop=0) ;',
											values:[resIP[0].id_page,resNoeuds[4],requetes_tot[nb_requete].data.diese]
										}
										//console.log(noeuds)
										client.query(query,function(err,results){
											if(err){
												callback(err);
											}else{
												//resTot.push(results.rows);
												results2=[results.rows];
												callback(null,resNoeuds,results2);
											}
										});
									},
									
									function(resNoeuds,results2,callback){
										const query = {
											text:'SELECT id_pion,taille,svg_pion,directed FROM pion WHERE nom_pion = $1;',
											values:[requetes_tot[nb_requete].data.type]
										}
										queryChainedClient(client,query,callback,resNoeuds,results2);
									},
									function(resNoeuds,results2,callback){
										const query = {
											text:'SELECT c.id_pion FROM carac_pions_page c JOIN pion p ON c.id_pion = p.id_pion WHERE p.nom_pion = $1 AND c.id_page = $2; ',
											values:[requetes_tot[nb_requete].data.type,resIP[0].id_page]
										}
										queryChainedClient(client,query,callback,resNoeuds,results2);
									},																								
									function(resNoeuds,results2,callback){
										const query = {
											text:'SELECT num_board_on_page FROM board_on_page b'+
												' LEFT JOIN (SELECT COUNT(noeud_stop) nbstop,MAX(id_page) id, MAX(num_board_on_page) num FROM stop_board_on_page'+
												' WHERE id_page = $1 AND ARRAY[noeud_stop] <@ $2 GROUP BY id_page,num_board_on_page) AS g'+
												' ON b.id_page=g.id AND b.num_board_on_page = g.num'+
												' WHERE b.id_page = $1 AND diese_board_on_page=$3'+
												' AND ARRAY[b.noeud_start] <@ $2 AND ( COALESCE(g.nbstop,0)=0 OR g.nbstop=0) ;',
											values:[resIP[0].id_page,resNoeuds[4],requetes_tot[nb_requete].data.board]
										}
										client.query(query,function(err,results){
											results2.push(results.rows);
											if(err){
												callback(err);
											}else if(results2[0].length!=1 || results2[3].length!=1){
												//mauvais diese
												callback({stack:'mauvais diese changePionOnBoard'});
											}else if(results2[1].length!=1){
												//pas le bon pion
												callback({stack:'mauvais pion changePionOnBoard'});
											}else{
												var r = {
													resNoeuds:resNoeuds,
													results2:results2
												};
												callback(null,r);
											}
										});
									},
								
									function(r,callback){
										//test pour savoir si on doit ajouter des caractéristiques de pion à la page
										r.test = results2[1].length!=1;
										const query={
											text:'INSERT INTO pion_on_board_on_page (id_page,diese_pion_on_board_on_page,num_pion_on_board_on_page,id_pion,diese_board_on_page,position_x,position_y,taille,noeud_start) '+
												' SELECT $1,$2, pp.new_num,$3,$4,$5,$6,$7, new_noeud '+
												' FROM page p LEFT JOIN '+
												'(SELECT MAX(id_page) id_pageb,MAX(noeud_suivant) new_noeud FROM save_on_page GROUP BY id_page) AS s ON p.id_page=s.id_pageb'+
												' LEFT JOIN (SELECT MAX(num_pion_on_board_on_page)+1 new_num, MAX(id_page) id_pagec FROM pion_on_board_on_page GROUP BY id_page) AS pp ON p.id_page=pp.id_pagec'+
												' WHERE id_page = $1 LIMIT 1; ',
											values:[resIP[0].id_page,requetes_tot[nb_requete].data.diese,r.results2[1][0].id_pion,requetes_tot[nb_requete].data.board,requetes_tot[nb_requete].data.left,requetes_tot[nb_requete].data.top,requetes_tot[nb_requete].data.height]
										};
										client.query(query,function(err,results){
											if(err){
												callback(err);
											}else{
												//resTot.push(results.rows);
												result3=[results.rows];
												callback(null,r,result3);
											}
										});
									},

									function(r,result3,callback){
										const query={
											text:'INSERT INTO stop_pion_on_board_on_page (id_page,diese_board_on_page,num_pion_on_board_on_page,noeud_stop)'+
												' SELECT $1,$2,$3,$4; ',
											values:[resIP[0].id_page,r.results2[0][0].diese_board_on_page,r.results2[0][0].num_pion_on_board_on_page,r.resNoeuds[3][0].new_noeud],
										};
										queryChainedClient(client,query,callback,r,result3);						
									},
									function(r,result3,callback){
										const query = {
											text:' SELECT MAX(num_pion_on_board_on_page) num FROM pion_on_board_on_page  WHERE id_page = $1 GROUP BY id_page;',
											values:[resIP[0].id_page]
										}
										queryChainedClient(client,query,callback,r,result3);
									},
									function(r,result3,callback){
										const query = {
											text:'INSERT INTO stop_pion_on_board_on_page (id_page,diese_board_on_page,num_pion_on_board_on_page,noeud_stop) SELECT '+
												'$1,$2 ,MAX(num_pion_on_board_on_page),0 FROM pion_on_board_on_page WHERE id_page = $1 GROUP BY id_page;',
											values:[resIP[0].id_page,requetes_tot[nb_requete].data.board]
										}
										queryChainedClient(client,query,callback,r,result3);
									},
									
									function(r,result3,callback){
										if(r.test){
											const query={
												text:'INSERT INTO carac_pions_page (id_page,id_pion,change_pion,diese_bac_on_page,taille) SELECT ($1,$2,$2,1,$3) ; ',
												values:[resIP[0].id_page,r.results2[1][0].id_pion,r.results2[1][0].taille]
											};
											
											client.query(query,function(err,results){
												if(err){
													callback(err);
												}else{
													//resTot.push(results.rows);
													result3.push(results.rows);
													callback(null,r,result3);
												}
											});
										}else{
											callback(null,r,result3);
										}
															
									},
									function(r,result3,callback){
										if(r.test){
											const query={
												text:'INSERT INTO carac_pions_joueur_on_page (id_joueur,id_page,id_pion,change_pion,diese_bac_on_page,taille) SELECT id_joueur,$1,$2,$2,1,$3 FROM carac_pions_joueur_on_page WHERE id_page = $1 LIMIT 1; ',
												values:[resIP[0].id_page,results2[1][0].id_pion,results2[1][0].taille]
											};
											
											client.query(query,function(err,results){
												if(err){
													callback(err);
												}else{
													//resTot.push(results.rows);
													result3.push(results.rows);
													callback(null,r,result3);
												}
											});
										}else{
											callback(null,r,result3);
										}
															
									},
									function(r,result3,callback){
										if(r.test){
											emit.push({text:'getTabPion',data:null});
											emit_broad.push({text:'getTabPion',data:null});
										}
										emit.push({text:'removeElement',data:{id:'pion-'+r.results2[0][0].num_pion_on_board_on_page,noeud:r.resNoeuds[3][0].new_noeud}});
										emit_broad.push({text:'removeElement',data:{id:'pion-'+r.results2[0][0].num_pion_on_board_on_page,noeud:r.resNoeuds[3][0].new_noeud}});
										
										requetes_tot[nb_requete].data.svg = r.results2[1][0].svg_pion;
										requetes_tot[nb_requete].data.directed = r.results2[1][0].directed;
										requetes_tot[nb_requete].data.num =result3[2][0].num;
										requetes_tot[nb_requete].data.noeud =r.resNoeuds[3][0].new_noeud;
										
										emit.push({text:'createPionOnboard',data:requetes_tot[nb_requete].data});
										emit_broad.push({text:'createPionOnboard',data:requetes_tot[nb_requete].data});
										nb_requete +=1;
										callback(null,r.resNoeuds);		
									}
								]);
							}
						//action modifyGroup--------------------------------------------------------------
						}else if(requete.action ==='modifyGroup'){//ok
							if(!checkValide(variables={
								diese:1,
								n:1,
								bac:1,
								type:''},
								needsData=true,errMess='modifyGroup',data=requete.data)){
								console.log(errMess+'.data incorrect dans modifyGroup');
								Abort = true
							}else{
								

								Array_waterfall=Array_waterfall.concat([
									function(resNoeuds,callback){
										const query = {
											text:'SELECT num_pion_on_bac_on_page FROM pion_on_bac_on_page p'+
												' LEFT JOIN (SELECT COUNT(noeud_stop) nbstop,MAX(id_page) id, MAX(num_pion_on_bac_on_page) num, MAX(diese_bac_on_page) diese FROM stop_pion_on_bac_on_page'+
												' WHERE id_page = $1 AND ARRAY[noeud_stop] <@ $2 GROUP BY id_page,diese_bac_on_page,num_pion_on_bac_on_page) AS g'+
												' ON p.id_page=g.id AND p.num_pion_on_bac_on_page = g.num AND p.diese_bac_on_page=g.diese'+
												' WHERE id_page = $1 AND diese_pion_on_bac_on_page = $3'+
												' AND ARRAY[noeud_start] <@ $2 AND ( COALESCE(g.nbstop,0)=0 OR g.nbstop=0) ;',
											values:[resIP[0].id_page,resNoeuds[4],requetes_tot[nb_requete].data.diese]
										}
										client.query(query,function(err,results){
											if(err){
												callback(err);
											}else{
												//resTot.push(results.rows);
												results2=[results.rows];
												callback(null,resNoeuds,results2);
											}
										});
									},
									
									function(resNoeuds,results2,callback){
										const query = {
											text:'SELECT id_pion,taille,svg_pion,directed FROM pion WHERE nom_pion = $1;',
											values:[requetes_tot[nb_requete].data.type]
										}
										queryChainedClient(client,query,callback,resNoeuds,results2);
									},
									function(resNoeuds,results2,callback){
										const query = {
											text:'SELECT c.id_pion FROM carac_pions_page c JOIN pion p ON c.id_pion = p.id_pion WHERE p.nom_pion = $1 AND c.id_page = $2; ',
											values:[requetes_tot[nb_requete].data.type,resIP[0].id_page]
										}
										queryChainedClient(client,query,callback,resNoeuds,results2);
									},																								
									function(resNoeuds,results2,callback){
										const query = {
											text:'SELECT num_bac_on_page FROM bac_on_page b'+
												' LEFT JOIN (SELECT COUNT(noeud_stop) nbstop,MAX(id_page) id, MAX(num_bac_on_page) num FROM stop_bac_on_page'+
												' WHERE id_page = $1 AND ARRAY[noeud_stop] <@ $2 GROUP BY id_page,num_bac_on_page) AS g'+
												' ON b.id_page=g.id AND b.num_bac_on_page = g.num'+
												' WHERE b.id_page = $1 AND diese_bac_on_page=$3'+
												' AND ARRAY[b.noeud_start] <@ $2 AND ( COALESCE(g.nbstop,0)=0 OR g.nbstop=0) ;',
											values:[resIP[0].id_page,resNoeuds[4],requetes_tot[nb_requete].data.bac]
										}
										client.query(query,function(err,results){
											results2.push(results.rows);
											if(err){
												callback(err);
											//verif que le pion et la board existent
											}else if(results2[0].length!=1 || results2[3].length!=1){
													callback({stack:'mauvais diese2'});
											}else if(results2[1].length!=1){
												//pas le bon pion
												callback({stack:'pion incorrect'});
											}else{
												r={
													resNoeuds:resNoeuds,
													results2:results2
												};
												callback(null,r);
											}
										});
									},
									function(r,callback){
										const query={
											text:'INSERT INTO stop_pion_on_bac_on_page (id_page,diese_bac_on_page,num_pion_on_bac_on_page,noeud_stop)'+
												' SELECT $1,$2,$3,$4; ',
											values:[resIP[0].id_page,requetes_tot[nb_requete].data.bac,r.results2[0][0].num_pion_on_bac_on_page,r.resNoeuds[3][0].new_noeud],
										};
										client.query(query,function(err,results){
											if(err){
												callback(err);
											}else{
												//resTot.push(results.rows);
												result3=[results.rows];
												callback(null,r,result3);
											}
										});
									},
									
									function(r,result3,callback){
										const query={
											text:'INSERT INTO pion_on_bac_on_page (id_page,diese_bac_on_page,id_pion,nombre_pions,diese_pion_on_bac_on_page,num_pion_on_bac_on_page,noeud_start) SELECT '+
												'$1,$2,$3,$4,$5, new_num, new_noeud  FROM page p'+
												' LEFT JOIN (SELECT MAX(id_page) id_pagea,MAX(num_pion_on_bac_on_page)+1 new_num FROM pion_on_bac_on_page GROUP BY id_page) AS n '+
												'ON p.id_page=n.id_pagea '+
												'LEFT JOIN (SELECT MAX(id_page) id_pageb,MAX(noeud_suivant) new_noeud FROM save_on_page GROUP BY id_page) AS s '+
												'ON n.id_pagea=s.id_pageb WHERE id_pagea=$1 LIMIT 1;',
											values:[resIP[0].id_page,requetes_tot[nb_requete].data.bac,r.results2[1][0].id_pion,requetes_tot[nb_requete].data.n,requetes_tot[nb_requete].data.diese]
										};
										
										queryChainedClient(client,query,callback,r,result3);						
									},

									function(r,result3,callback){
										const query = {
											text:'INSERT INTO stop_pion_on_bac_on_page (id_page,diese_bac_on_page,num_pion_on_bac_on_page,noeud_stop) SELECT '+
												'$1,$2 ,MAX(num_pion_on_bac_on_page),0 FROM pion_on_bac_on_page WHERE id_page = $1 GROUP BY id_page;',
											values:[resIP[0].id_page,requetes_tot[nb_requete].data.bac]
										}
										queryChainedClient(client,query,callback,r,result3);
									},
									
									function(r,result3,callback){	
										const query={
											text:' SELECT MAX(num_pion_on_bac_on_page) num FROM pion_on_bac_on_page  WHERE id_page = $1 GROUP BY id_page;',
											values:[resIP[0].id_page]
										};
										
										client.query(query,function(err,results){
											if(err){
												callback(err);
											}else{
												//resTot.push(results.rows);
												result3.push(results.rows);
												callback(null,r,result3);
											}
										});
									},function(r,result3,callback){
										
										emit.push({text:'removeElement',data:{id:'groupe-'+r.results2[0][0].num_pion_on_bac_on_page,noeud:r.resNoeuds[3][0].new_noeud}});
										emit_broad.push({text:'removeElement',data:{id:'groupe-'+r.results2[0][0].num_pion_on_bac_on_page,noeud:r.resNoeuds[3][0].new_noeud}});
										
										requetes_tot[nb_requete].data.svg = r.results2[1][0].svg_pion;
										requetes_tot[nb_requete].data.directed = r.results2[1][0].directed;
										requetes_tot[nb_requete].data.num =result3[3][0].num;
										requetes_tot[nb_requete].data.noeud =r.resNoeuds[3][0].new_noeud;
										
										emit.push({text:'createGroupeOnbac',data:requetes_tot[nb_requete].data});
										emit_broad.push({text:'createGroupeOnbac',data:requetes_tot[nb_requete].data});
										nb_requete +=1;
										callback(null,r.resNoeuds);															
									}
								]);
							}
						//action createPionOnboard--------------------------------------------------------------
						}else if(requete.action ==='createPionOnboard'){//ok
							if(!checkValide(variables={
								type:'45',
								top:1,
								left:1,
								height:1,
								board:1},needsData=true,errMess='createPionOnboard',data=requete.data)){
								
								console.log(errMess+'.data incorrect dans createPionOnboard');
								Abort = true
								//return false;
							}else{
								Array_waterfall=Array_waterfall.concat([
									function(resNoeuds,callback){
										const query = {
											text:'SELECT id_pion,taille,svg_pion,directed FROM pion WHERE nom_pion = $1;',
											values:[requetes_tot[nb_requete].data.type]
										}
										client.query(query,function(err,results){
											if(err){
												callback(err);
											}else{
												results2=[results.rows];
												callback(null,resNoeuds,results2);
											}
										});
									},
									
									function(resNoeuds,results2,callback){
										const query = {
											text:'SELECT c.id_pion FROM carac_pions_page c JOIN pion p ON c.id_pion=p.id_pion WHERE p.nom_pion = $1 AND c.id_page = $2; ',
											values:[requetes_tot[nb_requete].data.type,resIP[0].id_page]
										}
										queryChainedClient(client,query,callback,resNoeuds,results2);
									},
																				
									function(resNoeuds,results2,callback){
										const query = {
											text:'SELECT num_board_on_page FROM board_on_page b'+
												' LEFT JOIN (SELECT COUNT(noeud_stop) nbstop,MAX(id_page) id, MAX(num_board_on_page) num FROM stop_board_on_page'+
												' WHERE id_page = $1 AND ARRAY[noeud_stop] <@ $2 GROUP BY id_page,num_board_on_page) AS g'+
												' ON b.id_page=g.id AND b.num_board_on_page = g.num'+
												' WHERE b.id_page = $1 AND diese_board_on_page=$3'+
												' AND ARRAY[b.noeud_start] <@ $2 AND ( COALESCE(g.nbstop,0)=0 OR g.nbstop=0) ;',
											values:[resIP[0].id_page,resNoeuds[4],requetes_tot[nb_requete].data.board]
										}
										client.query(query,function(err,results){
											results2.push(results.rows);
											if(err){
												callback(err);
											}else if(results2[2].length!=1){
												//verif que le pion et la board existent
												//mauvais diese
												callback({stack:'mauvais diese createPionOnBoard'});
											}else if(results2[0].length!=1){
												//pas le bon pion
												callback({stack:'mauvais pion createPionOnBoard'});
											}else{
												var r={
													results2:results2,
													resNoeuds:resNoeuds
												}
												callback(null,r);
											}
										});
									},
									function(r,callback){
											 
										//test pour savoir si on doit ajouter des caractéristiques de pion à la page
										r.test = r.results2[1].length!=1;
										const query={
											text:'INSERT INTO pion_on_board_on_page (id_page,diese_pion_on_board_on_page,num_pion_on_board_on_page,id_pion,diese_board_on_page,position_x,position_y,taille,noeud_start) '+
												' SELECT $1,CASE WHEN pp.new_diese IS NULL THEN 1 ELSE new_diese END, '+
												' CASE WHEN pp.new_num IS NULL THEN 0 ELSE new_num END,$2,$3, $4,$5 , $6,$7 FROM page p'+
												' LEFT JOIN (SELECT MAX(num_pion_on_board_on_page)+1 new_num, MAX(diese_pion_on_board_on_page)+1 new_diese, MAX(id_page) id_pagec FROM pion_on_board_on_page GROUP BY id_page) AS pp ON p.id_page=pp.id_pagec'+
												' WHERE id_page = $1 LIMIT 1;',
											values:[resIP[0].id_page,r.results2[0][0].id_pion,requetes_tot[nb_requete].data.board,requetes_tot[nb_requete].data.left,requetes_tot[nb_requete].data.top,requetes_tot[nb_requete].data.height,r.resNoeuds[3][0].new_noeud],
										};
										client.query(query,function(err,results){
											if(err){
												callback(err);
											}else{
												//resTot.push(results.rows);
												result3=[results.rows];
												callback(null,r,result3);
											}
										});
									},

									function(r,result3,callback){
										const query={
											text:'INSERT INTO stop_pion_on_board_on_page (id_page,diese_board_on_page,num_pion_on_board_on_page,noeud_stop) SELECT '+
												'$1,$2 ,MAX(num_pion_on_board_on_page),0 FROM pion_on_board_on_page WHERE id_page = $1 GROUP BY id_page;',
											values:[resIP[0].id_page,requetes_tot[nb_requete].data.board]
										};
										
										queryChainedClient(client,query,callback,r,result3);						
									},

									function(r,result3,callback){
										const query = {
											text:' SELECT MAX(diese_pion_on_board_on_page) diese,MAX(num_pion_on_board_on_page) num FROM pion_on_board_on_page  WHERE id_page = $1 GROUP BY id_page;',
											values:[resIP[0].id_page]
										}
										queryChainedClient(client,query,callback,r,result3);
									},
									
									function(r,result3,callback){	
										if(r.test){
											const query={
												text:'INSERT INTO carac_pions_page (id_page,id_pion,change_pion,diese_bac_on_page,taille) SELECT ($1,$2,$2,1,$3) ; ',
												values:[resIP[0].id_page,r.results2[1][0].id_pion,r.results2[1][0].taille]
											};
											
											client.query(query,function(err,results){
												if(err){
													callback(err);
												}else{
													//resTot.push(results.rows);
													result3.push(results.rows);
													callback(null,result3);
												}
											});
										}else{
											callback(null,r,result3);
										}
															
									},
									function(r,result3,callback){		
										if(r.test){
											const query={
												text:'INSERT INTO carac_pions_joueur_on_page (id_joueur,id_page,id_pion,change_pion,diese_bac_on_page,taille) SELECT id_joueur,$1,$2,$2,1,$3 FROM carac_pions_joueur_on_page WHERE id_page = $1 LIMIT 1; ',
												values:[resIP[0].id_page,r.results2[1][0].id_pion,r.results2[1][0].taille]
											};
											
											client.query(query,function(err,results){
												if(err){
													callback(err);
												}else{
													//resTot.push(results.rows);
													result3.push(results.rows);
													callback(null,result3);
												}
											});
										}else{
											callback(null,r,result3);
										}
															
									},
									function(r,result3,callback){
										if(r.test){
											emit.push({text:'getTabPion',data:null});
											emit_broad.push({text:'getTabPion',data:null});
										}
										requetes_tot[nb_requete].data.diese = result3[2][0].diese;
										requetes_tot[nb_requete].data.svg = r.results2[0][0].svg_pion;
										requetes_tot[nb_requete].data.directed = r.results2[0][0].directed;
										requetes_tot[nb_requete].data.num =result3[2][0].num;
										requetes_tot[nb_requete].data.noeud =r.resNoeuds[3][0].new_noeud;
										emit.push({text:'createPionOnboard',data:requetes_tot[nb_requete].data});
										emit_broad.push({text:'createPionOnboard',data:requetes_tot[nb_requete].data});
										nb_requete +=1;
										callback(null,r.resNoeuds)
									}
								]);
							}							
						//action createGroupeOnbac--------------------------------------------------------------

						}else if(requete.action ==='createGroupeOnbac'){//ok
							if(!checkValide(variables={
								type:'45',
								n:1,
								bac:1},needsData=false,errMess='createGroupeOnbac',data=requete.data)){
								console.log(errMess+'.data incorrect dans createGroupeOnbac');
								Abort=true;
							}else{
								Array_waterfall=Array_waterfall.concat([
									function(resNoeuds,callback){
										const query = {
											text:'SELECT id_pion,taille,svg_pion FROM pion WHERE nom_pion = $1;',
											values:[requetes_tot[nb_requete].data.type]
										}
										client.query(query,function(err,results){
											if(err){
												callback(err);
											}else{
												//resTot.push(results.rows);
												results2=[results.rows];
												callback(null,resNoeuds,results2);
											}
										});
									},
									
									function(resNoeuds,results2,callback){
										const query = {
											text:'SELECT c.id_pion FROM carac_pions_page c JOIN pion p ON c.id_pion=p.id_pion WHERE p.nom_pion = $1 AND c.id_page = $2; ',
											values:[requetes_tot[nb_requete].data.type,resIP[0].id_page]
										}
										queryChainedClient(client,query,callback,resNoeuds,results2);
									},
																					
									function(resNoeuds,results2,callback){
										const query = {
											text:'SELECT num_bac_on_page FROM bac_on_page b'+
												' LEFT JOIN (SELECT COUNT(noeud_stop) nbstop,MAX(id_page) id, MAX(num_bac_on_page) num FROM stop_bac_on_page'+
												' WHERE id_page = $1 AND ARRAY[noeud_stop] <@ $2 GROUP BY id_page,num_bac_on_page) AS g'+
												' ON b.id_page=g.id AND b.num_bac_on_page = g.num'+
												' WHERE b.id_page = $1 AND diese_bac_on_page=$3'+
												' AND ARRAY[b.noeud_start] <@ $2 AND ( COALESCE(g.nbstop,0)=0 OR g.nbstop=0) ;',
											values:[resIP[0].id_page,resNoeuds[4],requetes_tot[nb_requete].data.bac]
										}
										client.query(query,function(err,results){
											results2.push(results.rows);
											if(err){
												callback(err);
											}else if(results2[0].length!=1 || results2[2].length!=1){
												//mauvais diese
												callback({stack:'mauvais diese createGroupeOnbac'});
											}else if(results2[1].length!=1){
												//pas le bon pion
												callback({stack:'mauvais pion createGroupeOnbac'});
											}else{
												var r ={
													resNoeuds:resNoeuds,
													results2,results2
												}
												callback(null,r);
											}
										});
									},
									function(r,callback){
										//test pour savoir si on doit ajouter des caractéristiques de pion à la page
										r.test = r.results2[1].length!=1;
										const query={
											text:'INSERT INTO pion_on_bac_on_page (id_page,diese_pion_on_bac_on_page,num_pion_on_bac_on_page,id_pion,diese_bac_on_page,nombre_pions,noeud_start) '+
												' SELECT $1'+
												', CASE WHEN new_diese IS NULL THEN 1 ELSE new_diese END,'+
												' CASE WHEN new_num IS NULL THEN 0 ELSE new_num END, '+
												'$2,$3, $4, new_noeud '+
												' FROM page  p LEFT JOIN '+
												'(SELECT MAX(id_page) id_pageb,MAX(noeud_suivant) new_noeud FROM save_on_page GROUP BY id_page) AS s ON p.id_page=s.id_pageb'+
												' LEFT JOIN (SELECT MAX(num_pion_on_bac_on_page)+1 new_num, MAX(diese_pion_on_bac_on_page)+1 new_diese, MAX(id_page) id_pagec FROM pion_on_bac_on_page GROUP BY id_page) AS pp ON p.id_page=pp.id_pagec'+
												' WHERE id_page = $1 LIMIT 1;',
											values:[resIP[0].id_page,r.results2[0][0].id_pion,requetes_tot[nb_requete].data.bac,requetes_tot[nb_requete].data.n],
										};
										client.query(query,function(err,results){
											if(err){
												callback(err);
											}else{
												//resTot.push(results.rows);
												result3=[results.rows];
												callback(null,r,result3);
											}
										});
									},

									function(r,result3,callback){
										const query={
											text:'INSERT INTO stop_pion_on_bac_on_page (id_page,diese_bac_on_page,num_pion_on_bac_on_page,noeud_stop) SELECT '+
												'$1,$2 ,MAX(num_pion_on_bac_on_page),0 FROM pion_on_bac_on_page WHERE id_page = $1 GROUP BY id_page;',
											values:[resIP[0].id_page,requetes_tot[nb_requete].data.bac]
										};
										
										queryChainedClient(client,query,callback,r,result3);						
									},
									function(r,result3,callback){
										const query = {
											text:' SELECT MAX(diese_pion_on_bac_on_page) diese,MAX(num_pion_on_bac_on_page) num FROM pion_on_bac_on_page  WHERE id_page = $1 GROUP BY id_page; ',
											values:[resIP[0].id_page]
										}
										queryChainedClient(client,query,callback,r,result3);
									},
									
									function(r,result3,callback){		
										if(r.test){
											const query={
												text:'INSERT INTO carac_pions_page (id_page,id_pion,change_pion,diese_bac_on_page,taille) SELECT ($1,$2,$2,1,$3) ; ',
												values:[resIP[0].id_page,r.results2[1][0].id_pion,r.results2[1][0].taille]
											};
											
											client.query(query,function(err,results){
												if(err){
													callback(err);
												}else{
													//resTot.push(results.rows);
													result3.push(results.rows);
													callback(null,r,result3);
												}
											});
										}else{
											callback(null,r,result3);
										}
															
									},
									function(r,result3,callback){		
										if(r.test){
											const query={
												text:'INSERT INTO carac_pions_joueur_on_page (id_joueur,id_page,id_pion,change_pion,diese_bac_on_page,taille) SELECT id_joueur,$1,$2,$2,1,$3 FROM carac_pions_joueur_on_page WHERE id_page = $1 LIMIT 1; ',
												values:[resIP[0].id_page,r.results2[1][0].id_pion,r.results2[1][0].taille]
											};
											
											client.query(query,function(err,results){
												if(err){
													callback(err);
												}else{
													//resTot.push(results.rows);
													result3.push(results.rows);
													callback(null,r,result3);
												}
											});
										}else{
											callback(null,r,result3);
										}
															
									},function(r,result3,callback){
										if(r.test){
											emit.push({text:'getTabPion',data:null});
											emit_broad.push({text:'getTabPion',data:null});
										}
										requetes_tot[nb_requete].data.diese = result3[2][0].diese;
										requetes_tot[nb_requete].data.svg=r.results2[0][0].svg_pion;
										requetes_tot[nb_requete].data.num = result3[2][0].num;
										requetes_tot[nb_requete].data.noeud = r.resNoeuds[3][0].new_noeud;
										emit.push({text:'createGroupeOnbac',data:requetes_tot[nb_requete].data});
										emit_broad.push({text:'createGroupeOnbac',data:requetes_tot[nb_requete].data});
										nb_requete +=1;
										callback(null,r.resNoeuds);
									}
								]);	
							}			
						//action changeLabelBac--------------------------------------------------------------

						}else if(requete.action ==='changeLabelBac'){//
							if(!checkValide(variables={diese:1,label:'20'},needsData=true,errMess='changeLabelBac',data=requete.data)){
								console.log(errMess+'.data incorrect dans changeLabelBac');
								Abort = true;
							}else{
								
								Array_waterfall=Array_waterfall.concat([
									function(resNoeuds,callback){
										const query={
											text:'SELECT num_bac_on_page FROM bac_on_page b'+
												' LEFT JOIN (SELECT COUNT(noeud_stop) nbstop,MAX(id_page) id, MAX(num_bac_on_page) num FROM stop_bac_on_page'+
												' WHERE id_page = $1 AND ARRAY[noeud_stop] <@ $2 GROUP BY id_page,num_bac_on_page) AS g'+
												' ON b.id_page=g.id AND b.num_bac_on_page = g.num'+
												' WHERE b.id_page = $1 AND diese_bac_on_page=$3'+
												' AND ARRAY[b.noeud_start] <@ $2 AND ( COALESCE(g.nbstop,0)=0 OR g.nbstop=0) ;',
											values:[resIP[0].id_page,resNoeuds[4],requetes_tot[nb_requete].data.diese]
											
										};
										client.query(query,
											function(err2,results2){
												if(err2){
													console.log('erreur de changeLabelBac : '+err2.stack);
													callback(err2);
													//return false;
												}else{
													//verif que le bac existe
													if(results2.rows.length!=1){
														//console.log('mauvais diese3');
														//socket.emit('refresh');
														//return false;
														callback({stak:'mauvais diese3'});
													}else{
														r={
															resNoeuds:resNoeuds,
															results2:results2.rows
														};
														callback(null,r);
													}
												}
											}
										);
									},
									function(r,callback){
										const query={
											text:'INSERT INTO stop_bac_on_page (id_page,num_bac_on_page,noeud_stop)'+
												' SELECT $1,$2,$3;',
											values:[resIP[0].id_page,r.results2[0].num_bac_on_page,r.resNoeuds[3][0].new_noeud],
										};
										client.query(query,function(err,results){
											if(err){
												callback(err);
											}else{
												//resTot.push(results.rows);
												resTot=[results.rows];
												callback(null,r,resTot);
											}
										});
									},

									function(r,resTot,callback){
										const query={
											text:'INSERT INTO bac_on_page (id_page,diese_bac_on_page,num_bac_on_page,label_bac,noeud_start) '+
												' SELECT $1,$2, new_num, $3, new_noeud FROM page p LEFT JOIN '+
												'(SELECT MAX(id_page) id_pageb,MAX(noeud_suivant) new_noeud FROM save_on_page GROUP BY id_page) AS s ON p.id_page=s.id_pageb'+
												' LEFT JOIN (SELECT MAX(num_bac_on_page)+1 new_num,MAX(id_page) id_pagec FROM bac_on_page GROUP BY id_page) AS b ON p.id_page=b.id_pagec'+
												' WHERE id_page = $1 LIMIT 1;',
											values:[resIP[0].id_page,requetes_tot[nb_requete].data.diese,requetes_tot[nb_requete].data.label]
										};
										
										queryChainedClient(client,query,callback,r,resTot);						
									},
									function(r,resTot,callback){
										const query={
											text:'INSERT INTO stop_bac_on_page (id_page,num_bac_on_page,noeud_stop) SELECT '+
												'$1,MAX(num_bac_on_page),0 FROM bac_on_page WHERE id_page = $1 GROUP BY id_page;',
											values:[resIP[0].id_page]
										};
										
										queryChainedClient(client,query,callback,r,resTot);						
									},						
									
									function(r,resTot,callback){
										const query={
											text:'SELECT MAX(num_bac_on_page) num FROM bac_on_page WHERE id_page = $1 GROUP BY id_page; ',
											values:[resIP[0].id_page]
										};
										
										client.query(query,function(err,results){
											if(err){
												callback(err);
											}else{
												//resTot.push(results.rows);
												resTot.push(results.rows);
												callback(null,r,resTot);
											}
										});						
									},
									function(r,resTot,callback){
										requetes_tot[nb_requete].data.num=resTot[3][0].num;
										requetes_tot[nb_requete].data.oldnum=r.results2[0].num_bac_on_page;
										//requetes_tot[nb_requete].data.noeud=resIP[0].noeud_courant;
										requetes_tot[nb_requete].data.noeud=resNoeuds[3][0].new_noeud;
										emit.push({text:'changeLabelBac',data:requetes_tot[nb_requete].data});
										emit_broad.push({text:'changeLabelBac',data:requetes_tot[nb_requete].data});
										nb_requete +=1;
										callback(null,r.resNoeuds);
									}
								]);
							}								
						}		
					}
					
					if(shouldAbort(Abort)){
						return
					}else{
						async.waterfall(Array_waterfall,
							function(err,result){
								if(shouldAbort(err)){
									return
								}else{
									client.query('COMMIT',function(err){
										if(shouldAbort(err)) return
										for(var i=0; i<emit.length;i++){
											if(emit[i].data===null){
												socket.emit(emit[i].text);
											}else{
												socket.emit(emit[i].text,emit[i].data);
											}
										}
										for(var i=0; i<emit_broad.length;i++){
											if(emit_broad[i].data===null){
												socket.emit(emit_broad[i].text);
											}else{
												socket.broadcast.to(socket.nomPlatoo).emit(emit_broad[i].text,emit_broad[i].data);
											}
										}
										done();
									});
								}
							}
						);
					}
				});
			}
		});
	});
});



//http://stackoverflow.com/questions/19035373/how-do-i-redirect-in-expressjs-while-passing-some-context

//http://stackoverflow.com/questions/5710358/how-to-retrieve-post-query-parameters

//http://naholyr.fr/2011/06/bonnes-pratiques-asynchrone-javascript-nodejs/

//promises
