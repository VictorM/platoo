# platoo

## Instructions

Launch docker compose stack:

```
sudo docker-compose up
```

Now, find the ID of your PostgreSQL database:

```bash
sudo docker ps
# My output:
# CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                    NAMES
# 1e211e6495ef        postgres:9.6.18     "docker-entrypoint.s…"   15 minutes ago      Up 15 minutes       5432/tcp                 platoo_postgresql_1
# 03577974bad6        victorm/platoo:v1   "node index.js"          15 minutes ago      Up 15 minutes       0.0.0.0:8080->8080/tcp   platoo_platoo_1

```

Mine is `1e211e6495ef`.
Now, we will copy our SQL file in the container and open a shell in it

```
docker cp platoo.sql 1e211e6495ef:/tmp
docker exec -ti 1e211e6495ef bash
```

Now, we will simply restore our file:

```
cd /tmp
psql -U platoo platoodb < platoo.sql
```

