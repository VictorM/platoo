FROM debian:buster

RUN apt-get update && \
    apt-get install -y nodejs npm
    
COPY . /srv/platoo
WORKDIR /srv/platoo

RUN npm install

CMD ["node", "index.js"]





